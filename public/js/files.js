/*function readUrl(input) {
  
  if (input.files && input.files[0]) {
    let reader = new FileReader();
    reader.onload = (e) => {
      let imgData = e.target.result;
      let imgName = input.files[0].name;
      input.setAttribute("data-title", imgName);
      console.log(e.target.result);
    }
    reader.readAsDataURL(input.files[0]);
  }

}*/

(function() {
  
    // getElementById
    function $id(id) {
      return document.getElementById(id);
    }
  
  
    // output information
    function Output(msg) {
      var m = $id("messages");
      m.innerHTML = msg + m.innerHTML;
    }
  
  
    // file drag hover
    function FileDragHover(e) {
      e.stopPropagation();
      e.preventDefault();
      e.target.className = (e.type == "dragover" ? "hover" : "");
    }
  
  
    // file selection
    function FileSelectHandler(e) {
  
      // cancel event and hover styling
      FileDragHover(e);
  
      // fetch FileList object
      var files = e.target.files || e.dataTransfer.files;
  
      // process all File objects
      for (var i = 0, f; f = files[i]; i++) {
        ParseFile(f);
        UploadFile(f);
      }
  
    }
  
  
    // output file information
    function ParseFile(file) {
      
      Output(
        "<p>File information: <strong>" + file.name +
        "</strong> type: <strong>" + file.type +
        "</strong> size: <strong>" + file.size +
        "</strong> bytes</p>"
      );
  
    }

    // upload JPEG files
    function UploadFile(file) {

      var xhr = new XMLHttpRequest();
      if (xhr.upload && file.size <= $id("MAX_FILE_SIZE").value) {
      
        // create progress bar
        var o = $id("progress");
        var progress = o.appendChild(document.createElement("p"));
        progress.appendChild(document.createTextNode("uploading " + file.name + " | " + file.size + " Bytes"));
    
        // progress bar
        xhr.upload.addEventListener("progress", function(e) {
          var pc = parseInt(100 - (e.loaded / e.total * 100));
          progress.style.backgroundPosition = pc + "% 0";
        }, false);
    
        // file received/failed
        xhr.onreadystatechange = function(e) {
          if (xhr.readyState == 4) {
            progress.className = (xhr.status == 200 ? "success" : "failure");
          }
        };
    
        // start upload
        xhr.open("POST", $id("uploadFilesForm").action+"?idArchivo="+$id("idArchivo").value+"&idPadre="+$id("idPadre").value, true);
        xhr.setRequestHeader("X-FILENAME", file.name);
        xhr.send(file);

      } else {
        alert("Archivo demasiado grande para hacer upload");
      }

    }
  
  
    // initialize
    function Init() {
  
      var fileselect = $id("uploadify"),
        filedrag = $id("filedrag"),
        submitbutton = $id("submitbutton");
  
      // file select
      fileselect.addEventListener("change", FileSelectHandler, false);
  
      // is XHR2 available?
      var xhr = new XMLHttpRequest();
      if (xhr.upload) {
  
        // file drop
        filedrag.addEventListener("dragover", FileDragHover, false);
        filedrag.addEventListener("dragleave", FileDragHover, false);
        filedrag.addEventListener("drop", FileSelectHandler, false);
        filedrag.style.display = "block";
  
        // remove submit button
        //submitbutton.style.display = "none";
      }
  
    }
  
    // call initialization file
    if (window.File && window.FileList && window.FileReader) {
      //Init();
    }
  
  
  })();