<?php
//Route::get('/', 'Auth\LoginController@showFormlogin')->name('login');
Route::get('/', ['as' => 'index', 'uses' => 'Auth\LoginController@showFormlogin']);

/* Login and logout */
Route::post('login', 'Auth\LoginController@login')->name('login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
//Route::get('resetpassword', 'Auth\LoginController@resetpassword')->name('resetpassword');

/* password reset */
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.request');

/* Home - Splash - Main modules */
Route::get('home', 'HomeController@index')->name('home')->middleware('terms&conditions');
Route::get('terms', 'HomeController@terms')->name('terms');
Route::post('terms', 'HomeController@accept');

/* Administracion */
Route::get('administration', 'AdministrationController@index')->name('administration');
Route::get('administration.languageselection', 'AdministrationController@languageselection')->name('administration.languageselection');
Route::get('administration.user.find', 'UserController@search')->name('administration.user.find');
Route::get('administration.profile.find', 'ProfileController@search')->name('administration.profile.find');
Route::get('administration.groups.find', 'GroupsController@search')->name('administration.groups.find');
Route::get('administration.news.find', 'NoticiasController@search')->name('administration.news.find');
Route::get('administration.enterprise.find', 'EmpresasController@search')->name('administration.enterprise.find');
Route::get('administration.country.find', 'AdministrationController@findcountry')->name('administration.country.find');
Route::get('administration.translations.find', 'TranslationsController@search')->name('administration.translations.find');

	/* Usuarios */
	Route::get('administration.users', 'UserController@index')->name('administration.users');
	Route::get('user/create', 'UserController@create')->name('administration.user.create');
	Route::post('user', 'UserController@store')->name('administration.user.store');
	Route::get('user/{id}', 'UserController@show')->name('administration.user.show');
	Route::get('user/{id}/edit', 'UserController@edit')->name('administration.user.edit');
	Route::put('user/{id}', 'UserController@update')->name('administration.user.update');
	Route::delete('user/{id}', 'UserController@destroy')->name('administration.user.delete');
		/* Herramientas */
		Route::get('user.offices/{id}', 'UserSettingsController@offices')->name('administration.user.offices');
		Route::get('user.officesrelated', 'UserSettingsController@officesrelated')->name('administration.user.officesrelated');
		Route::post('user.relateoffice', 'UserSettingsController@relateoffice')->name('administration.user.relateoffice');
		Route::delete('user.deleteoffice', 'UserSettingsController@deleteoffice')->name('administration.user.deleteoffice');
		/* Configuraciones generales */
		
		/* SI */
		Route::get('user.inspections/{id}', 'UserSettingsController@inspections')->name('administration.user.inspections');
		Route::post('inspection.documents', 'UserSettingsController@documents')->name('administration.inspection.documents');

		Route::get('user.ispm/{id}', 'UserSettingsController@ispm')->name('administration.user.ispm');
		Route::post('ispm.projects', 'UserSettingsController@customerprojectsselection')->name('administration.ispm.ispmprojects');

		/* PR */
		Route::get('user.certifies/{id}', 'UserSettingsController@certifies')->name('administration.user.certifies');
		

	/* Perfiles */
	Route::get('administration.profiles', 'ProfileController@index')->name('administration.profiles');
	Route::get('profile/create', 'ProfileController@create')->name('administration.profile.create');
	Route::post('profile', 'ProfileController@store')->name('administration.profile.store');
	Route::get('profile/{id}', 'ProfileController@show')->name('administration.profile.show');
	Route::get('profile/{id}/edit', 'ProfileController@edit')->name('administration.profile.edit');
	Route::put('profile/{id}', 'ProfileController@update')->name('administration.profile.update');
	Route::delete('profile/{id}', 'ProfileController@destroy')->name('administration.profile.delete');
		/*Menues*/
		Route::get('administration.profile.menuadmin/{id}/parent/{id_parent?}', 'MenuController@show')->name('administration.profile.menuadmin');
		Route::post('administration.menuadmin', 'MenuController@store')->name('administration.menuadmin');
	/* Grupos */
	Route::get('administration.groups', 'GroupsController@index')->name('administration.groups');
	Route::get('group/create', 'GroupsController@create')->name('administration.groups.create');
	Route::post('group', 'GroupsController@store')->name('administration.groups.store');
	Route::get('group/{id}', 'GroupsController@show')->name('administration.groups.show');
	Route::get('group/{id}/edit', 'GroupsController@edit')->name('administration.groups.edit');
	Route::put('group/{id}', 'GroupsController@update')->name('administration.groups.update');
	Route::delete('group/{id}', 'GroupsController@destroy')->name('administration.groups.delete');
		/* Management */
		Route::get('group/{id}/users', 'GroupsController@users')->name('administration.groups.users');
		Route::post('group/{idgroup}/user/{iduser}', 'GroupsController@userstore')->name('administration.groups.userstore');
		Route::get('group/{id}/admin', 'GroupsController@admin')->name('administration.groups.admin');
		Route::get('group/{id}/user/{iduser}', 'GroupsController@adminstore')->name('administration.groups.adminstore');

	/* News */
	Route::get('administration.news', 'NoticiasController@index')->name('administration.news');
	Route::get('news/create', 'NoticiasController@create')->name('administration.news.create');
	Route::post('news', 'NoticiasController@store')->name('administration.news.store');
	Route::get('news/{id}', 'NoticiasController@show')->name('administration.news.show');
	Route::get('news/{id}/edit', 'NoticiasController@edit')->name('administration.news.edit');
	Route::put('news/{id}', 'NoticiasController@update')->name('administration.news.update');
	Route::delete('news/{id}', 'NoticiasController@destroy')->name('administration.news.delete');
		/* Translate */
		Route::get('news/{id}/translate/{ididioma?}', 'NoticiasController@translate')->name('administration.news.translate');

	/* Enterprises */
	Route::get('administration.enterprises', 'EmpresasController@index')->name('administration.enterprises');
	Route::get('enterprises/create', 'EmpresasController@create')->name('administration.enterprises.create');
	Route::post('enterprises', 'EmpresasController@store')->name('administration.enterprises.store');
	Route::get('enterprises/{id}', 'EmpresasController@show')->name('administration.enterprises.show');
	Route::get('enterprises/{id}/edit', 'EmpresasController@edit')->name('administration.enterprises.edit');
	Route::put('enterprises/{id}', 'EmpresasController@update')->name('administration.enterprises.update');
	Route::delete('enterprises/{id}', 'EmpresasController@destroy')->name('administration.enterprises.delete');
		/* wizard */
		Route::get('enterprises/{id}/address', 'EmpresasController@address')->name('administration.enterprises.address');
		Route::post('enterprises/storeadress', 'EmpresasController@storeaddress')->name('administration.enterprises.storeaddress');
		Route::get('enterprises/{id}/type', 'EmpresasController@type')->name('administration.enterprises.type');
		Route::post('enterprises/storetype', 'EmpresasController@storetype')->name('administration.enterprises.storetype');
		/* offices */
		Route::get('enterprises/{idempresa}/offices', 'OficinasController@index')->name('administration.offices');
		Route::get('offices/create/{idempresa}', 'OficinasController@create')->name('administration.offices.create');
		Route::post('offices', 'OficinasController@store')->name('administration.offices.store');
		Route::get('offices/{idoficina}', 'OficinasController@show')->name('administration.offices.show');
		Route::get('offices/{idoficina}/edit', 'OficinasController@edit')->name('administration.offices.edit');
		Route::put('offices/{idoficina}', 'OficinasController@update')->name('administration.offices.update');
		Route::delete('offices/{idoficina}', 'OficinasController@destroy')->name('administration.offices.delete');

	/* countries -> could be improved later */
	Route::get('administration.countries', 'AdministrationController@countries')->name('administration.countries');

	/* translations */
	Route::get('administration.translations', 'TranslationsController@index')->name('administration.translations');
	Route::get('translations/create', 'TranslationsController@create')->name('administration.translations.create');
	Route::post('translations', 'TranslationsController@store')->name('administration.translations.store');
	Route::get('translations/{id}', 'TranslationsController@show')->name('administration.translations.show');
	Route::get('translations/{id}/edit', 'TranslationsController@edit')->name('administration.translations.edit');
	Route::put('translations/{id}', 'TranslationsController@update')->name('administration.translations.update');
	Route::delete('translations/{id}', 'TranslationsController@destroy')->name('administration.translations.delete');
	
	/* dummy unit */
	Route::get('administration.lnupdates', 'AdministrationController@lnupdates')->name('administration.lnupdates');

Route::get('administration.emailconfig', 'AdministrationController@emailconfig')->name('administration.emailconfig');
Route::get('administration.mailsentlog', 'AdministrationController@mailsentlog')->name('administration.mailsentlog');

/* Utils */
Route::get('utils', 'UtilsController@index')->name('utils');
Route::post('utils.files.search', 'FilesController@search')->name('utils.files.search');
Route::get('files/{id}/info', 'FilesController@info')->name('utils.files.info');


	Route::get('utils.files', 'FilesController@index')->name('utils.files');
	Route::get('files/create', 'FilesController@create')->name('utils.files.create');
	Route::post('files', 'FilesController@store')->name('utils.files.store');
	Route::get('files/show', 'FilesController@show')->name('utils.files.show');
	Route::get('files/{id}/edit', 'FilesController@edit')->name('utils.files.edit');
	Route::put('files/{id}', 'FilesController@update')->name('utils.files.update');
	Route::delete('files/{id}', 'FilesController@destroy')->name('utils.files.delete');

	Route::post('files.copy', 'FilesController@copy')->name('utils.files.copy');
	Route::post('files.cut', 'FilesController@cut')->name('utils.files.cut');
	Route::post('files.paste', 'FilesController@paste')->name('utils.files.paste');
	Route::post('files.upload', 'FilesController@upload')->name('utils.files.upload');
	Route::get('files/permission', 'FilesController@permission')->name('utils.files.permission');
	Route::get('files/grouppermission', 'FilesController@grouppermission')->name('utils.files.grouppermission');
	Route::post('files/grouppermission', 'FilesController@grouppermissionedit')->name('utils.files.grouppermission');
	Route::get('files/userspermission', 'FilesController@userspermission')->name('utils.files.userspermission');
	Route::post('files/userspermission', 'FilesController@userspermissionedit')->name('utils.files.userspermission');
	Route::post('files/setpermission', 'FilesController@setpermission')->name('utils.files.setpermission');

		Route::get('files.fillaviso', 'FilesController@fillaviso')->name('utils.files.fillaviso');
		Route::get('files.fillgroups', 'FilesController@fillgroups')->name('utils.files.fillgroups');
		Route::get('files.fillusers', 'FilesController@fillusers')->name('utils.files.fillusers');


Route::get('utils.agenda', 'UtilsController@agenda')->name('utils.agenda');
Route::get('utils.forum', 'UtilsController@forum')->name('utils.forum');


/* Products */
Route::get('products', 'ProductsController@index')->name('products');

/* Industrials */
Route::get('industrials', 'ProductsController@index')->name('industrials');

/* Learning */
Route::get('learning', 'ProductsController@index')->name('learning');

/* Due Diligence */
Route::get('duediligence', 'ProductsController@index')->name('duediligence');