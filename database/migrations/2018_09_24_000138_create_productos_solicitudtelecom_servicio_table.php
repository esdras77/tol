<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosSolicitudtelecomServicioTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'productos_solicitudtelecom_servicio';

    /**
     * Run the migrations.
     * @table productos_solicitudtelecom_servicio
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('idSolicitudTelecom')->unsigned();
            $table->integer('idServicio')->unsigned();

            $table->index(["idServicio"], 'fk_productos_solicitudtelecom_servicio_productos_servicio1_idx');

            $table->index(["idSolicitudTelecom"], 'fk_productos_solicitudtelecom_servicio_productos_solicitudt_idx');


            $table->foreign('idSolicitudTelecom', 'fk_productos_solicitudtelecom_servicio_productos_solicitudt_idx')
                ->references('idSolicitudTelecom')->on('productos_solicitudtelecom')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idServicio', 'fk_productos_solicitudtelecom_servicio_productos_servicio1_idx')
                ->references('idServicio')->on('productos_servicio')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
