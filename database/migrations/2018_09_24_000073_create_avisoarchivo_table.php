<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvisoarchivoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'avisoarchivo';

    /**
     * Run the migrations.
     * @table avisoarchivo
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idAvisoArchivo');
            $table->integer('idArchivo')->unsigned();
            $table->dateTime('enviado');

            $table->index(["idArchivo"], 'fk_avisoarchivo_archivo1_idx');


            $table->foreign('idArchivo', 'fk_avisoarchivo_archivo1_idx')
                ->references('idArchivo')->on('archivo')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
