<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosMemoriatecnicaTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'productos_memoriatecnica';

    /**
     * Run the migrations.
     * @table productos_memoriatecnica
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idMemoriaTecnica');
            $table->integer('idUsuario')->unsigned();
            $table->integer('idDatosCertificacion')->unsigned();
            $table->integer('idTipoDeProducto')->unsigned();
            $table->string('titular', 50);
            $table->string('solicitante', 50);
            $table->date('fechaAlta')->default('2014-01-01');
            $table->string('taller_nombre')->nullable()->default(null);
            $table->string('taller_direccion')->nullable()->default(null);
            $table->string('taller_origen', 50);
            $table->date('fechaDeEmbarque')->nullable()->default(null);
            $table->string('imagen')->nullable()->default(null);
            $table->text('observaciones')->nullable()->default(null);
            $table->string('firma')->nullable()->default(null);;
            $table->string('aclaracion')->nullable()->default(null);;
            $table->tinyInteger('confirmed')->default('0');
            $table->integer('rol_facturacion')->unsigned();
            $table->integer('rol_facturacion_cuotas')->unsigned();
            $table->integer('rol_facturacion_empresa')->unsigned();
            $table->string('ordenCompra')->nullable()->default(null);
            $table->tinyInteger('enviado')->default('0');
            $table->integer('cantidadEmpleados')->nullable()->default('0');

            $table->index(["idDatosCertificacion"], 'fk_productos_memoriatecnica_productos_datoscertificacion1_idx');

            $table->index(["titular"], 'fk_productos_memoriatecnica_empresa1_idx');

            $table->index(["idUsuario"], 'fk_productos_memoriatecnica_usuario1_idx');

            $table->index(["solicitante"], 'fk_productos_memoriatecnica_empresa2_idx');

            $table->index(["rol_facturacion"], 'fk_productos_memoriatecnica_productos_rol1_idx');

            $table->index(["rol_facturacion_cuotas"], 'fk_productos_memoriatecnica_productos_rol2_idx');

            $table->index(["rol_facturacion_empresa"], 'fk_productos_memoriatecnica_productos_rol3_idx');

            $table->index(["idTipoDeProducto"], 'fk_productos_memoriatecnica_productos_tipodeproducto1_idx');


            $table->foreign('idDatosCertificacion', 'fk_productos_memoriatecnica_productos_datoscertificacion1_idx')
                ->references('idDatosCertificacion')->on('productos_datoscertificacion')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('rol_facturacion', 'fk_productos_memoriatecnica_productos_rol1_idx')
                ->references('idRol')->on('productos_rol')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('rol_facturacion_cuotas', 'fk_productos_memoriatecnica_productos_rol2_idx')
                ->references('idRol')->on('productos_rol')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('rol_facturacion_empresa', 'fk_productos_memoriatecnica_productos_rol3_idx')
                ->references('idRol')->on('productos_rol')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('titular', 'fk_productos_memoriatecnica_empresa1_idx')
                ->references('idEmpresa')->on('empresa')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('solicitante', 'fk_productos_memoriatecnica_empresa2_idx')
                ->references('idEmpresa')->on('empresa')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idTipoDeProducto', 'fk_productos_memoriatecnica_productos_tipodeproducto1_idx')
                ->references('idTipoDeProducto')->on('productos_tipodeproducto')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idUsuario', 'fk_productos_memoriatecnica_usuario1_idx')
                ->references('idUsuario')->on('usuario')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
