<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosDatoscertificacionTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'productos_datoscertificacion';

    /**
     * Run the migrations.
     * @table productos_datoscertificacion
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idDatosCertificacion');
            $table->integer('idLaboratorio')->unsigned();
            $table->integer('campoCertificacion');
            $table->integer('resolucion');
            $table->integer('normaTecnica');
            $table->string('sistemaCertificacion');
            $table->string('ID_Categoria_Producto_Grafico', 50);

            $table->index(["ID_Categoria_Producto_Grafico"], 
                'productos_mt_categorias1');

            $table->index(["idLaboratorio"], 'productos_laboratorio1_idx');

            $table->foreign('idLaboratorio', 'productos_laboratorio1_idx')
                ->references('idLaboratorio')->on('productos_laboratorio')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('ID_Categoria_Producto_Grafico', 'productos_mt_categorias1_idx')
                ->references('ID_Categoria_Producto_Grafico')->on('productos_mt_categorias')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
