<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigInspectSpecificTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'config_inspect_specific';

    /**
     * Run the migrations.
     * @table config_inspect_specific
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idSpecific');
            $table->integer('idUsuario')->unsigned();
            $table->string('idOficina', 32);
            $table->tinyInteger('inspecciones')->nullable()->default(null);
            $table->tinyInteger('operadores')->nullable()->default(null);
            $table->string('numRefTuv', 100)->nullable()->default(null);

            $table->index(["idOficina"], 'fk_config_inspect_specific_oficina1_idx');

            $table->index(["idUsuario"], 'fk_config_inspect_specific_usuario1_idx');


            $table->foreign('idUsuario', 'fk_config_inspect_specific_usuario1_idx')
                ->references('idUsuario')->on('usuario')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idOficina', 'fk_config_inspect_specific_oficina1_idx')
                ->references('idOficina')->on('oficina')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
