<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosSolicitudruedasAlmacenamientoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'productos_solicitudruedas_almacenamiento';

    /**
     * Run the migrations.
     * @table productos_solicitudruedas_almacenamiento
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('idSolicitudServicio')->unsigned();
            $table->integer('idAlmacenamiento')->unsigned();

            $table->index(["idAlmacenamiento"], 'fk_productos_solicitudruedas_almacenamiento_productos_almac_idx');

            $table->index(["idSolicitudServicio"], 'fk_productos_solicitudruedas_almacenamiento_productos_solic_idx');


            $table->foreign('idSolicitudServicio', 'fk_productos_solicitudruedas_almacenamiento_productos_solic_idx')
                ->references('idSolicitudServicio')->on('productos_solicitudruedas')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idAlmacenamiento', 'fk_productos_solicitudruedas_almacenamiento_productos_almac_idx')
                ->references('idAlmacenamiento')->on('productos_almacenamiento')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
