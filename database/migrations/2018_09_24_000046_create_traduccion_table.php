<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTraduccionTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'traduccion';

    /**
     * Run the migrations.
     * @table traduccion
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idTraduccion');
            $table->string('idTexto', 100);
            $table->integer('idIdioma')->unsigned();
            $table->text('traduccion');
            $table->date('fechaEdicion');
            $table->integer('usuarioEditor');

            $table->index(["idTexto"], 'fk_traduccion_texto1_idx');

            $table->index(["idIdioma"], 'fk_traduccion_idioma1_idx');


            $table->foreign('idTexto', 'fk_traduccion_texto1_idx')
                ->references('idTexto')->on('texto')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idIdioma', 'fk_traduccion_idioma1_idx')
                ->references('idIdioma')->on('idioma')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
