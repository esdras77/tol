<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosSolicitudtelecomAlmacenamientoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'productos_solicitudtelecom_almacenamiento';

    /**
     * Run the migrations.
     * @table productos_solicitudtelecom_almacenamiento
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('idSolicitudTelecom')->unsigned();
            $table->integer('idAlmacenamiento')->unsigned();

            $table->index(["idAlmacenamiento"], 'fk_productos_solicitudtelecom_almacenamiento_productos_alma_idx');

            $table->index(["idSolicitudTelecom"], 'fk_productos_solicitudtelecom_almacenamiento_productos_soli_idx');


            $table->foreign('idSolicitudTelecom', 'fk_productos_solicitudtelecom_almacenamiento_productos_soli_idx')
                ->references('idSolicitudTelecom')->on('productos_solicitudtelecom')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idAlmacenamiento', 'fk_productos_solicitudtelecom_almacenamiento_productos_alma_idx')
                ->references('idAlmacenamiento')->on('productos_almacenamiento')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
