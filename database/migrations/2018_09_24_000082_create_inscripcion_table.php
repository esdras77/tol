<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInscripcionTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'inscripcion';

    /**
     * Run the migrations.
     * @table inscripcion
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idInscripcion');
            $table->integer('idCurso')->unsigned();
            $table->integer('idUsuario')->unsigned();
            $table->tinyInteger('aprobado')->nullable()->default('0');
            $table->tinyInteger('participado')->nullable()->default('0');
            $table->tinyInteger('desaprobado')->nullable()->default('0');
            $table->string('nro_certificado', 45)->nullable()->default(null);
            $table->tinyInteger('notificado')->nullable()->default('0');
            $table->date('fecha')->nullable()->default(null);

            $table->index(["idUsuario"], 'fk_inscripcion_usuario1_idx');

            $table->index(["idCurso"], 'fk_inscripcion_curso1_idx');


            $table->foreign('idCurso', 'fk_inscripcion_curso1_idx')
                ->references('idCurso')->on('curso')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idUsuario', 'fk_inscripcion_usuario1_idx')
                ->references('idUsuario')->on('usuario')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
