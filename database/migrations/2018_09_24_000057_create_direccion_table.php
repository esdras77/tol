<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDireccionTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'direccion';

    /**
     * Run the migrations.
     * @table direccion
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('idDireccion', 32)->primary();
            $table->integer('idPais')->unsigned();
            $table->string('direccion');
            $table->string('codigoPostal', 25)->nullable();
            $table->string('provincia')->nullable()->default(null);
            $table->string('ciudad')->nullable();
            $table->string('localidad')->nullable();

            $table->index(["idPais"], 'fk_direccion_pais1_idx');


            $table->foreign('idPais', 'fk_direccion_pais1_idx')
                ->references('idPais')->on('pais')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
