<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentovigilanciaTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'documentovigilancia';

    /**
     * Run the migrations.
     * @table documentovigilancia
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('idLotus', 32);
            $table->string('idEmpresa', 50);
            $table->primary(['idLotus', 'idEmpresa']);
            $table->dateTime('fechaEmision');
            $table->dateTime('fechaValidez');
            $table->text('listaCertificados');
            $table->text('tipoControl');
            $table->text('productos');
            $table->string('titularCertificado');
            $table->string('agente')->nullable()->default(null);

            $table->index(["idEmpresa"], 'fk_documentovigilancia_empresa1_idx');


            $table->foreign('idEmpresa', 'fk_documentovigilancia_empresa1_idx')
                ->references('idEmpresa')->on('empresa')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
