<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosMemoriatecnicaValuesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'productos_memoriatecnica_values';

    /**
     * Run the migrations.
     * @table productos_memoriatecnica_values
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idMemoriaTecnicaValues');
            $table->integer('idMemoriaTecnica')->unsigned();
            $table->integer('idCategoria')->unsigned();
            $table->integer('idCategoriaInput')->unsigned();
            $table->text('value')->nullable()->default(null);

            $table->index(["idCategoria"], 'fk_productos_memoriatecnica_values_productos_categoria1_idx');

            $table->index(["idCategoriaInput"], 'fk_productos_memoriatecnica_values_productos_categoria_inpu_idx');

            $table->index(["idMemoriaTecnica"], 'fk_productos_memoriatecnica_values_productos_memoriatecnica_idx');


            $table->foreign('idCategoriaInput', 'fk_productos_memoriatecnica_values_productos_categoria_inpu_idx')
                ->references('idCategoriaInput')->on('productos_categoria_input')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idCategoria', 'fk_productos_memoriatecnica_values_productos_categoria1_idx')
                ->references('idCategoria')->on('productos_categoria')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idMemoriaTecnica', 'fk_productos_memoriatecnica_values_productos_memoriatecnica_idx')
                ->references('idMemoriaTecnica')->on('productos_memoriatecnica')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
