<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosSolicitudelectricosAlmacenamientoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'productos_solicitudelectricos_almacenamiento';

    /**
     * Run the migrations.
     * @table productos_solicitudelectricos_almacenamiento
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('idSolicitudElectricos')->unsigned();
            $table->integer('idAlmacenamiento')->unsigned();

            $table->index(["idAlmacenamiento"], 'fk_productos_solicitudelectricos_almacenamiento_productos_a_idx');

            $table->index(["idSolicitudElectricos"], 'fk_productos_solicitudelectricos_almacenamiento_productos_s_idx');


            $table->foreign('idSolicitudElectricos', 'fk_productos_solicitudelectricos_almacenamiento_productos_s_idx')
                ->references('idSolicitudElectricos')->on('productos_solicitudelectricos')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idAlmacenamiento', 'fk_productos_solicitudelectricos_almacenamiento_productos_a_idx')
                ->references('idAlmacenamiento')->on('productos_almacenamiento')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
