<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatosdefacturacionTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'datosDeFacturacion';

    /**
     * Run the migrations.
     * @table datosDeFacturacion
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idDatosDeFacturacion');
            $table->integer('idUsuario')->unsigned();
            $table->string('idDireccion', 32)->unique()->nullable();
            $table->integer('idTipoIdentificacion')->unsigned();
            $table->string('razonSocial')->nullable()->default(null);
            $table->string('identificacion', 11)->nullable()->default(null);
            $table->string('rubro')->nullable()->default(null);
            $table->string('telefono')->nullable()->default(null);
            $table->string('fax')->nullable()->default(null);
            $table->string('responsableDePago')->nullable()->default(null);
            $table->string('email')->nullable()->default(null);
            $table->string('ordenDeCompra', 45)->nullable()->default(null);
            $table->string('preferenciaFacturacion')->nullable()->default(null);
            $table->string('dni', 13)->nullable()->default(null);
            $table->string('cuit', 13)->nullable()->default(null);
            $table->tinyInteger('sapCustomer')->default('0');

            $table->index(["idUsuario"], 'fk_datosDeFacturacion_usuario1_idx');

            $table->index(["idDireccion"], 'fk_datosDeFacturacion_direccion1_idx');

            $table->index(["idTipoIdentificacion"], 'fk_datosDeFacturacion_tipoIdentificacion1_idx');


            $table->foreign('idDireccion', 'fk_datosDeFacturacion_direccion1_idx')
                ->references('idDireccion')->on('direccion')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idUsuario', 'fk_datosDeFacturacion_usuario1_idx')
                ->references('idUsuario')->on('usuario')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idTipoIdentificacion', 'fk_datosDeFacturacion_tipoIdentificacion1_idx')
                ->references('idTipoIdentificacion')->on('tipoIdentificacion')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
