<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentoproyectoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'documentoproyecto';

    /**
     * Run the migrations.
     * @table documentoproyecto
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('idLotus', 32);
            $table->string('idEmpresa', 50);
            $table->primary(['idLotus', 'idEmpresa']);
            $table->text('servicioSolicitado');
            $table->dateTime('fechaPedido');
            $table->string('nroPedido');
            $table->string('status');
            $table->string('responsableTUV')->nullable()->default(null);
            $table->text('estadoProceso')->nullable()->default(null);
            $table->string('producto')->nullable()->default(null);
            $table->string('marca')->nullable()->default(null);
            $table->text('modelo')->nullable()->default(null);
            $table->string('agente')->nullable()->default(null);
            $table->string('fechaCambioEstado', 10)->nullable()->default(null);
            $table->string('responsableEstado', 50)->nullable()->default(null);
            $table->text('statusHistoryTuvOnline')->nullable()->default(null);

            $table->index(["idEmpresa"], 'fk_documentoproyecto_empresa1_idx');


            $table->foreign('idEmpresa', 'fk_documentoproyecto_empresa1_idx')
                ->references('idEmpresa')->on('empresa')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
