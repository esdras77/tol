<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIspmContratoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'ispm_contrato';

    /**
     * Run the migrations.
     * @table ispm_contrato
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('idUsuario')->unsigned();
            $table->integer('idContrato');

            $table->index(["idContrato"], 'fk_ispm_contrato_ispm_inspeccion1_idx');

            $table->index(["idUsuario"], 'fk_ispm_contrato_usuario1_idx');


            $table->foreign('idUsuario', 'fk_ispm_contrato_usuario1_idx')
                ->references('idUsuario')->on('usuario')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idContrato', 'fk_ispm_contrato_ispm_inspeccion1_idx')
                ->references('idContrato')->on('ispm_inspeccion')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
