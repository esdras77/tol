<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEessxconfiguracionMovilidadTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'eessxconfiguracion_movilidad';

    /**
     * Run the migrations.
     * @table eessxconfiguracion_movilidad
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('idEess')->unsigned();
            $table->integer('idConfiguracion')->unsigned();

            $table->index(["idConfiguracion"], 'fk_eessxconfiguracion_movilidad_configuracion_movilidad1_idx');

            $table->index(["idEess"], 'fk_eessxconfiguracion_movilidad_eess1_idx');


            $table->foreign('idEess', 'fk_eessxconfiguracion_movilidad_eess1_idx')
                ->references('idEess')->on('eess')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idConfiguracion', 'fk_eessxconfiguracion_movilidad_configuracion_movilidad1_idx')
                ->references('idConfiguracion')->on('configuracion_movilidad')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
