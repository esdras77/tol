<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuarioItemdemenuTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'usuario_itemdemenu';

    /**
     * Run the migrations.
     * @table usuario_itemdemenu
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('idItemMenu')->unsigned();
            $table->integer('idUsuario')->unsigned();

            $table->index(["idItemMenu"], 'fk_usuario_itemdemenu_itemdemenu1_idx');

            $table->index(["idUsuario"], 'fk_usuario_itemdemenu_usuario1_idx');


            $table->foreign('idItemMenu', 'fk_usuario_itemdemenu_itemdemenu1_idx')
                ->references('idItemMenu')->on('itemdemenu')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idUsuario', 'fk_usuario_itemdemenu_usuario1_idx')
                ->references('idUsuario')->on('usuario')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
