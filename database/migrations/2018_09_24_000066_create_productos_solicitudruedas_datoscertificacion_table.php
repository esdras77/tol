<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosSolicitudruedasDatoscertificacionTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'productos_solicitudruedas_datoscertificacion';

    /**
     * Run the migrations.
     * @table productos_solicitudruedas_datoscertificacion
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idDatosCertificacion');
            $table->integer('idLaboratorio')->unsigned();
            $table->integer('campoCertificacion');
            $table->integer('resolucion');
            $table->integer('normaTecnica');
            $table->string('denominacion')->nullable()->default(null);
            $table->string('marca')->nullable()->default(null);
            $table->string('codigo')->nullable()->default(null);

            $table->index(["idLaboratorio"], 'fk_productos_solicitudruedas_datoscertificacion_productos_l_idx1');


            $table->foreign('idLaboratorio', 'fk_productos_solicitudruedas_datoscertificacion_productos_l_idx1')
                ->references('idLaboratorio')->on('productos_laboratorio')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
