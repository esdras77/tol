<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosEmpresasolicitanteTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'productos_empresasolicitante';

    /**
     * Run the migrations.
     * @table productos_empresasolicitante
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idProductos_empresasolicitante');
            $table->string('idEmpresa', 32)->unique();
            $table->string('cuit', 45);
            $table->string('rubro');
            $table->string('responsableLegal');
            $table->string('cargo');
            $table->string('personaDeContacto');
            $table->string('emailDeContacto');
            $table->string('telefonoDeContacto');
            $table->string('logo')->nullable()->default(null);
            $table->string('personaDeContactoTecnico')->nullable()->default(null);
            $table->string('emailDeContactoTecnico')->nullable()->default(null);
            $table->string('telefonoDeContactoTecnico')->nullable()->default(null);

            $table->index(["idEmpresa"], 'fk_productos_empresasolicitante_empresa1_idx');


            $table->foreign('idEmpresa', 'fk_productos_empresasolicitante_empresa1_idx')
                ->references('idEmpresa')->on('empresa')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
