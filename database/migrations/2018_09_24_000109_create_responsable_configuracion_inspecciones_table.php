<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResponsableConfiguracionInspeccionesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'responsable_configuracion_inspecciones';

    /**
     * Run the migrations.
     * @table responsable_configuracion_inspecciones
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idResponsableConfiguracion');
            $table->string('idEmpresa', 32);
            $table->integer('responsableTuv')->unsigned();
            $table->tinyInteger('documentos')->nullable()->default('0');
            $table->tinyInteger('vencimientos')->nullable()->default('0');

            $table->index(["responsableTuv"], 'fk_responsable_configuracion_inspecciones_usuario1_idx');

            $table->index(["idEmpresa"], 'fk_responsable_configuracion_inspecciones_empresa1_idx1');


            $table->foreign('idEmpresa', 'fk_responsable_configuracion_inspecciones_empresa1_idx1')
                ->references('idEmpresa')->on('empresa')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('responsableTuv', 'fk_responsable_configuracion_inspecciones_usuario1_idx')
                ->references('idUsuario')->on('usuario')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
