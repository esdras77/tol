<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIspmInspeccionTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'ispm_inspeccion';

    /**
     * Run the migrations.
     * @table ispm_inspeccion
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idInspeccion');
            $table->integer('idContrato');
            $table->integer('idOP');
            $table->string('cliente');
            $table->string('proyecto');
            $table->string('proveedor');
            $table->string('tipoDeInspeccion');
            $table->date('fechaDesde');
            $table->date('fechaHasta')->nullable()->default(null);
            $table->string('estado');
            $table->text('observaciones')->nullable()->default(null);
            $table->text('comentario')->nullable()->default(null);
            $table->float('calificacion')->nullable()->default(null);
            $table->string('tipoProveedor', 50)->nullable()->default(null);

            $table->index(["idContrato"], 'idContrato');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
