<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariogrupoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'usuariogrupo';

    /**
     * Run the migrations.
     * @table usuariogrupo
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('idGrupo')->unsigned();
            $table->integer('idUsuario')->unsigned();

            $table->index(["idGrupo"], 'fk_usuariogrupo_grupo1_idx');

            $table->index(["idUsuario"], 'fk_usuariogrupo_usuario1_idx');


            $table->foreign('idGrupo', 'fk_usuariogrupo_grupo1_idx')
                ->references('idGrupo')->on('grupo')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idUsuario', 'fk_usuariogrupo_usuario1_idx')
                ->references('idUsuario')->on('usuario')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
