<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosSolicitudftalatosTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'productos_solicitudftalatos';

    /**
     * Run the migrations.
     * @table productos_solicitudftalatos
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idSolicitudFtalatos');
            $table->integer('idUsuario')->unsigned();
            $table->integer('idLaboratorio')->unsigned();
            $table->string('tipoSolicitud', 45);
            $table->string('titular', 50);
            $table->string('solicitante', 50);
            $table->string('apoderado');
            $table->string('fabrica_razonSocial')->nullable()->default(null);
            $table->string('fabrica_direccion')->nullable()->default(null);
            $table->integer('fabrica_pais')->nullable()->default(null);
            $table->date('fechaAlta')->default('2014-01-01');
            $table->string('nro_oferta', 50)->nullable()->default(null);
            $table->integer('rol_facturacion')->unsigned();
            $table->integer('rol_facturacion_cuotas')->unsigned();
            $table->integer('rol_facturacion_empresa')->unsigned();
            $table->string('firma')->nullable()->default(null);
            $table->string('aclaracion')->nullable()->default(null);
            $table->tinyInteger('confirmed')->default('0');
            $table->string('ordenCompra')->nullable()->default(null);
            $table->tinyInteger('enviado')->default('0');

            $table->index(["rol_facturacion_empresa"], 'fk_productos_solicitudftalatos_productos_rol3_idx');

            $table->index(["solicitante"], 'fk_productos_solicitudftalatos_empresa2_idx');

            $table->index(["titular"], 'fk_productos_solicitudftalatos_empresa1_idx');

            $table->index(["idLaboratorio"], 'fk_productos_solicitudftalatos_productos_laboratorio1_idx');

            $table->index(["rol_facturacion_cuotas"], 'fk_productos_solicitudftalatos_productos_rol2_idx');

            $table->index(["rol_facturacion"], 'fk_productos_solicitudftalatos_productos_rol1_idx');

            $table->index(["idUsuario"], 'fk_productos_solicitudftalatos_usuario1_idx');


            $table->foreign('rol_facturacion', 'fk_productos_solicitudftalatos_productos_rol1_idx')
                ->references('idRol')->on('productos_rol')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('rol_facturacion_cuotas', 'fk_productos_solicitudftalatos_productos_rol2_idx')
                ->references('idRol')->on('productos_rol')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('rol_facturacion_empresa', 'fk_productos_solicitudftalatos_productos_rol3_idx')
                ->references('idRol')->on('productos_rol')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('titular', 'fk_productos_solicitudftalatos_empresa1_idx')
                ->references('idEmpresa')->on('empresa')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('solicitante', 'fk_productos_solicitudftalatos_empresa2_idx')
                ->references('idEmpresa')->on('empresa')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idLaboratorio', 'fk_productos_solicitudftalatos_productos_laboratorio1_idx')
                ->references('idLaboratorio')->on('productos_laboratorio')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idUsuario', 'fk_productos_solicitudftalatos_usuario1_idx')
                ->references('idUsuario')->on('usuario')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
