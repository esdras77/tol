<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosProductoElectricoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'productos_producto_electrico';

    /**
     * Run the migrations.
     * @table productos_producto_electrico
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idProducto');
            $table->integer('idSolicitudElectricos')->unsigned();
            $table->string('nombre');
            $table->string('modelo');
            $table->string('marca_comercial');
            $table->string('normas_tecnicas');
            $table->integer('cantidad')->nullable()->default(null);
            $table->string('imagen')->nullable()->default(null);
            $table->tinyInteger('certificado')->default('0');
            $table->string('nroCertificado')->nullable()->default(null);

            $table->index(["idSolicitudElectricos"], 'fk_productos_producto_electrico_productos_solicitudelectric_idx');


            $table->foreign('idSolicitudElectricos', 'fk_productos_producto_electrico_productos_solicitudelectric_idx')
                ->references('idSolicitudElectricos')->on('productos_solicitudelectricos')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
