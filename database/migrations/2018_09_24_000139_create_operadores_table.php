<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperadoresTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'operadores';

    /**
     * Run the migrations.
     * @table operadores
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('idLotus', 50)->primary();
            $table->string('idOficina', 32);
            $table->string('idPerson', 32);
            $table->string('numRefTuv', 100);
            $table->string('credentialStatus', 100);
            $table->string('name');
            $table->string('dni', 10);
            $table->tinyInteger('hasDniCopy')->default('0');
            $table->tinyInteger('hasPicture')->default('0');
            $table->text('experience')->nullable()->default(null);
            $table->date('validDateMedicalCert')->nullable()->default(null);
            $table->date('evaluationDate')->nullable()->default(null);
            $table->date('expiration')->nullable()->default(null);
            $table->string('equipment')->nullable()->default(null);
            $table->string('capacity', 100)->nullable()->default(null);
            $table->string('nroOperador', 100)->nullable()->default(null);
            $table->date('certificateGenerated')->nullable()->default(null);
            $table->tinyInteger('new')->default('0');

            $table->index(["idOficina"], 'fk_operadores_oficina1_idx');


            $table->foreign('idOficina', 'fk_operadores_oficina1_idx')
                ->references('idOficina')->on('oficina')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
