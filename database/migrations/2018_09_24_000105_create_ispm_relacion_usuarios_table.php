<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIspmRelacionUsuariosTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'ispm_relacion_usuarios';

    /**
     * Run the migrations.
     * @table ispm_relacion_usuarios
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idRelacionUsuarios');
            $table->integer('idUsuario')->unsigned();
            $table->integer('idUserIspm');

            $table->index(["idUsuario"], 'fk_ispm_relacion_usuarios_usuario1_idx');


            $table->foreign('idUsuario', 'fk_ispm_relacion_usuarios_usuario1_idx')
                ->references('idUsuario')->on('usuario')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
