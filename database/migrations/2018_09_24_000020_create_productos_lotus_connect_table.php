<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosLotusConnectTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'productos_lotus_connect';

    /**
     * Run the migrations.
     * @table productos_lotus_connect
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idProductosLotusConnect');
            $table->string('tipo', 45);
            $table->integer('idSolicitud');
            $table->date('fechaEnvio')->nullable()->default(null);
            $table->string('sistemaCertificacion')->nullable()->default(null);
            $table->string('ID_Categoria_Producto_Grafico', 50)->nullable()->default(null);
            $table->string('tipoSolicitud', 45)->nullable()->default(null);
            $table->string('loggedUser')->nullable()->default(null);
            $table->string('titular', 50);
            $table->string('solicitante', 50);
            $table->string('laboratorio', 100);
            $table->string('servicios')->nullable()->default(null);
            $table->string('nroCertificado', 50)->nullable()->default(null);
            $table->integer('idProducto');
            $table->text('producto');
            $table->string('marca')->nullable()->default(null);
            $table->text('modelo')->nullable()->default(null);
            $table->string('normas')->nullable()->default(null);
            $table->string('codigo_origen', 50)->nullable()->default(null);
            $table->string('codigo_comercio', 50)->nullable()->default(null);
            $table->string('franja_etarea')->nullable()->default(null);
            $table->string('fabrica_razonSocial')->nullable()->default(null);
            $table->string('fabrica_direccion')->nullable()->default(null);
            $table->tinyInteger('procesado')->default('0');
            $table->string('ordenCompra', 100)->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
