<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosSolicitudruedasServicioTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'productos_solicitudruedas_servicio';

    /**
     * Run the migrations.
     * @table productos_solicitudruedas_servicio
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('idSolicitudServicio')->unsigned();
            $table->integer('idServicio')->unsigned();

            $table->index(["idSolicitudServicio"], 'fk_productos_solicitudruedas_servicio_productos_solicitudru_idx');

            $table->index(["idServicio"], 'fk_productos_solicitudruedas_servicio_productos_servicio1_idx');


            $table->foreign('idSolicitudServicio', 'fk_productos_solicitudruedas_servicio_productos_solicitudru_idx')
                ->references('idSolicitudServicio')->on('productos_solicitudruedas')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idServicio', 'fk_productos_solicitudruedas_servicio_productos_servicio1_idx')
                ->references('idServicio')->on('productos_servicio')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
