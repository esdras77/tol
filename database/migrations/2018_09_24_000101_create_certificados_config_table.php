<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCertificadosConfigTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'certificados_config';

    /**
     * Run the migrations.
     * @table certificados_config
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idConfiguracionCertificado');
            $table->integer('idDepartamento')->unsigned()->nullable();
            $table->integer('idUsuario')->unsigned();
            $table->integer('currentYear');
            $table->integer('numero');
            $table->dateTime('fechaEdicion');

            $table->index(["idUsuario"], 'fk_certificados_config_usuario1_idx');

            $table->index(["idDepartamento"], 'fk_certificados_config_departamento1_idx');


            $table->foreign('idDepartamento', 'fk_certificados_config_departamento1_idx')
                ->references('idDepartamento')->on('departamento')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idUsuario', 'fk_certificados_config_usuario1_idx')
                ->references('idUsuario')->on('usuario')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
