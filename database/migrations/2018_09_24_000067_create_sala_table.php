<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalaTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'sala';

    /**
     * Run the migrations.
     * @table sala
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idSala');
            $table->integer('idGrupo')->unsigned();
            $table->string('nombre', 45);
            $table->text('descripcion');
            $table->string('fechaCreacion', 45);

            $table->index(["idGrupo"], 'fk_sala_grupo1_idx');


            $table->foreign('idGrupo', 'fk_sala_grupo1_idx')
                ->references('idGrupo')->on('grupo')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
