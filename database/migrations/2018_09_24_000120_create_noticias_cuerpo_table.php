<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoticiasCuerpoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'noticias_cuerpo';

    /**
     * Run the migrations.
     * @table noticias_cuerpo
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idCuerpo');
            $table->integer('idNoticia')->unsigned();
            $table->integer('idIdioma')->unsigned();
            $table->string('titulo', 100);
            $table->text('cuerpo');

            $table->index(["idIdioma"], 'fk_noticias_cuerpo_idioma1_idx');

            $table->index(["idNoticia"], 'fk_noticias_cuerpo_noticias_cabecera1_idx');


            $table->foreign('idNoticia', 'fk_noticias_cuerpo_noticias_cabecera1_idx')
                ->references('idNoticia')->on('noticias_cabecera')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idIdioma', 'fk_noticias_cuerpo_idioma1_idx')
                ->references('idIdioma')->on('idioma')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
