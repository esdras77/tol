<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerispmxconfiguracionInspeccionTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'customerispmxconfiguracion_inspeccion';

    /**
     * Run the migrations.
     * @table customerispmxconfiguracion_inspeccion
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('idCustomerIspm')->unsigned();
            $table->integer('idUsuario')->unsigned();

            $table->primary(['idCustomerIspm', 'idUsuario'], 'primary_index');

            $table->index(["idUsuario"], 'fk_customerispmxconfiguracion_inspeccion_usuario1_idx');


            $table->foreign('idUsuario', 'fk_customerispmxconfiguracion_inspeccion_usuario1_idx')
                ->references('idUsuario')->on('usuario')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
