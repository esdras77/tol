<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosSolicitudruedasValuesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'productos_solicitudruedas_values';

    /**
     * Run the migrations.
     * @table productos_solicitudruedas_values
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idSolicitudRuedasValues');
            $table->integer('idCategoria')->unsigned();
            $table->integer('idCategoriaInput')->unsigned();
            $table->integer('idSolicitudServicio')->unsigned();
            $table->string('value')->nullable()->default(null);

            $table->index(["idCategoria"], 'fk_productos_solicitudruedas_values_productos_categoria1_idx1');

            $table->index(["idSolicitudServicio"], 'fk_productos_solicitudruedas_values_productos_solicitudrued_idx1');

            $table->index(["idCategoriaInput"], 'fk_productos_solicitudruedas_values_productos_categoria_inp_idx1');


            $table->foreign('idSolicitudServicio', 'fk_productos_solicitudruedas_values_productos_solicitudrued_idx1')
                ->references('idSolicitudServicio')->on('productos_solicitudruedas')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idCategoria', 'fk_productos_solicitudruedas_values_productos_categoria1_idx1')
                ->references('idCategoria')->on('productos_categoria')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idCategoriaInput', 'fk_productos_solicitudruedas_values_productos_categoria_inp_idx1')
                ->references('idCategoriaInput')->on('productos_categoria_input')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
