<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDdFormularioprocesoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'dd_formularioproceso';

    /**
     * Run the migrations.
     * @table dd_formularioproceso
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idFormularioProceso');
            $table->integer('idSocioNegocios')->unsigned();
            $table->integer('idTipoNegocio')->unsigned();
            $table->integer('idDepartamento')->unsigned();
            $table->integer('idFormularioResolucion')->unsigned();
            $table->integer('idDueDiligence')->unsigned()->nullable();
            $table->integer('creador');
            $table->date('fechaCreacion')->nullable()->default(null);
            $table->date('fechaEnvioCuestionario')->nullable()->default(null);

            $table->index(["idFormularioResolucion"], 'fk_dd_formularioproceso_dd_formularioresolucion1_idx');

            $table->index(["idSocioNegocios"], 'fk_dd_formularioproceso_dd_socionegocios1_idx');

            $table->index(["idDepartamento"], 'fk_dd_formularioproceso_departamento1_idx');

            $table->index(["idTipoNegocio"], 'fk_dd_formularioproceso_dd_tiponegocio1_idx');

            $table->index(["idDueDiligence"], 'fk_dd_formularioproceso_dd_duediligence1_idx');

            $table->index(["creador"], 'fk_dd_formularioProceso_usuario1_idx');


            $table->foreign('idSocioNegocios', 'fk_dd_formularioproceso_dd_socionegocios1_idx')
                ->references('idSocioNegocios')->on('dd_socionegocios')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idTipoNegocio', 'fk_dd_formularioproceso_dd_tiponegocio1_idx')
                ->references('idTipoNegocio')->on('dd_tiponegocio')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idDepartamento', 'fk_dd_formularioproceso_departamento1_idx')
                ->references('idDepartamento')->on('departamento')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idFormularioResolucion', 'fk_dd_formularioproceso_dd_formularioresolucion1_idx')
                ->references('idFormularioResolucion')->on('dd_formularioresolucion')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idDueDiligence', 'fk_dd_formularioproceso_dd_duediligence1_idx')
                ->references('idDueDiligence')->on('dd_duediligence')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
