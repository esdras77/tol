<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDdFormulariohistorialTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'dd_formulariohistorial';

    /**
     * Run the migrations.
     * @table dd_formulariohistorial
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idFormularioHistorial');
            $table->integer('idFormularioProceso')->unsigned();
            $table->integer('idFormularioEstado')->unsigned();
            $table->integer('responsable');
            $table->date('fechaEstado');

            $table->index(["idFormularioEstado"], 'fk_dd_formulariohistorial_dd_formularioestado1_idx');

            $table->index(["idFormularioProceso"], 'fk_dd_formulariohistorial_dd_formularioproceso1_idx');


            $table->foreign('idFormularioProceso', 'fk_dd_formulariohistorial_dd_formularioproceso1_idx')
                ->references('idFormularioProceso')->on('dd_formularioproceso')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idFormularioEstado', 'fk_dd_formulariohistorial_dd_formularioestado1_idx')
                ->references('idFormularioEstado')->on('dd_formularioestado')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
