<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosSolicitudruedasTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'productos_solicitudruedas';

    /**
     * Run the migrations.
     * @table productos_solicitudruedas
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idSolicitudServicio');
            $table->integer('idUsuario')->unsigned();
            $table->integer('idDatosCertificacion')->unsigned();
            $table->integer('idTipoDeProducto')->unsigned();
            $table->string('titular', 50);
            $table->string('solicitante', 50);
            $table->string('fabrica_razonSocial', 45);
            $table->string('fabrica_direccion', 45);
            $table->string('fabrica_pais', 45)->nullable()->default(null);
            $table->string('fechaAlta', 45);
            $table->string('nro_oferta', 45)->nullable()->default(null);
            $table->integer('rol_facturacion')->unsigned();
            $table->integer('rol_facturacion_cuotas')->unsigned();
            $table->integer('rol_facturacion_inspeccion')->unsigned();
            $table->string('firma')->nullable()->default(null);
            $table->string('aclaracion', 45)->nullable()->default(null);
            $table->tinyInteger('confirmed')->default('0');
            $table->tinyInteger('sdu')->default('0');
            $table->string('ordenCompra', 45)->nullable()->default(null);
            $table->string('nroCertificado', 45)->nullable()->default(null);
            $table->tinyInteger('qaSystem')->nullable()->default(null);
            $table->tinyInteger('qaISO')->nullable()->default(null);
            $table->string('certOrg')->nullable()->default(null);
            $table->integer('cantidad')->nullable()->default(null);
            $table->tinyInteger('enviado')->default('0');
            $table->string('imagen')->nullable()->default(null);
            $table->text('observaciones')->nullable()->default(null);

            $table->index(["rol_facturacion_cuotas"], 'fk_productos_solicitudruedas_productos_rol2_idx');

            $table->index(["idTipoDeProducto"], 'fk_productos_solicitudruedas_productos_tipodeproducto1_idx1');

            $table->index(["solicitante"], 'fk_productos_solicitudruedas_empresa2_idx');

            $table->index(["rol_facturacion_inspeccion"], 'fk_productos_solicitudruedas_productos_rol3_idx');

            $table->index(["idDatosCertificacion"], 'fk_productos_solicitudruedas_productos_solicitudruedas_dato_idx1');

            $table->index(["idUsuario"], 'fk_productos_solicitudruedas_usuario1_idx1');

            $table->index(["titular"], 'fk_productos_solicitudruedas_empresa1_idx');

            $table->index(["rol_facturacion"], 'fk_productos_solicitudruedas_productos_rol1_idx');


            $table->foreign('idTipoDeProducto', 'fk_productos_solicitudruedas_productos_tipodeproducto1_idx1')
                ->references('idTipoDeProducto')->on('productos_tipodeproducto')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idDatosCertificacion', 'fk_productos_solicitudruedas_productos_solicitudruedas_dato_idx1')
                ->references('idDatosCertificacion')->on('productos_solicitudruedas_datoscertificacion')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idUsuario', 'fk_productos_solicitudruedas_usuario1_idx1')
                ->references('idUsuario')->on('usuario')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('titular', 'fk_productos_solicitudruedas_empresa1_idx')
                ->references('idEmpresa')->on('empresa')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('solicitante', 'fk_productos_solicitudruedas_empresa2_idx')
                ->references('idEmpresa')->on('empresa')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('rol_facturacion', 'fk_productos_solicitudruedas_productos_rol1_idx')
                ->references('idRol')->on('productos_rol')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('rol_facturacion_cuotas', 'fk_productos_solicitudruedas_productos_rol2_idx')
                ->references('idRol')->on('productos_rol')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('rol_facturacion_inspeccion', 'fk_productos_solicitudruedas_productos_rol3_idx')
                ->references('idRol')->on('productos_rol')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
