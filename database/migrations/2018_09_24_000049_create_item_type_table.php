<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemTypeTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'item_type';

    /**
     * Run the migrations.
     * @table item_type
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idItem_type');
            $table->integer('idItem_type_parent')->unsigned();
            $table->string('titulo')->nullable()->default(null);
            $table->text('texto')->nullable()->default(null);
            $table->tinyInteger('critico')->default('0');
            $table->tinyInteger('lleva_fecha')->default('0');
            $table->integer('orden');

            $table->index(["idItem_type_parent"], 'fk_item_type_item_type_parent1_idx');


            $table->foreign('idItem_type_parent', 'fk_item_type_item_type_parent1_idx')
                ->references('idItem_type_parent')->on('item_type_parent')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
