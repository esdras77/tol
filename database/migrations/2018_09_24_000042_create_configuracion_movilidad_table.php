<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfiguracionMovilidadTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'configuracion_movilidad';

    /**
     * Run the migrations.
     * @table configuracion_movilidad
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idConfiguracion');
            $table->integer('idUsuario');
            $table->integer('vencimientoMovilidad');
            $table->integer('proximoAviso')->default('15');

            $table->index(["idUsuario"], 'fk_usuario_has_eess_usuario1');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
