<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArchivoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'archivo';

    /**
     * Run the migrations.
     * @table archivo
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idArchivo');
            $table->integer('idPadre')->nullable();
            $table->integer('idTipoAviso')->default('0')->unsigned();
            $table->integer('aviso')->default('0');
            $table->string('nombre');
            $table->string('url', 512)->nullable()->default(null);
            $table->string('extension', 10)->nullable()->default(null);
            $table->dateTime('fechaModificacion');
            $table->text('descripcion')->nullable()->default(null);

            $table->index(["nombre"], 'nombre');

            $table->index(["idTipoAviso"], 'fk_archivo_tipoaviso1_idx');

            $table->foreign('idTipoAviso', 'fk_archivo_tipoaviso1_idx')
                ->references('idTipoAviso')->on('tipoaviso')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
