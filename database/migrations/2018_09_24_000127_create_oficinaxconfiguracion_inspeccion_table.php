<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOficinaxconfiguracionInspeccionTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'oficinaxconfiguracion_inspeccion';

    /**
     * Run the migrations.
     * @table oficinaxconfiguracion_inspeccion
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('idConfiguracion')->unsigned();
            $table->string('idOficina', 32);
            $table->tinyInteger('inspecciones')->default('0');
            $table->tinyInteger('operadores')->default('0');
            $table->integer('email')->nullable()->default('0');

            $table->index(["idConfiguracion"], 'fk_oficinaxconfiguracion_inspeccion_configuracion_inspeccio_idx');

            $table->index(["idOficina"], 'fk_oficinaxconfiguracion_inspeccion_oficina1_idx');


            $table->foreign('idConfiguracion', 'fk_oficinaxconfiguracion_inspeccion_configuracion_inspeccio_idx')
                ->references('idConfiguracion')->on('configuracion_inspecciones')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idOficina', 'fk_oficinaxconfiguracion_inspeccion_oficina1_idx')
                ->references('idOficina')->on('oficina')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
