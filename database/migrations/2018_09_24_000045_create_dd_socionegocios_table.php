<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDdSocionegociosTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'dd_socionegocios';

    /**
     * Run the migrations.
     * @table dd_socionegocios
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idSocioNegocios');
            $table->string('idDireccion', 32);
            $table->string('razonSocial', 100);
            $table->string('telefono', 45)->nullable()->default(null);
            $table->string('email', 100)->nullable()->default(null);

            $table->index(["idDireccion"], 'fk_dd_socioNegocios_direccion_idx');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
