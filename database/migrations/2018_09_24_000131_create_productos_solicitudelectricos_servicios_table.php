<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosSolicitudelectricosServiciosTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'productos_solicitudelectricos_servicios';

    /**
     * Run the migrations.
     * @table productos_solicitudelectricos_servicios
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('idServicio')->unsigned();
            $table->integer('idSolicitudElectricos')->unsigned();

            $table->index(["idServicio"], 'fk_productos_solicitudelectricos_servicios_productos_servic_idx');

            $table->index(["idSolicitudElectricos"], 'fk_productos_solicitudelectricos_servicios_productos_solici_idx');


            $table->foreign('idServicio', 'fk_productos_solicitudelectricos_servicios_productos_servic_idx')
                ->references('idServicio')->on('productos_servicio')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idSolicitudElectricos', 'fk_productos_solicitudelectricos_servicios_productos_solici_idx')
                ->references('idSolicitudElectricos')->on('productos_solicitudelectricos')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
