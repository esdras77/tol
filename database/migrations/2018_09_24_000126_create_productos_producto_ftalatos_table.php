<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosProductoFtalatosTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'productos_producto_ftalatos';

    /**
     * Run the migrations.
     * @table productos_producto_ftalatos
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idProductoFtalatos');
            $table->integer('idSolicitudFtalatos')->unsigned();
            $table->string('codigo_origen', 50);
            $table->string('codigo_comercio', 50);
            $table->string('nombre');
            $table->text('descripcion');
            $table->tinyInteger('contenido1')->default('1');
            $table->tinyInteger('contenido2')->default('1');
            $table->string('imagen')->nullable()->default(null);

            $table->index(["idSolicitudFtalatos"], 'fk_productos_producto_ftalatos_productos_solicitudftalatos1_idx');


            $table->foreign('idSolicitudFtalatos', 'fk_productos_producto_ftalatos_productos_solicitudftalatos1_idx')
                ->references('idSolicitudFtalatos')->on('productos_solicitudftalatos')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
