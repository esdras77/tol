<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosMemoriatecnicaAlmacenamientoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'productos_memoriatecnica_almacenamiento';

    /**
     * Run the migrations.
     * @table productos_memoriatecnica_almacenamiento
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('idMemoriaTecnica')->unsigned();
            $table->integer('idAlmacenamiento')->unsigned();

            $table->index(["idAlmacenamiento"], 'fk_productos_memoriatecnica_almacenamiento_productos_almace_idx');

            $table->index(["idMemoriaTecnica"], 'fk_productos_memoriatecnica_almacenamiento_productos_memori_idx');


            $table->foreign('idMemoriaTecnica', 'fk_productos_memoriatecnica_almacenamiento_productos_memori_idx')
                ->references('idMemoriaTecnica')->on('productos_memoriatecnica')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idAlmacenamiento', 'fk_productos_memoriatecnica_almacenamiento_productos_almace_idx')
                ->references('idAlmacenamiento')->on('productos_almacenamiento')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
