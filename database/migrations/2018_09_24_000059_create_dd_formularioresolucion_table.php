<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDdFormularioresolucionTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'dd_formularioresolucion';

    /**
     * Run the migrations.
     * @table dd_formularioresolucion
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idFormularioResolucion');
            $table->integer('idTipoResolucion')->unsigned();
            $table->date('fechaResolucion');
            $table->integer('responsable');
            $table->text('observaciones');

            $table->index(["idTipoResolucion"], 'fk_dd_formularioresolucion_dd_tiporesolucion1_idx');


            $table->foreign('idTipoResolucion', 'fk_dd_formularioresolucion_dd_tiporesolucion1_idx')
                ->references('idTipoResolucion')->on('dd_tiporesolucion')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
