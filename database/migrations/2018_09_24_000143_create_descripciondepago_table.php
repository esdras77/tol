<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDescripciondepagoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'descripciondepago';

    /**
     * Run the migrations.
     * @table descripciondepago
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idDescripcionDePago');
            $table->integer('idPago')->unsigned();
            $table->string('numeroTarjeta', 45)->nullable()->default(null);
            $table->integer('cantidadCuotas')->nullable()->default(null);
            $table->string('numeroCheque', 50)->nullable()->default(null);
            $table->integer('cancelacionDias')->nullable()->default(null);
            $table->integer('numeroDocumento')->nullable()->default(null);

            $table->index(["idPago"], 'fk_descripciondepago_pago1_idx');


            $table->foreign('idPago', 'fk_descripciondepago_pago1_idx')
                ->references('idPago')->on('pago')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
