<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemdemenuTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'itemdemenu';

    /**
     * Run the migrations.
     * @table itemdemenu
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idItemMenu');
            $table->string('url')->nullable()->default(null);
            $table->integer('padre');
            $table->integer('posicion');
            $table->string('descripcion');
            $table->tinyInteger('activo');
            $table->string('icono')->nullable()->default(null);
            $table->string('imagen')->nullable()->default(null);
            $table->text('textoweb')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
