<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaxconfiguracionCertificacionTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'empresaxconfiguracion_certificacion';

    /**
     * Run the migrations.
     * @table empresaxconfiguracion_certificacion
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('idEmpresa', 50);
            $table->integer('idConfiguracion')->unsigned();

            $table->index(["idConfiguracion"], 'fk_empresaxconfiguracion_certificacion_configuracion_certif_idx');

            $table->index(["idEmpresa"], 'fk_empresaxconfiguracion_certificacion_empresa1_idx');


            $table->foreign('idEmpresa', 'fk_empresaxconfiguracion_certificacion_empresa1_idx')
                ->references('idEmpresa')->on('empresa')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idConfiguracion', 'fk_empresaxconfiguracion_certificacion_configuracion_certif_idx')
                ->references('idConfiguracion')->on('configuracion_certificados')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
