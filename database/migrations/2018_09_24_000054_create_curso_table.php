<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCursoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'curso';

    /**
     * Run the migrations.
     * @table curso
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idCurso');
            $table->float('costo')->nullable()->default(null);
            $table->string('descripcion')->nullable()->default(null);
            $table->tinyInteger('habilitadoPago')->nullable()->default('0');
            $table->dateTime('fechaDesde')->nullable()->default(null);
            $table->string('nombre')->nullable()->default(null);
            $table->string('estado', 45)->nullable()->default('1');
            $table->tinyInteger('online')->nullable()->default('0');
            $table->dateTime('fechaHasta')->nullable()->default(null);
            $table->integer('cupo')->nullable()->default(null);
            $table->date('fechaCancelacion')->nullable()->default(null);
            $table->text('comentarioCancelacion')->nullable()->default(null);
            $table->string('code', 40)->nullable()->default(null)->comment('id del curso en dokeos. Es un dato alfanumérico con el mismo nombre en la tabla "course" ');
            $table->string('locationCurso')->nullable()->default(null);
            $table->string('pais')->nullable()->default(null);
            $table->integer('horas')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
