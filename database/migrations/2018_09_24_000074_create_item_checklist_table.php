<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemChecklistTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'item_checklist';

    /**
     * Run the migrations.
     * @table item_checklist
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idCheckList');
            $table->integer('idItem_type')->unsigned();
            $table->integer('idItem_type_parent')->unsigned()->nullable();
            $table->integer('idFichaMovilidad')->unsigned();
            $table->tinyInteger('cumple')->default('0');
            $table->date('fecha');
            $table->text('observaciones')->nullable()->default(null);
            $table->text('foto')->nullable()->default(null)->comment('url a imagen');

            $table->index(["idFichaMovilidad"], 'fk_item_checklist_ficha_movilidad1_idx');

            $table->index(["idItem_type"], 'fk_item_checklist_item_type1_idx');

            $table->index(["idItem_type_parent"], 'fk_item_checklist_item_type_parent1_idx');


            $table->foreign('idItem_type_parent', 'fk_item_checklist_item_type_parent1_idx')
                ->references('idItem_type_parent')->on('item_type_parent')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idItem_type', 'fk_item_checklist_item_type1_idx')
                ->references('idItem_type')->on('item_type')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idFichaMovilidad', 'fk_item_checklist_ficha_movilidad1_idx')
                ->references('idFichaMovilidad')->on('ficha_movilidad')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
