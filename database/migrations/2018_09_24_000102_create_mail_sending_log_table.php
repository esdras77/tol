<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailSendingLogTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'mail_sending_log';

    /**
     * Run the migrations.
     * @table mail_sending_log
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idMailSent');
            $table->integer('idUsuario')->unsigned();
            $table->date('sentOn');
            $table->string('recipients');
            $table->string('subject');
            $table->text('body')->nullable()->default(null);

            $table->index(["idUsuario"], 'fk_mail_sending_log_usuario1_idx');


            $table->foreign('idUsuario', 'fk_mail_sending_log_usuario1_idx')
                ->references('idUsuario')->on('usuario')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
