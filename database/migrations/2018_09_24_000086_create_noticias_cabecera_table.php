<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoticiasCabeceraTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'noticias_cabecera';

    /**
     * Run the migrations.
     * @table noticias_cabecera
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idNoticia');
            $table->integer('idUsuario')->unsigned();
            $table->date('fechaPublicacion');
            $table->date('fechaVencimiento')->nullable()->default(null);
            $table->tinyInteger('principal')->default('0');
            $table->string('descripcion', 50);

            $table->index(["idUsuario"], 'fk_noticias_cabecera_usuario1_idx');


            $table->foreign('idUsuario', 'fk_noticias_cabecera_usuario1_idx')
                ->references('idUsuario')->on('usuario')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
