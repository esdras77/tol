<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestriccionesConfiguracionCertificadoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'restricciones_configuracion_certificado';

    /**
     * Run the migrations.
     * @table restricciones_configuracion_certificado
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idRestriccion');
            $table->string('idEmpresa', 50);
            $table->integer('idConfiguracion')->unsigned();
            $table->string('campo', 100);
            $table->string('filtro', 45);
            $table->string('valor', 100);
            $table->string('nexo', 45)->nullable()->default(null);

            $table->index(["idConfiguracion"], 'fk_restricciones_configuracion_certificado_configuracion_ce_idx');

            $table->index(["idEmpresa"], 'fk_restricciones_configuracion_certificado_empresa1_idx');


            $table->foreign('idEmpresa', 'fk_restricciones_configuracion_certificado_empresa1_idx')
                ->references('idEmpresa')->on('empresa')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idConfiguracion', 'fk_restricciones_configuracion_certificado_configuracion_ce_idx')
                ->references('idConfiguracion')->on('configuracion_certificados')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
