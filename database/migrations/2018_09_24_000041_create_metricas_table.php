<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetricasTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'metricas';

    /**
     * Run the migrations.
     * @table metricas
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idMetrica');
            $table->string('module');
            $table->string('controller');
            $table->string('action');
            $table->integer('who')->comment('quien hace');
            $table->string('doing')->comment('grupo de acciones que puede realizar el usuario');
            $table->string('over')->comment('nombre de la tabla');
            $table->text('with')->comment('parametros de cambio');
            $table->text('from')->nullable()->default(null)->comment('parametros previos al cambio que se realiza');
            $table->dateTime('on')->comment('fecha de la asignacion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
