<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfiguracionCertificadosTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'configuracion_certificados';

    /**
     * Run the migrations.
     * @table configuracion_certificados
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idConfiguracion');
            $table->integer('idUsuario')->unsigned();
            $table->integer('vencimientoCertificado')->nullable()->default(null);
            $table->integer('proximoAviso')->nullable()->default(null);

            $table->index(["idUsuario"], 'fk_configuracion_certificados_usuario1_idx');


            $table->foreign('idUsuario', 'fk_configuracion_certificados_usuario1_idx')
                ->references('idUsuario')->on('usuario')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
