<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDdItembusquedaTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'dd_itembusqueda';

    /**
     * Run the migrations.
     * @table dd_itembusqueda
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idItemBusqueda');
            $table->integer('idTipoItemBusqueda')->unsigned();
            $table->integer('idFormularioProceso')->unsigned();
            $table->string('fuente');
            $table->string('archivo')->nullable()->default(null);
            $table->text('observacion')->nullable()->default(null);

            $table->index(["idFormularioProceso"], 'fk_dd_itembusqueda_dd_formularioproceso1_idx');

            $table->index(["idTipoItemBusqueda"], 'fk_dd_itembusqueda_dd_tipoitembusqueda1_idx');


            $table->foreign('idTipoItemBusqueda', 'fk_dd_itembusqueda_dd_tipoitembusqueda1_idx')
                ->references('idTipoItemBusqueda')->on('dd_tipoitembusqueda')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idFormularioProceso', 'fk_dd_itembusqueda_dd_formularioproceso1_idx')
                ->references('idFormularioProceso')->on('dd_formularioproceso')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
