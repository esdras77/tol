<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentocertificadoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'documentocertificado';

    /**
     * Run the migrations.
     * @table documentocertificado
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('idLotus', 32);
            $table->string('idEmpresa', 50);
            $table->primary(['idLotus', 'idEmpresa']);
            $table->string('estado', 50);
            $table->string('tipoCertificado', 50)->nullable()->default(null);
            $table->string('nroCertificado', 32);
            $table->string('idTipoCertificado', 45)->nullable()->default(null);
            $table->string('revisionCertificado', 45)->nullable()->default(null);
            $table->date('fechaEmision');
            $table->date('fechaRefSeguimiento')->nullable()->default(null);
            $table->string('titularCertificado');
            $table->string('producto')->nullable()->default(null);
            $table->string('marca')->nullable()->default(null);
            $table->text('modelo')->nullable()->default(null);
            $table->date('fechaValidez')->nullable()->default(null);
            $table->string('agente')->nullable()->default(null);
            $table->date('fechaValidezJuguetes')->nullable()->default(null);

            $table->index(["idEmpresa"], 'fk_documentocertificado_empresa1_idx');


            $table->foreign('idEmpresa', 'fk_documentocertificado_empresa1_idx')
                ->references('idEmpresa')->on('empresa')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
