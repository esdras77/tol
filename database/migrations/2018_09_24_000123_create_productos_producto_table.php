<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosProductoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'productos_producto';

    /**
     * Run the migrations.
     * @table productos_producto
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idProducto');
            $table->integer('idSolicitudServicio')->unsigned();
            $table->string('nombre');
            $table->string('modelo');
            $table->string('marca_comercial');
            $table->string('normas_tecnicas');
            $table->string('franja_etarea_prevista')->nullable()->default(null);
            $table->tinyInteger('ftalatos')->default('0');
            $table->tinyInteger('relacionado_ftalatos')->default('0');
            $table->integer('cantidad')->nullable()->default(null);
            $table->string('imagen')->nullable()->default(null);

            $table->index(["idSolicitudServicio"], 'fk_productos_producto_productos_solicitudservicio1_idx');


            $table->foreign('idSolicitudServicio', 'fk_productos_producto_productos_solicitudservicio1_idx')
                ->references('idSolicitudServicio')->on('productos_solicitudservicio')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
