<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuarioEnOficinaTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'usuario_en_oficina';

    /**
     * Run the migrations.
     * @table usuario_en_oficina
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('idUsuario')->unsigned();
            $table->string('idOficina', 32);
            $table->tinyInteger('configuraMail')->default('0');

            $table->index(["idOficina"], 'fk_usuario_en_oficina_oficina1_idx');

            $table->index(["idUsuario"], 'fk_usuario_en_oficina_usuario1_idx');


            $table->foreign('idUsuario', 'fk_usuario_en_oficina_usuario1_idx')
                ->references('idUsuario')->on('usuario')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idOficina', 'fk_usuario_en_oficina_oficina1_idx')
                ->references('idOficina')->on('oficina')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
