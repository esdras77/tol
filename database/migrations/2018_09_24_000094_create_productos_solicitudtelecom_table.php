<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosSolicitudtelecomTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'productos_solicitudtelecom';

    /**
     * Run the migrations.
     * @table productos_solicitudtelecom
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idSolicitudTelecom');
            $table->integer('idUsuario')->unsigned();
            $table->integer('idTipoDeProducto')->unsigned();
            $table->string('titular', 50);
            $table->string('solicitante', 50);
            $table->string('fabrica_razonSocial');
            $table->string('fabrica_direccion');
            $table->integer('fabrica_pais')->nullable()->default(null);
            $table->date('fechaAlta');
            $table->string('internacional')->nullable()->default(null);
            $table->integer('rol_facturacion')->unsigned();
            $table->integer('rol_facturacion_cuotas')->unsigned();
            $table->integer('rol_facturacion_inspeccion')->unsigned();
            $table->string('nro_oferta', 45)->nullable()->default(null);
            $table->string('firma')->nullable()->default(null);;
            $table->string('aclaracion')->nullable()->default(null);;
            $table->tinyInteger('confirmed')->default('0');
            $table->string('ordenCompra')->nullable()->default(null);
            $table->tinyInteger('enviado')->default('0');

            $table->index(["rol_facturacion"], 'fk_productos_solicitudtelecom_productos_rol1_idx');

            $table->index(["rol_facturacion_inspeccion"], 'fk_productos_solicitudtelecom_productos_rol3_idx');

            $table->index(["idTipoDeProducto"], 'fk_productos_solicitudtelecom_productos_tipodeproducto1_idx');

            $table->index(["idUsuario"], 'fk_productos_solicitudtelecom_usuario1_idx');

            $table->index(["titular"], 'fk_productos_solicitudtelecom_empresa1_idx');

            $table->index(["solicitante"], 'fk_productos_solicitudtelecom_empresa2_idx');

            $table->index(["rol_facturacion_cuotas"], 'fk_productos_solicitudtelecom_productos_rol2_idx');


            $table->foreign('idTipoDeProducto', 'fk_productos_solicitudtelecom_productos_tipodeproducto1_idx')
                ->references('idTipoDeProducto')->on('productos_tipodeproducto')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idUsuario', 'fk_productos_solicitudtelecom_usuario1_idx')
                ->references('idUsuario')->on('usuario')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('titular', 'fk_productos_solicitudtelecom_empresa1_idx')
                ->references('idEmpresa')->on('empresa')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('solicitante', 'fk_productos_solicitudtelecom_empresa2_idx')
                ->references('idEmpresa')->on('empresa')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('rol_facturacion', 'fk_productos_solicitudtelecom_productos_rol1_idx')
                ->references('idRol')->on('productos_rol')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('rol_facturacion_cuotas', 'fk_productos_solicitudtelecom_productos_rol2_idx')
                ->references('idRol')->on('productos_rol')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('rol_facturacion_inspeccion', 'fk_productos_solicitudtelecom_productos_rol3_idx')
                ->references('idRol')->on('productos_rol')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
