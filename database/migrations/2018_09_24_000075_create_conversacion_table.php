<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConversacionTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'conversacion';

    /**
     * Run the migrations.
     * @table conversacion
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idConversacion');
            $table->integer('idSala')->unsigned();
            $table->string('tema');
            $table->text('introduccion');
            $table->integer('creador');
            $table->date('fechaCreacion');
            $table->tinyInteger('estado')->default('1');

            $table->index(["idSala"], 'fk_conversacion_sala1_idx');

            $table->index(["creador"], 'creador');


            $table->foreign('idSala', 'fk_conversacion_sala1_idx')
                ->references('idSala')->on('sala')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
