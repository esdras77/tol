<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosTipodeproductoServicioTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'productos_tipodeproducto_servicio';

    /**
     * Run the migrations.
     * @table productos_tipodeproducto_servicio
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('idTipoDeProducto')->unsigned();
            $table->integer('idServicio')->unsigned();

            $table->index(["idServicio"], 'fk_productos_tipodeproducto_servicio_productos_servicio1_idx');

            $table->index(["idTipoDeProducto"], 'fk_productos_tipodeproducto_servicio_productos_tipodeproduc_idx');


            $table->foreign('idTipoDeProducto', 'fk_productos_tipodeproducto_servicio_productos_tipodeproduc_idx')
                ->references('idTipoDeProducto')->on('productos_tipodeproducto')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idServicio', 'fk_productos_tipodeproducto_servicio_productos_servicio1_idx')
                ->references('idServicio')->on('productos_servicio')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
