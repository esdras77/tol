<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailtagreplacementTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'mailtagreplacement';

    /**
     * Run the migrations.
     * @table mailtagreplacement
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idTagReplacement');
            $table->integer('idIdioma')->unsigned();
            $table->string('placeholder');
            $table->string('field')->nullable()->default(null);
            $table->string('table')->nullable()->default(null);
            $table->string('formula')->nullable()->default(null);

            $table->index(["idIdioma"], 'fk_mailtagreplacement_idioma1_idx');


            $table->foreign('idIdioma', 'fk_mailtagreplacement_idioma1_idx')
                ->references('idIdioma')->on('idioma')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
