<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuItemdemenuTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'menu_itemdemenu';

    /**
     * Run the migrations.
     * @table menu_itemdemenu
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('idItemMenu')->unsigned();
            $table->integer('idMenu')->unsigned();

            $table->index(["idMenu"], 'fk_menu_itemdemenu_menu1_idx');

            $table->foreign('idMenu', 'fk_menu_itemdemenu_menu1_idx')
                ->references('idMenu')->on('menu')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
