<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentoextensionTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'documentoextension';

    /**
     * Run the migrations.
     * @table documentoextension
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('idLotus', 32);
            $table->string('idEmpresa', 50);
            $table->primary(['idLotus', 'idEmpresa']);
            $table->string('numeroExtension');
            $table->dateTime('fechaEmision');
            $table->string('numeroCertificado');
            $table->string('titularCertificado');
            $table->string('titularExtension');
            $table->string('producto');
            $table->string('marca');
            $table->text('modelo');
            $table->text('expSecretaria')->nullable()->default(null);
            $table->string('estado', 50);
            $table->string('agente')->nullable()->default(null);

            $table->index(["idEmpresa"], 'fk_documentoextension_empresa1_idx');


            $table->foreign('idEmpresa', 'fk_documentoextension_empresa1_idx')
                ->references('idEmpresa')->on('empresa')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
