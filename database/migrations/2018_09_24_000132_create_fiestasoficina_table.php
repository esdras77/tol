<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFiestasoficinaTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'fiestasoficina';

    /**
     * Run the migrations.
     * @table fiestasoficina
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('idOficina', 32)->unique();
            $table->integer('idFiesta')->unsigned();

            $table->index(["idOficina"], 'fk_fiestasoficina_oficina1_idx');

            $table->index(["idFiesta"], 'fk_fiestasoficina_fiesta1_idx');


            $table->foreign('idOficina', 'fk_fiestasoficina_oficina1_idx')
                ->references('idOficina')->on('oficina')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idFiesta', 'fk_fiestasoficina_fiesta1_idx')
                ->references('idFiesta')->on('fiesta')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
