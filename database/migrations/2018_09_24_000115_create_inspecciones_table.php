<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInspeccionesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'inspecciones';

    /**
     * Run the migrations.
     * @table inspecciones
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('idLotus')->primary();
            $table->string('idOficina', 32);
            $table->string('idLotusEquipo', 32);
            $table->string('numRefTuv', 100);
            $table->string('numCorrelatividad', 100)->nullable()->default(null);
            $table->string('tipoEquipo');
            $table->string('subTipoEquipo')->nullable()->default(null);
            $table->string('fabricante')->nullable()->default(null);
            $table->string('modelo', 100)->nullable()->default(null);
            $table->string('numSerie', 100)->nullable()->default(null);
            $table->string('anioFabricacion', 4)->nullable()->default(null);
            $table->string('numInterno', 100)->nullable()->default(null);
            $table->date('fechaInspeccion')->nullable()->default(null);
            $table->string('lugarInspeccion')->nullable()->default(null);
            $table->string('inspector')->nullable()->default(null);
            $table->string('resultado', 50)->nullable()->default(null);
            $table->date('fechaVencimiento')->nullable()->default(null);
            $table->string('numOblea', 100)->nullable()->default(null);
            $table->tinyInteger('new')->default('0');

            $table->index(["idOficina"], 'fk_inspecciones_oficina1_idx');


            $table->foreign('idOficina', 'fk_inspecciones_oficina1_idx')
                ->references('idOficina')->on('oficina')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
