<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFichaMovilidadTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'ficha_movilidad';

    /**
     * Run the migrations.
     * @table ficha_movilidad
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idFichaMovilidad');
            $table->integer('idEess')->unsigned();
            $table->string('tp', 15)->nullable()->default(null);
            $table->integer('sello_acreditacion')->nullable()->default(null);
            $table->integer('sello_rechazo')->nullable()->default(null);
            $table->string('subgerencia')->nullable()->default(null);
            $table->string('unidad')->nullable()->default(null);
            $table->string('conductor')->nullable()->default(null);
            $table->string('conductor_secundario')->nullable()->default(null);
            $table->string('propiedad', 15)->nullable()->default(null);
            $table->string('propietario')->nullable()->default(null);
            $table->string('marca')->nullable()->default(null);
            $table->string('modelo')->nullable()->default(null);
            $table->string('tipo_vehiculo')->nullable()->default(null);
            $table->string('patente', 15)->nullable()->default(null);
            $table->date('fecha')->nullable()->default(null);
            $table->date('fecha_vencimiento')->nullable()->default(null);
            $table->integer('km')->nullable()->default(null);
            $table->integer('ano')->nullable()->default(null);
            $table->string('nombre_subcontratista')->nullable()->default(null);
            $table->text('foto_adelante')->nullable()->default(null);
            $table->text('foto_atras')->nullable()->default(null);
            $table->tinyInteger('vigente')->nullable()->default(null);
            $table->tinyInteger('retirado')->nullable()->default(null);

            $table->index(["idEess"], 'fk_ficha_movilidad_eess1_idx');


            $table->foreign('idEess', 'fk_ficha_movilidad_eess1_idx')
                ->references('idEess')->on('eess')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
