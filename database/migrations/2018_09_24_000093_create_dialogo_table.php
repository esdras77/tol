<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDialogoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'dialogo';

    /**
     * Run the migrations.
     * @table dialogo
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idDialogo');
            $table->integer('idConversacion')->unsigned();
            $table->text('comentario');
            $table->integer('interlocutor');
            $table->date('fechaComentario');

            $table->index(["interlocutor"], 'interlocutor');

            $table->index(["idConversacion"], 'fk_dialogo_conversacion1_idx');


            $table->foreign('idConversacion', 'fk_dialogo_conversacion1_idx')
                ->references('idConversacion')->on('conversacion')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
