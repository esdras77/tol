<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificacionesenforosTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'notificacionesenforos';

    /**
     * Run the migrations.
     * @table notificacionesenforos
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idNotificacion');
            $table->integer('idConversacion')->unsigned();
            $table->integer('idUsuario')->unsigned();
            $table->tinyInteger('recibirPrivados')->default('1');
            $table->tinyInteger('recibirNotificacionesAdmin')->default('1');
            $table->tinyInteger('recibirNotificacionesComment')->default('1');

            $table->index(["idConversacion"], 'fk_notificacionesenforos_conversacion1_idx');

            $table->index(["idUsuario"], 'fk_notificacionesenforos_usuario1_idx');


            $table->foreign('idConversacion', 'fk_notificacionesenforos_conversacion1_idx')
                ->references('idConversacion')->on('conversacion')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idUsuario', 'fk_notificacionesenforos_usuario1_idx')
                ->references('idUsuario')->on('usuario')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
