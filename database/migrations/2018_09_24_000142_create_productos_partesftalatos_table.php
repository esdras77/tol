<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosPartesftalatosTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'productos_partesftalatos';

    /**
     * Run the migrations.
     * @table productos_partesftalatos
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idPartesFtalatos');
            $table->integer('idProductoFtalatos')->unsigned();
            $table->string('nombre');

            $table->index(["idProductoFtalatos"], 'fk_productos_partesftalatos_productos_producto_ftalatos1_idx');


            $table->foreign('idProductoFtalatos', 'fk_productos_partesftalatos_productos_producto_ftalatos1_idx')
                ->references('idProductoFtalatos')->on('productos_producto_ftalatos')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
