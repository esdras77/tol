<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosProductoTelecomTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'productos_producto_telecom';

    /**
     * Run the migrations.
     * @table productos_producto_telecom
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idProducto');
            $table->integer('idSolicitudTelecom')->unsigned();
            $table->string('nombre');
            $table->string('modelo');
            $table->string('marca_comercial');
            $table->string('consiste');

            $table->index(["idSolicitudTelecom"], 'fk_productos_producto_telecom_productos_solicitudtelecom1_idx');


            $table->foreign('idSolicitudTelecom', 'fk_productos_producto_telecom_productos_solicitudtelecom1_idx')
                ->references('idSolicitudTelecom')->on('productos_solicitudtelecom')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
