<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuarioTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'usuario';

    /**
     * Run the migrations.
     * @table usuario
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idUsuario');
            $table->integer('idDepartamento')->unsigned()->nullable();
            $table->integer('idPais')->unsigned();
            $table->integer('idPerfil')->unsigned();
            $table->string('idDireccion', 32)->nullable();
            $table->string('username')->nullable()->default(null);
            $table->string('password', 15)->nullable()->default(null);
            $table->string('nombre');
            $table->string('apellido')->nullable()->default(null);
            $table->dateTime('fechaNacimiento');
            $table->dateTime('inicioActividades');
            $table->dateTime('ultimoLogin')->nullable()->default(null);
            $table->string('email')->nullable()->default(null);
            $table->string('telefono', 25)->nullable()->default(null);
            $table->string('fax', 25)->nullable()->default(null);
            $table->string('celular', 25)->nullable()->default(null);
            $table->integer('responsable')->nullable()->default(null);
            $table->integer('aceptaCondiciones')->nullable()->default('0');
            $table->string('cargo')->nullable()->default(null);
            $table->string('firma')->nullable()->default(null);
            $table->text('request')->nullable()->default(null);

            $table->index(["idPais"], 'fk_usuario_pais1_idx');

            $table->index(["idPerfil"], 'fk_usuario_perfil1_idx');

            $table->index(["idDireccion"], 'fk_usuario_direccion1_idx');

            $table->index(["idDepartamento"], 'fk_usuario_departamento_idx');


            $table->foreign('idDepartamento', 'fk_usuario_departamento_idx')
                ->references('idDepartamento')->on('departamento')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idPais', 'fk_usuario_pais1_idx')
                ->references('idPais')->on('pais')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idPerfil', 'fk_usuario_perfil1_idx')
                ->references('idPerfil')->on('perfil')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idDireccion', 'fk_usuario_direccion1_idx')
                ->references('idDireccion')->on('direccion')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
