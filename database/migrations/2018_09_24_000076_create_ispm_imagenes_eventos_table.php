<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIspmImagenesEventosTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'ispm_imagenes_eventos';

    /**
     * Run the migrations.
     * @table ispm_imagenes_eventos
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idImagenesEventos');
            $table->integer('idEventoInspeccion')->unsigned();
            $table->string('url');
            $table->string('nombre')->nullable()->default(null);
            $table->text('descripcion')->nullable()->default(null);

            $table->index(["idEventoInspeccion"], 'fk_ispm_imagenes_eventos_ispm_eventos_inspeccion1_idx');


            $table->foreign('idEventoInspeccion', 'fk_ispm_imagenes_eventos_ispm_eventos_inspeccion1_idx')
                ->references('idEventoInspeccion')->on('ispm_eventos_inspeccion')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
