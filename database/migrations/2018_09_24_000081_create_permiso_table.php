<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermisoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'permiso';

    /**
     * Run the migrations.
     * @table permiso
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idPermiso');
            $table->integer('idArchivo')->unsigned();
            $table->integer('idUsuario')->unsigned();
            $table->tinyInteger('admin')->default('0');
            $table->tinyInteger('read')->default('0');
            $table->tinyInteger('download')->default('0');
            $table->tinyInteger('delete')->default('0');

            $table->index(["idArchivo"], 'fk_permiso_archivo1_idx');

            $table->index(["idUsuario"], 'fk_permiso_usuario1_idx');


            $table->foreign('idArchivo', 'fk_permiso_archivo1_idx')
                ->references('idArchivo')->on('archivo')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idUsuario', 'fk_permiso_usuario1_idx')
                ->references('idUsuario')->on('usuario')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
