<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'pago';

    /**
     * Run the migrations.
     * @table pago
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idPago');
            $table->integer('idCompra')->unsigned();
            $table->integer('idTipoDePago')->unsigned();
            $table->date('date');
            $table->string('status', 50)->nullable()->default(null);
            $table->string('nro_factura')->nullable()->default(null);
            $table->string('nro_recibo')->nullable()->default(null);
            $table->string('idOrden')->nullable()->default(null);
            $table->string('estadoOrden')->nullable()->default(null);
            $table->text('extraParam')->nullable()->default(null);
            $table->dateTime('ultConsulta')->nullable()->default(null);
            $table->string('idTransaction')->nullable()->default(null);
            $table->text('estadoOrdenExplicacion')->nullable()->default(null);
            $table->string('finalValue')->nullable()->default(null);
            $table->string('cuotas')->nullable()->default(null);

            $table->index(["idTipoDePago"], 'fk_pago_tipodepago1_idx');

            $table->index(["idCompra"], 'fk_pago_compra1_idx');


            $table->foreign('idTipoDePago', 'fk_pago_tipodepago1_idx')
                ->references('idTipoDePago')->on('tipodepago')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idCompra', 'fk_pago_compra1_idx')
                ->references('idCompra')->on('compra')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
