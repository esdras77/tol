<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosInputValuesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'productos_input_values';

    /**
     * Run the migrations.
     * @table productos_input_values
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idInputValues');
            $table->integer('idCategoriaInput')->unsigned();
            $table->string('nombre');
            $table->text('descripcion')->nullable()->default(null);
            $table->tinyInteger('default')->default('0');

            $table->index(["idCategoriaInput"], 'fk_productos_input_values_productos_categoria_input1_idx');


            $table->foreign('idCategoriaInput', 'fk_productos_input_values_productos_categoria_input1_idx')
                ->references('idCategoriaInput')->on('productos_categoria_input')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
