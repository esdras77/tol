<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemscompraTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'itemsCompra';

    /**
     * Run the migrations.
     * @table itemsCompra
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('idItemDeCompra')->unsigned();
            $table->integer('idCompra')->unsigned();

            $table->index(["idCompra"], 'fk_itemsCompra_compra1_idx');

            $table->index(["idItemDeCompra"], 'fk_itemsCompra_itemDeCompra1_idx');


            $table->foreign('idItemDeCompra', 'fk_itemsCompra_itemDeCompra1_idx')
                ->references('idItemDeCompra')->on('itemDeCompra')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idCompra', 'fk_itemsCompra_compra1_idx')
                ->references('idCompra')->on('compra')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
