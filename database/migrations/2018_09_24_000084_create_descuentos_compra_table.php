<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDescuentosCompraTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'descuentos_compra';

    /**
     * Run the migrations.
     * @table descuentos_compra
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('idItemDeCompra')->unsigned();
            $table->integer('idUsuario')->unsigned();
            $table->date('Fecha')->nullable()->default(null);
            $table->decimal('Monto', 10, 2)->nullable()->default(null);
            $table->text('Detalle')->nullable()->default(null);
            $table->decimal('porcentaje', 10, 2)->nullable()->default(null);

            $table->index(["idUsuario"], 'fk_descuentos_compra_usuario1_idx');

            $table->index(["idItemDeCompra"], 'fk_descuentos_compra_itemDeCompra1_idx');


            $table->primary(['idItemDeCompra', 'idUsuario']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
