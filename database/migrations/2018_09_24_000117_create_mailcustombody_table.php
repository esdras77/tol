<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailcustombodyTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'mailcustombody';

    /**
     * Run the migrations.
     * @table mailcustombody
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idCustomBody');
            $table->integer('idCustom')->unsigned();
            $table->integer('idIdioma')->unsigned();
            $table->text('text');
            $table->string('subject');
            $table->string('attachment')->nullable()->default(null);

            $table->index(["idCustom"], 'fk_mailcustombody_mailcustom1_idx');

            $table->index(["idIdioma"], 'fk_mailcustombody_idioma1_idx');


            $table->foreign('idCustom', 'fk_mailcustombody_mailcustom1_idx')
                ->references('idCustom')->on('mailcustom')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idIdioma', 'fk_mailcustombody_idioma1_idx')
                ->references('idIdioma')->on('idioma')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
