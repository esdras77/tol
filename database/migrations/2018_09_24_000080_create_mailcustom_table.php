<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailcustomTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'mailcustom';

    /**
     * Run the migrations.
     * @table mailcustom
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idCustom');
            $table->integer('idUsuario')->unsigned()->nullable();
            $table->integer('idModuloRelevante')->unsigned();
            $table->string('situation', 50);
            $table->dateTime('editedOn')->nullable()->default(null);

            $table->index(["idUsuario"], 'fk_mailcustom_usuario1_idx');

            $table->index(["idModuloRelevante"], 'fk_mailcustom_modulosrelevantes1_idx');


            $table->foreign('idUsuario', 'fk_mailcustom_usuario1_idx')
                ->references('idUsuario')->on('usuario')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idModuloRelevante', 'fk_mailcustom_modulosrelevantes1_idx')
                ->references('idModuloRelevante')->on('modulosrelevantes')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
