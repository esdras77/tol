<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHabilitacionXEmpresaTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'habilitacion_x_empresa';

    /**
     * Run the migrations.
     * @table habilitacion_x_empresa
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idHabilitacionEmpresa');
            $table->string('idEmpresa', 50);
            $table->string('numRefTuv', 45);
            $table->string('correlatividad', 45)->nullable()->default(null);

            $table->index(["idEmpresa"], 'fk_habilitacion_x_empresa_empresa1_idx');


            $table->foreign('idEmpresa', 'fk_habilitacion_x_empresa_empresa1_idx')
                ->references('idEmpresa')->on('empresa')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
