<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartamentoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'departamento';

    /**
     * Run the migrations.
     * @table departamento
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idDepartamento');
            $table->integer('idPais')->unsigned();
            $table->integer('administrador')->default('0');
            $table->string('nombre');
            $table->string('prefix', 5);
            $table->text('descripcion')->nullable()->default(null);

            $table->index(["administrador"], 'administrador');

            $table->index(["idPais"], 'fk_departamento_pais1_idx');


            $table->foreign('idPais', 'fk_departamento_pais1_idx')
                ->references('idPais')->on('pais')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
