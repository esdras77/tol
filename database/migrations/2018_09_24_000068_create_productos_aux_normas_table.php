<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosAuxNormasTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'productos_aux_normas';

    /**
     * Run the migrations.
     * @table productos_aux_normas
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idNorma');
            $table->integer('idTipoDeProducto')->unsigned();
            $table->string('nombre');
            $table->text('descripcion')->nullable()->default(null);

            $table->index(["idTipoDeProducto"], 'fk_productos_aux_normas_productos_tipodeproducto1_idx');


            $table->foreign('idTipoDeProducto', 'fk_productos_aux_normas_productos_tipodeproducto1_idx')
                ->references('idTipoDeProducto')->on('productos_tipodeproducto')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
