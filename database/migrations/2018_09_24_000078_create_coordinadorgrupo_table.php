<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoordinadorgrupoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'coordinadorgrupo';

    /**
     * Run the migrations.
     * @table coordinadorgrupo
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('idUsuario')->unsigned();
            $table->integer('idGrupo')->unsigned();

            $table->index(["idUsuario"], 'fk_coordinadorgrupo_usuario1_idx');

            $table->index(["idGrupo"], 'fk_coordinadorgrupo_grupo1_idx');


            $table->foreign('idUsuario', 'fk_coordinadorgrupo_usuario1_idx')
                ->references('idUsuario')->on('usuario')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idGrupo', 'fk_coordinadorgrupo_grupo1_idx')
                ->references('idGrupo')->on('grupo')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
