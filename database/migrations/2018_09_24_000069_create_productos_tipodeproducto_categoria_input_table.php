<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosTipodeproductoCategoriaInputTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'productos_tipodeproducto_categoria_input';

    /**
     * Run the migrations.
     * @table productos_tipodeproducto_categoria_input
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('idTipoDeProducto')->unsigned();
            $table->integer('idCategoria')->unsigned();
            $table->integer('idCategoriaInput')->unsigned();
            $table->integer('order');

            $table->index(["idCategoriaInput"], 'fk_productos_tipodeproducto_categoria_input_productos_categ_idx1');

            $table->index(["idTipoDeProducto"], 'fk_productos_tipodeproducto_categoria_input_productos_tipod_idx');

            $table->index(["idCategoria"], 'fk_productos_tipodeproducto_categoria_input_productos_categ_idx');


            $table->foreign('idTipoDeProducto', 'fk_productos_tipodeproducto_categoria_input_productos_tipod_idx')
                ->references('idTipoDeProducto')->on('productos_tipodeproducto')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idCategoria', 'fk_productos_tipodeproducto_categoria_input_productos_categ_idx')
                ->references('idCategoria')->on('productos_categoria')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idCategoriaInput', 'fk_productos_tipodeproducto_categoria_input_productos_categ_idx1')
                ->references('idCategoriaInput')->on('productos_categoria_input')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
