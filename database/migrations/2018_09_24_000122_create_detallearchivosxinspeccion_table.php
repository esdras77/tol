<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetallearchivosxinspeccionTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'detallearchivosxinspeccion';

    /**
     * Run the migrations.
     * @table detallearchivosxinspeccion
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('idTipoArchivoInspecciones')->unsigned();
            $table->integer('idConfiguracion')->unsigned();

            $table->index(["idTipoArchivoInspecciones"], 'fk_detallearchivosxinspeccion_tiposarchivosinspecciones1_idx');

            $table->index(["idConfiguracion"], 'fk_detallearchivosxinspeccion_configuracion_inspecciones1_idx');


            $table->foreign('idTipoArchivoInspecciones', 'fk_detallearchivosxinspeccion_tiposarchivosinspecciones1_idx')
                ->references('idTipoArchivoInspecciones')->on('tiposarchivosinspecciones')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idConfiguracion', 'fk_detallearchivosxinspeccion_configuracion_inspecciones1_idx')
                ->references('idConfiguracion')->on('configuracion_inspecciones')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
