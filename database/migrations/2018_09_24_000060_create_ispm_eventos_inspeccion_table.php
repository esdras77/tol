<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIspmEventosInspeccionTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'ispm_eventos_inspeccion';

    /**
     * Run the migrations.
     * @table ispm_eventos_inspeccion
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idEventoInspeccion');
            $table->integer('idInspeccion')->unsigned();
            $table->dateTime('fecha');
            $table->text('evento');

            $table->index(["idInspeccion"], 'fk_ispm_eventos_inspeccion_ispm_inspeccion1_idx');


            $table->foreign('idInspeccion', 'fk_ispm_eventos_inspeccion_ispm_inspeccion1_idx')
                ->references('idInspeccion')->on('ispm_inspeccion')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
