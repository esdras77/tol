<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOficinaTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'oficina';

    /**
     * Run the migrations.
     * @table oficina
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('idOficina')->primary();
            $table->string('idDireccion', 32)->unique()->nullable();
            $table->string('idEmpresa', 32);
            $table->integer('idPais')->unsigned();
            $table->string('nombre');
            $table->string('telefono1', 75)->nullable()->default(null);
            $table->string('telefono2', 75)->nullable()->default(null);
            $table->string('fax', 75)->nullable()->default(null);
            $table->dateTime('fechaModificacion')->nullable()->default(null);
            $table->dateTime('fechaBaja')->nullable()->default(null);
            $table->integer('principal')->nullable()->default(null);

            $table->index(["idDireccion"], 'fk_oficina_direccion1_idx');

            $table->index(["idPais"], 'fk_oficina_pais1_idx');

            $table->index(["idEmpresa"], 'fk_oficina_empresa1_idx');


            $table->foreign('idEmpresa', 'fk_oficina_empresa1_idx')
                ->references('idEmpresa')->on('empresa')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idDireccion', 'fk_oficina_direccion1_idx')
                ->references('idDireccion')->on('direccion')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idPais', 'fk_oficina_pais1_idx')
                ->references('idPais')->on('pais')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
