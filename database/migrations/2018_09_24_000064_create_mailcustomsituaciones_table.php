<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailcustomsituacionesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'mailcustomsituaciones';

    /**
     * Run the migrations.
     * @table mailcustomsituaciones
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idSituacion');
            $table->integer('idModuloRelevante')->unsigned();
            $table->string('situacion');
            $table->string('titulo')->nullable()->default(null);

            $table->index(["idModuloRelevante"], 'fk_mailcustomsituaciones_modulosrelevantes1_idx');


            $table->foreign('idModuloRelevante', 'fk_mailcustomsituaciones_modulosrelevantes1_idx')
                ->references('idModuloRelevante')->on('modulosrelevantes')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
