<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfiguracionInspeccionesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'configuracion_inspecciones';

    /**
     * Run the migrations.
     * @table configuracion_inspecciones
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idConfiguracion');
            $table->string('idEmpresa', 32)->unique();
            $table->integer('vencimientoInspeccion')->default('0');
            $table->integer('proximoAviso')->default('0');
            $table->tinyInteger('inspecciones')->default('0');
            $table->tinyInteger('operadores')->default('0');
            $table->date('ultimoAviso')->nullable()->default(null);

            $table->index(["idEmpresa"], 'fk_configuracion_inspecciones_empresa1_idx');


            $table->foreign('idEmpresa', 'fk_configuracion_inspecciones_empresa1_idx')
                ->references('idEmpresa')->on('empresa')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
