<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompraTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'compra';

    /**
     * Run the migrations.
     * @table compra
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idCompra');
            $table->integer('idUsuario')->unsigned();
            $table->integer('idPais')->unsigned();
            $table->integer('estado')->unsigned();
            $table->tinyInteger('online')->default('0');

            $table->index(["idUsuario"], 'fk_compra_usuario1_idx');

            $table->index(["estado"], 'fk_compra_estadoDeCompra1_idx');

            $table->index(["idPais"], 'fk_compra_pais1_idx');


            $table->foreign('idUsuario', 'fk_compra_usuario1_idx')
                ->references('idUsuario')->on('usuario')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idPais', 'fk_compra_pais1_idx')
                ->references('idPais')->on('pais')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('estado', 'fk_compra_estadoDeCompra1_idx')
                ->references('idEstadoDeCompra')->on('estadoDeCompra')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
