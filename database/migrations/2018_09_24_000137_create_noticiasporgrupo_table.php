<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoticiasporgrupoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'noticiasporgrupo';

    /**
     * Run the migrations.
     * @table noticiasporgrupo
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('idGrupo')->unsigned();
            $table->integer('idNoticia')->unsigned();

            $table->index(["idNoticia"], 'fk_noticiasporgrupo_noticias_cabecera1_idx');

            $table->index(["idGrupo"], 'fk_noticiasporgrupo_grupo1_idx');


            $table->foreign('idGrupo', 'fk_noticiasporgrupo_grupo1_idx')
                ->references('idGrupo')->on('grupo')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idNoticia', 'fk_noticiasporgrupo_noticias_cabecera1_idx')
                ->references('idNoticia')->on('noticias_cabecera')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
