<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemdecompraTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'itemDeCompra';

    /**
     * Run the migrations.
     * @table itemDeCompra
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idItemDeCompra');
            $table->integer('idEntidadFacturable')->unsigned();
            $table->string('descripcion')->nullable()->default(null);
            $table->decimal('monto', 11, 2)->nullable()->default(null);

            $table->index(["idEntidadFacturable"], 'fk_itemDeCompra_entidadFacturable1_idx');


            $table->foreign('idEntidadFacturable', 'fk_itemDeCompra_entidadFacturable1_idx')
                ->references('idEntidadFacturable')->on('entidadFacturable')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
