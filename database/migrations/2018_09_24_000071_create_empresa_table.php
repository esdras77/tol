<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'empresa';

    /**
     * Run the migrations.
     * @table empresa
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('idEmpresa', 32)->primary();
            $table->integer('idTipoCuenta')->unsigned();
            $table->string('idDireccion', 32)->unique()->nullable();
            $table->string('tipoEmpresa');
            $table->string('nombre');
            $table->string('sapCode');
            $table->string('BK', 20);

            $table->index(["idDireccion"], 'fk_empresa_direccion1_idx');

            $table->index(["idTipoCuenta"], 'fk_empresa_tipocuenta1_idx');

            $table->index(["tipoEmpresa"], 'fk_empresa_tipoempresa1_idx');


            $table->foreign('idTipoCuenta', 'fk_empresa_tipocuenta1_idx')
                ->references('idTipoCuenta')->on('tipocuenta')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('idDireccion', 'fk_empresa_direccion1_idx')
                ->references('idDireccion')->on('direccion')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('tipoEmpresa', 'fk_empresa_tipoempresa1_idx')
                ->references('tipoEmpresa')->on('tipoempresa')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
