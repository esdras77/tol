
@foreach ($itemMenuList as $menuItem)

	@if($loop->iteration %3 == 1)
		<div class="card-group">
	@endif

	<div class="card shadow float-left mr-5 mt-5" style="width: 18rem;">

		@if (!empty($menuItem->imagen))
			<img class="card-img-top" src="{{ $menuItem->imagen }}" alt="Card image cap">
		@endif

		<div class="card-body">

	    	<h3 class="card-title">@lang('tol.'.$menuItem->descripcion)</h3>
	    
		    <p class="card-text">
		    	@if (!empty($menuItem->textoweb))
					@lang('tol.'.$menuItem->textoweb)
				@endif
			</p>
		    
		    
		</div>

		<div class="card-footer">
	      <a href="{{ route($menuItem->url) }}" class="btn btn-primary btn-block btn-sm">
	      	Acceder
	      </a>
	    </div>

	</div>

	@if($loop->iteration %3 == 0)
		</div>
	@endif
	
@endforeach