<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>TüvOnline 4.0 - Tüv Rheinland de Argentina S.A. - Genau Richtig!</title>
        <link rel="shortcut icon" type="image/x-icon" href="/images/general/favicon.ico" />

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <script src="{{ asset('js/app.js') }}"></script>
    </head>
    <body>
        <!-- BIG IMAGE -->
        <div class="row">
            <div><a href="{{ route('index') }}"><img src="/images/general/logo_header.png" class="m-3" style="width: 35%;"></a></div>
        </div>

        <div class="container">
            @if (session()->has('status'))
                <div class="alert alert-success text-center" role="alert">
                    <p class="mt-1 mb-1 h4">{{ session('status') }}</p>
                </div>
            @endif

            @if (session()->has('info'))
                <div class="alert alert-success text-center" role="alert">
                    <p class="mt-1 mb-1 h4">{{ session('info') }}</p>
                </div>
            @endif

            @yield('content')
        </div>
    </body>
    <footer class="my-5 pt-5 text-muted text-center text-small footer">
        <p class="mb-1">© 2012-2018 Tüv Rheinland de Argentina S.A.</p>
        <ul class="list-inline">
          <li class="list-inline-item"><a href="#">Privacy</a></li>
          <li class="list-inline-item"><a href="mailto:ithelpdesk@ar.tuv.com">Support</a></li>
        </ul>
      </footer>
</html>
