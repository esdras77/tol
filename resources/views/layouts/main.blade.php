<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>TüvOnline 4.0 - Tüv Rheinland de Argentina S.A. - Genau Richtig!</title>
        <link rel="shortcut icon" type="image/x-icon" href="/images/general/favicon.ico" />

        <link href="/open-iconic-master/font/css/open-iconic-bootstrap.css" rel="stylesheet">

        <script src="https://unpkg.com/feather-icons"></script>
        <script src="https://cdn.jsdelivr.net/npm/feather-icons/dist/feather.min.js"></script>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <script src="{{ asset('js/app.js') }}"></script>

        {{-- datepicker --}}
        <link rel="stylesheet" href="{{ asset('datepicker/css/bootstrap-datepicker3.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('datepicker/css/bootstrap-datepicker.standalone.css') }}" rel="stylesheet">

        <script src="{{ asset('datePicker/js/bootstrap-datepicker.js') }}"></script>
        {{-- languages --}}
        <script src="{{ asset('datePicker/locales/bootstrap-datepicker.es.min.js') }}"></script>
        
        <script>
            $(document).ready(function(){
                feather.replace();

                $('.datepicker').datepicker({
                    language: "es",
                    daysOfWeekHighlighted: "1,2,3,4,5",
                    autoclose: true,
                    todayHighlight: true
                });

                // popovers along the site, they will have a html aproach
                $('[data-toggle="popover"]').popover({
                    container: 'body',
                    trigger: 'focus',
                    html: true,
                    content: function() {
                        var data = $(this).data('content_target');
                        if ($('#'+data)){
                            return $('#'+data).html();
                        }
                    }
                })
            })

            function languageselection(idIdioma){
                $.get('administration.languageselection', {'languageChangeRequest': idIdioma,}, function(result){
                    location.reload();
                })
            }
        </script>


    </head>
    <body>
        <div class="container-flex ml-3 mr-3 mt-3">
            
            <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm rounded">
                <h5 class="my-0 mr-md-auto font-weight-normal">
                   <a href="{{ route('home') }}"><img src="/images/general/logo_header.png" width="20%" alt=""></a>
                </h5>
                <nav class="my-2 mr-3 w-25">
                    <a class="btn btn-light float-right mr-1" href="#">
                        <i data-feather="message-square"></i> Contacto
                    </a>
                    <a class="btn btn-light float-right mr-1" href="https://campus.tuv-online.com.ar" target="_blank">
                        <i data-feather="book-open"></i> Campus
                    </a>
                </nav>
                <div class="btn-group">
                    <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ auth()->user()->nombre }} {{ auth()->user()->apellido }}
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="#"><i data-feather="user"></i> My profile</a>

                        <div class="dropdown-item dropdown-item-text ml-4">
                            @foreach (\App\Idioma::where('activo', 1)->get() as $language)
                                <a class="mr-1" href="#" onclick="javascript: languageselection({{ $language->idIdioma }})" title="{{ $language->nombre }}"><img src="/images/icon_flags/{{ $language->iso }}.png"></a>
                            @endforeach
                        </div>

                        <div class="dropdown-divider"></div>

                        <form class="dropdown-item " method="POST" action="logout"> 
                            {{csrf_field()}} <input type="submit" class="btn btn-danger btn-block btn-sm" value="{{trans('Salir')}}">
                        </form>
                    </div>
                </div>
            </div>

            @component('components.notifications.status')
            @endcomponent
            
            @component('components.notifications.info')
            @endcomponent

            <div class="row  mt-3">
                <div class="container-flex mr-3 ml-3 w-100">
                    @yield('news')
                </div>

                <div class="container mt-2">
                    @yield('breadcrumb')
                </div>

                <div class="container w-75">
                    @yield('content')
                </div>
            </div> 

        </div>

        @yield('scripts')

    </body>

    <footer class="my-5 pt-5 text-muted text-center text-small">
        <p class="mb-1">© 2012-{{ date("Y") }} Tüv Rheinland de Argentina S.A.</p>
        <ul class="list-inline">
          <li class="list-inline-item"><a href="{{ route('terms') }}">Privacy</a></li>
          <li class="list-inline-item"><a href="mailto:ithelpdesk@ar.tuv.com">Support</a></li>
        </ul>
      </footer>
</html>
