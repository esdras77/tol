{!! csrf_field() !!}

@component('components.form.input-search', [
    'name' => 'nombre',
    'label' => 'Nombre de archivo',
    'attributes' => 'required autofocus'
])@endcomponent
