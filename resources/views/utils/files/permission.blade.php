<div class="container" id="permissionContainer">
	
	<div class="row">
		
		<div class="col-md-4">My permission</div>
		@include('utils.files.permissionstatus', ['currentPermission' => $permission, 'user' => $permission])

	</div>
	<hr>
	<div class="row" id="groupStatus">
		
		<div class="col-md-4">
			@include('utils.files.permissionform')
		</div>
		
	</div>
	<hr>
	<div id="groupUsers">
				
	</div>
	
</div>

<script type="text/javascript">
	$('#idGrupo').on('change', function(e){
		$("#groupStatus .col-md-2").each(function(index, value){
			value.remove();
		});

		$("#groupUsers .col-md-4").each(function(index, value){
			value.remove();
		});
		$("#groupUsers .col-md-2").each(function(index, value){
			value.remove();
		});

		var route = '{{ route("utils.files.grouppermission") }}';
		var params = {'idGrupo': $(this).val(), 'idArchivo': {{ $permission->idArchivo }} };

		$.get(route, params, function(retrieve){
			$("#groupStatus").append(retrieve);
			$("#groupStatus .col-md-2").addClass('mt-4');
		});

		var route = '{{ route("utils.files.userspermission") }}';
		var params = {'idGrupo': $(this).val(), 'idArchivo': {{ $permission->idArchivo }} };

		$.get(route, params, function(userslist){
			$("#groupUsers").append(userslist);
			$("#groupUsers .col-md-2").addClass('mt-4');
		});
	});

	function changePermission(action, owner){
		type = typeof owner !== 'undefined' ?  'Usuario' : 'Grupo';
		owner = typeof owner !== 'undefined' ?  owner : $('#idGrupo').val();
		file = {{ $permission->idArchivo }};

		if (type == 'Grupo'){
			route = '{{ route('utils.files.grouppermission') }}';
			params = {'idGrupo': owner, 'action': action, 'file': file, '_token': '{{csrf_token()}}' };
		} else {
			route = '{{ route('utils.files.userspermission') }}';
			params = {'idUsuario': owner, 'action': action, 'file': file, '_token': '{{csrf_token()}}' };
		} 
		
		$.post(route, params, function(retrieve){
			$('#idGrupo').change();
		});

		//alert(owner);
		//alert(action);
		//alert(file);
	}
</script>