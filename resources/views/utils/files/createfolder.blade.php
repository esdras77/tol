<form action="{{ route('utils.files.store') }}" method="POST" id="form-edit">

    {!! csrf_field() !!}

    @component('components.form.hidden', [
        'name' => 'idArchivo',
        'type' => 'hidden',
        'object' => (!empty($baseFile)) ? $baseFile : 0,
    ])@endcomponent

    @component('components.form.input', [
        'name' => 'nombre',
        'label' => 'Nombre',
        'object' => $folder,
        'attributes' => 'required autofocus'
    ])@endcomponent

    @component('components.form.select', [
        'name' => 'idTipoAviso',
        'label' => 'Aviso:',
        'options' => $tipoAviso->pluck('nombre', 'idTipoAviso'),
        'object' => $folder,
        'comparison' => 'idTipoAviso',
        'attributes' => 'required',
        'multiple' => false
    ])@endcomponent

    @component('components.form.select', [
        'name' => 'aviso',
        'comparison' => 'idTipoAviso',
        'options' => array(),
        'attributes' => 'required',
        'multiple' => false
    ])@endcomponent

</form>