<div class="col-md-2" title="Admin">
	<button class="btn {{ (empty($currentPermission)) ? 'btn-outline-danger' 
													  : (!$currentPermission->admin) ? 'btn-outline-danger' : 'btn-outline-success' }}"
			onclick="javascript: changePermission('admin', {{ (isset($user)) ? $user->idUsuario : null }})">
		<i data-feather="user"></i>
	</button>
</div>
<div class="col-md-2" title="View">
	<button class="btn {{ (empty($currentPermission)) ? 'btn-outline-danger' 
													  : (!$currentPermission->read) ? 'btn-outline-danger' : 'btn-outline-success' }}"
			onclick="javascript: changePermission('read', {{ (isset($user)) ? $user->idUsuario : null }})">
		<i data-feather="eye"></i>
	</button>
</div>
<div class="col-md-2" title="Download">
	<button class="btn {{ (empty($currentPermission)) ? 'btn-outline-danger' 
													  : (!$currentPermission->download) ? 'btn-outline-danger' : 'btn-outline-success' }}"
			onclick="javascript: changePermission('download', {{ (isset($user)) ? $user->idUsuario : null }})">
		<i data-feather="download"></i>
	</button>
</div>
<div class="col-md-2" title="Delete">
	<button class="btn {{ (empty($currentPermission)) ? 'btn-outline-danger' 
													  : (!$currentPermission->delete) ? 'btn-outline-danger' : 'btn-outline-success' }}"
			onclick="javascript: changePermission('delete', {{ (isset($user)) ? $user->idUsuario : null }})">
		<i data-feather="trash"></i>
	</button>
</div>

<script type="text/javascript">
	feather.replace();
</script>
