@extends('layouts.main')

@section('breadcrumb')
	@include('components.breadcrumb.breadcrumb')
@endsection

@section('content')

	<hr class="mt-5">
	
	<form method="post" action="{{ route('utils.files.search') }}">

		@include('utils.files.search')
		
	</form>

	<div id="fileInfo" class="d-none"></div>

	@include('components.files.alternative-menu')

	<div class="container-flex mt-5">
		@include('components.files.iconview')
	</div>

@endsection

@section('scripts')

<script>
	
	$(document).ready(function(){

		$(".icon").on("mouseover", function(){
			$(this).css("background-color", "lightblue");
		});

		$(".icon").on("mouseout", function(){
			$(".icon").css("background-color", "white");
		});

		$(".icon").on("mousedown", function(){
			$(".icon").css("z-index", 0);
			$(this).css("z-index", 1);
		});

		/*
		$('.icon > .card-body').bind('mousedown', function(event) {
			switch (event.which) {
	        	case 3: alert("PERMISOS")
	        }
	    });
		*/

		$(".icon.folder").dblclick(function(){
			var idArchivo = ($(this).data('idarchivo'));
			window.location.href = '{{ route("utils.files") }}' + '?base=' + idArchivo;
		});

		$(".icon.file").dblclick(function(e){
			e.preventDeafault();
		});

	});

	function sentContentToIcon(obj){
		var idArchivo = obj.parent().data('idarchivo');
		window.location.href = '{{ route("utils.files") }}' + '?base=' + idArchivo;
	}

	//files mini-menu actions
	function perform(action, idFile){
		switch(action){
			case 'delete': 
							var conf = confirm("Esta acción es permanente y no puede ser desecha. ¿Desea continuar?");
						    if (conf == true){
						   		$('#deleteForm' + idFile).submit();
						   	}
				break;
			case 'copy': 
						var route = '{{ route('utils.files.copy') }}';
						var params = {"_token": "{{ csrf_token() }}", 'idArchivo': idFile};

						$.post(route, params, function(){

						});
				break;
			case 'cut': 
						var route = '{{ route('utils.files.cut') }}';
						var params = {"_token": "{{ csrf_token() }}", 'idArchivo': idFile};

						$.post(route, params, function(){

						});
				break;
			case 'paste': 
						var route = '{{ route('utils.files.paste') }}';
						var params = {"_token": "{{ csrf_token() }}", 'idArchivo': idFile};

						$.post(route, params, function(){
						 	location.reload();
						});
				break;
			case 'info':
						var route = '{{ route('utils.files.info', 1) }}';
					    var params = {"_token": "{{ csrf_token() }}", 'idArchivo': idFile};

					    $.get(route, params, function(htmlrecover){
					    	$("#alternative" + idFile +" .modal-dialog .modal-content .modal-body").html(htmlrecover);
					    });

					    $("#alternative" + idFile).modal('toggle');
					    $("#alternative" + idFile + " #submit").css('display', 'none');
				break;
			case 'download':
						var route = '{{ route('utils.files.show') }}';
						var params = {"_token": "{{ csrf_token() }}", 'idArchivo': idFile};

						$.get(route, params, function(retrieve){
						 	window.open(retrieve);
						});
				break;
			case 'create':
						$("#alternative" + idFile + " #submit").css('display', 'block');

						var route = '{{ route('utils.files.create') }}';
					    var params = {"_token": "{{ csrf_token() }}", 'idParent': idFile};

					    $.get(route, params, function(htmlrecover){
							//carga del formulario en el cuadro
							$("#alternative" + idFile +" .modal-dialog .modal-content .modal-body").html(htmlrecover);

							fillComboAviso($("#alternative" + idFile +" .modal-dialog .modal-content .modal-body #form-edit #idTipoAviso"), $("#alternative" + idFile +" .modal-dialog .modal-content .modal-body #form-edit #aviso"), idFile);

							//carga del select dinamico de persona de aviso
							$("#alternative" + idFile +" .modal-dialog .modal-content .modal-body #form-edit #idTipoAviso").on('change', function(){
								// cuando selecciono el tipo de aviso
								fillComboAviso($(this), $("#alternative" + idFile +" .modal-dialog .modal-content .modal-body #form-edit #aviso"), idFile);
							})

							//envio del formulario
							$("#alternative" + idFile +" .modal-dialog .modal-content .modal-footer #submit").on('click', function(e){
								e.preventDefault();
								var params = $("#alternative" + idFile +" .modal-dialog .modal-content .modal-body #form-edit").serialize();
								var route = $("#alternative" + idFile +" .modal-dialog .modal-content .modal-body #form-edit").attr("action");

								$.post(route, params, function(retrieve){
									if (retrieve == 'ok'){
										location.reload();
									}
								});

	  						});
						});

						$("#alternative" + idFile).modal('toggle');
				break;
			case 'edit': 
						$("#alternative" + idFile + " #submit").css('display', 'block');

						var route = '{{ route('utils.files.edit', 1) }}';
					    var params = {"_token": "{{ csrf_token() }}", 'idArchivo': idFile};

					    $.get(route, params, function(htmlrecover){
							//carga del formulario en el cuadro
							$("#alternative" + idFile +" .modal-dialog .modal-content .modal-body").html(htmlrecover);

							fillComboAviso($("#alternative" + idFile +" .modal-dialog .modal-content .modal-body #form-edit #idTipoAviso"), $("#alternative" + idFile +" .modal-dialog .modal-content .modal-body #form-edit #aviso"), idFile);

							//carga del select dinamico de persona de aviso
							$("#alternative" + idFile +" .modal-dialog .modal-content .modal-body #form-edit #idTipoAviso").on('change', function(){
								// cuando selecciono el tipo de aviso
								fillComboAviso($(this), $("#alternative" + idFile +" .modal-dialog .modal-content .modal-body #form-edit #aviso"), idFile);
							})

							//envio del formulario
							$("#alternative" + idFile +" .modal-dialog .modal-content .modal-footer #submit").on('click', function(e){
								e.preventDefault();
								var params = $("#alternative" + idFile +" .modal-dialog .modal-content .modal-body #form-edit").serialize();
								var route = $("#alternative" + idFile +" .modal-dialog .modal-content .modal-body #form-edit").attr("action");

								$.post(route, params, function(retrieve){
									if (retrieve == 'ok'){
										location.reload();
									}
								});

	  						});
						});

						$("#alternative" + idFile).modal('toggle');
				break;

			case 'permission':
						$("#alternative" + idFile + " #submit").css('display', 'block');

						var route = '{{ route('utils.files.permission') }}';
					    var params = {"_token": "{{ csrf_token() }}", 'idArchivo': idFile};

					    $.get(route, params, function(htmlrecover){
							//carga del formulario en el cuadro
							$("#alternative" + idFile +" .modal-dialog .modal-content .modal-body").html(htmlrecover);
						});

						$("#alternative" + idFile).modal('toggle');
						$("#alternative" + idFile + " #submit").css('display', 'none');
				break;
		}		
	}

	function fillComboAviso(objOri, objDest, idFile){
		// Carga para el valor del tipo de aviso
		var params = {'_token': "{{ csrf_token() }}", 
					  'idTipoAviso': objOri.val(), 
					  'idArchivo': idFile};
		var route = '{{ route('utils.files.fillaviso') }}';
		// cargo los datos en el otro select
		fillcombo(route, params, objDest);
	}

	function fillComboGrupos(objOri, objDest, idFile){
		// Carga para el valor del tipo de aviso
		var params = {'_token': "{{ csrf_token() }}", 
					  'idTipoAviso': objOri.val(), 
					  'idArchivo': idFile};
		var route = '{{ route('utils.files.fillgroups') }}';
		// cargo los datos en el otro select
		fillcombo(route, params, objDest);
	}

	function fillComboUsuarios(objOri, objDest, idFile){
		// Carga para el valor del tipo de aviso
		var params = {'_token': "{{ csrf_token() }}", 
					  'idTipoAviso': objOri.val(), 
					  'idArchivo': idFile};
		var route = '{{ route('utils.files.fillusers') }}';
		// cargo los datos en el otro select
		fillcombo(route, params, objDest);
	}

	function fillcombo(route, params, objDest){
		$.getJSON(route, params, function(retrieve){
			objDest.html("");
			$.each(retrieve, function(i, item){
				var selected = "";
				if (item.nombre != null){
					if (item.selected != 0) selected = "selected"; 
					objDest.append("<option label='"+item.nombre+"' value='"+item.id+"' "+selected+">"+item.nombre+"</option>");
				}
			});
		});
	}

	

</script>

@endsection