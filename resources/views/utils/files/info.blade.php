<div class="container">
	
	<div class="row">

		@if (!empty($file->extension))
			@if (in_array($file->extension,  array('png', 'jpg', 'bmp', 'jpeg')))

				<div class="col-md-6">
					<image src="{{ url('storage/'.$file->url) }}">
				</div>

			@endif
		@endif

		<div class="col-md-6">

			<div class="row">
				
				<div class="col-md-4">Nombre</div>
				<div class="col-md-8">{{ $file->nombre }}</div>
				<div class="col-md-4">Ultima Edición</div>
				<div class="col-md-8">{{ \Carbon\Carbon::parse($file->updated_at)->format('d/m/Y') }}</div>
				<div class="col-md-4">Extensión</div>
				<div class="col-md-8">{{ $file->extension }}</div>
				<hr>
				<div class="col-md-4">Dueño</div>
				<div class="col-md-8">{{ $owner->nombre }}, {{$owner->apellido }}</div>

			</div>


		</div>

	</div>

</div>