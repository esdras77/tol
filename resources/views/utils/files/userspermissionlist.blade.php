@foreach ($users as $user)
	<div class="row">
		<div class="col-md-4 mt-4 text-center" title="Admin">
			{{ $user->nombre }} {{ $user->apellido }}
		</div>
		@include('utils.files.permissionstatus', 
				['currentPermission' => $user->permisos()->where('idArchivo', $file->idArchivo)->first(), 'user' => $user])
	</div>
@endforeach