{!! csrf_field() !!}

@component('components.form.select', [
    'name' => 'idGrupo',
    'label' => 'Grupo:',
    'options' => $currentUser->groups->pluck('nombre', 'idGrupo'),
    'attributes' => 'required',
    'multiple' => false
])@endcomponent