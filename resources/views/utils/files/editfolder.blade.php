<form action="{{ route('utils.files.update', $folder->idArchivo) }}" method="post" id="form-edit">

    {!! csrf_field() !!}

    {!! method_field('PUT') !!}

    @component('components.form.input', [
        'name' => 'nombre',
        'label' => 'Nombre',
        'object' => $folder,
        'attributes' => 'required autofocus'
    ])@endcomponent

    @component('components.form.select', [
        'name' => 'idTipoAviso',
        'label' => 'Aviso:',
        'options' => $folder->tipoAviso->pluck('nombre', 'idTipoAviso'),
        'object' => $folder,
        'comparison' => 'idTipoAviso',
        'attributes' => 'required',
        'multiple' => false
    ])@endcomponent

    @component('components.form.select', [
        'name' => 'aviso',
        'comparison' => 'idTipoAviso',
        'options' => array(),
        'attributes' => 'required',
        'multiple' => false
    ])@endcomponent

</form>