@extends('layouts.main')

@section('breadcrumb')
	@include('components.breadcrumb.breadcrumb')
@endsection

@section('content')

	<hr class="mt-5">
	<form method="get" action="{{ route('utils.agenda') }}">
		{!! csrf_field() !!}
		<div class="input-group">
			  <select class="custom-select" id="idGrupo" name="idGrupo" aria-label="Example select with button addon">
			    <option selected>Seleccione...</option>
			    @foreach ($groups as $group)
			    	<option value="{{ $group->idGrupo }}">{{ $group->nombre }}</option>
			    @endforeach
			  </select>
			  <div class="input-group-append">
			    <button class="btn btn-outline-secondary" type="button" onclick="javascript: submit()">
			    	<i data-feather="search"></i> Buscar
			    </button>
			  </div>
		</div>
		
	</form>
	
	@if (!empty($currentGroup))
		<div class="h3 mt-5">Grupo: {{ $currentGroup->nombre }}</div>
		
		
			
		
		<table class="table table-hover">
		  <thead>
		    <tr>
		      <th scope="col">#</th>
		      <th scope="col">@lang('tol.txt_label_name')</th>
		      <th scope="col">@lang('tol.txt_label_email')</th>
		      <th scope="col">@lang('tol.txt_label_phones')</th>
		    </tr>
		  </thead>
		  <tbody>
			@foreach ($currentGroup->user as $person)
				<tr>
				<th scope="row">{{ $loop->iteration }}</th>
				<td>{{ $person->nombre }}, {{ $person->apellido }}</td>
				<td>{{ $person->email }}</td>
				<td>{{ $person->telefono }} | {{ $person->celular }}</td>
				</tr>
			@endforeach
		    
		  </tbody>
		</table>
	@endif

@endsection