@component('components.lists.navtitle',[
	'title' => 'Listado de Noticias',
	'formTitle' => 'Buscar noticias',
	'form' => [
		'formAction' => 'administration.news.find',
		'formComponents' => [
			['input', 'descripcion', 'Titular']
		]
	]
])
@endcomponent