@extends('layouts.main')

@section('breadcrumb')
	@include('components.breadcrumb.breadcrumb')
@endsection


@section('content')

	<div class="container mt-4">
		
		<h1>Traducir una noticia</h1>

		<div class="container mt-3 mb-3">
			<button class="btn btn-outline-dark"><img src="/images/flags/flag_argentina.png" alt="Español"></button>
			<button class="btn btn-outline-dark"><img src="/images/flags/flag_usa.png" alt="Español"></button>
			<button class="btn btn-outline-dark"><img src="/images/flags/flag_pt.png" alt="Español"></button>
		</div>

		<form method="post" action="{{ route('administration.news.update', $news->idNoticia) }}">
			
			{!! method_field('PUT') !!}

			@include('administration.news.tform')
			
		</form>

	</div>
	
@endsection

@section('scripts')

<script type="text/javascript">

	$(document).ready(function(){

		if ($('#principal').prop('checked'))
			$('#grupos').parent().parent().hide();

		ClassicEditor
				    .create( document.querySelector( '.editor' ) )
				    .then( editor => {
				        console.log( editor );
				    } )
				    .catch( error => {
				        console.error( error );
				    } );
	});

</script>

@endsection