@extends('layouts.main')

@section('breadcrumb')
	@include('components.breadcrumb.breadcrumb')
@endsection

@section('content')

	@include('administration.news.navtitle')

	<table class="table table-hover mt-5">
	  	<thead>
		    <tr>
				<th scope="col">#</th>
				<th scope="col">Titular</th>
				<th scope="col" class="text-center">Publicado por</th>
				<th scope="col">Fecha de publicación</th>
				<th scope="col">Fecha de desactivación</th>
				<th scope="col">
					<button class="btn btn-success btn-sm mr-3" title="Nueva noticia" onclick="javascript: window.location.href='{{ route('administration.news.create')}}'"><i data-feather="radio"></i></button>	
				</th>
		    </tr>
	  	</thead>
	  	<tbody>
			@foreach ($news as $new)
			<tr>
				<th scope="row">{{ $new->idNoticia }}</th>
				<td>{!! empty($new->getBody()->titulo) ? '<span class="text-info">'.$new->descripcion.'</span>' : $new->getBody()->titulo !!}</td>
				<td class="w-25 text-center"><p>{{ $new->responsible->nombre }}, {{ $new->responsible->apellido }}</p></td>
				<td>{{ \Carbon\Carbon::parse($new->fechaPublicacion)->format('d/m/Y')}}</td>
				<td>{{ \Carbon\Carbon::parse($new->fechaVencimiento)->format('d/m/Y')}}</td>
				<td class="w-25">
					<button class="btn btn-outline-secondary btn-sm" title="Vista previa"
							onclick="javascript: window.location.href='{{ route('administration.news.show', $new->idNoticia) }}'">
						<i data-feather="eye"></i>
					</button>

					<button class="btn btn-outline-secondary btn-sm" title="Edit" 
							onclick="javascript: window.location.href='{{ route('administration.news.edit', $new->idNoticia) }}'">
						<i data-feather="edit"></i>
					</button>

					<button class="btn btn-outline-secondary btn-sm" title="Translate" 
							onclick="javascript: window.location.href='{{ route('administration.news.translate', $new->idNoticia) }}'">
						<i data-feather="voicemail"></i>
					</button>

					<form style="display: inline;" method="POST" action={{ route('administration.news.delete', $new->idNoticia) }}>
			      		{!! csrf_field() !!}
			      		{!! method_field('delete') !!}
			      		<button class="btn btn-outline-danger btn-sm" type="submit" title="Delete"><i data-feather="trash"></i> </button>	
			      	</form>

				</td>
			</tr>
			@endforeach
	  	</tbody>
	</table>

	<div class="flex">{{ $news->appends(request()->all())->onEachSide(3)->links() }}</div>

@endsection