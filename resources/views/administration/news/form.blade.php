{!! csrf_field() !!}

@component('components.form.hidden', [
	'type' => 'hidden',
    'name' => 'idNoticia',
    'object' => $news
])@endcomponent

@component('components.form.hidden', [
	'type' => 'hidden',
    'name' => 'idIdioma',
    'object' => $news->getBody()
])@endcomponent

@component('components.form.input', [
    'name' => 'descripcion',
    'label' => 'Descripcion general',
    'object' => $news,
    'attributes' => 'required autofocus'
])@endcomponent

@component('components.form.input', [
    'name' => 'titulo',
    'label' => 'Titular',
    'object' => $news->getBody(),
    'attributes' => 'required'
])@endcomponent

@component('components.form.textarea', [
    'name' => 'cuerpo',
    'label' => 'Noticia',
    'object' => $news->getBody(),
    'attributes' => 'required',
    'class' => 'editor'
])@endcomponent

@component('components.form.input', [
    'name' => 'fechaPublicacion',
    'label' => 'Fecha de publicación',
    'object' => $news,
    'attributes' => 'required',
    'class' => 'datepicker'
])@endcomponent

@component('components.form.input', [
    'name' => 'fechaVencimiento',
    'label' => 'Fecha de cierre',
    'object' => $news,
    'attributes' => 'required',
    'class' => 'datepicker'
])@endcomponent

@component('components.form.checkbox', [
    'name' => 'principal',
    'label' => 'La noticia se muestra en página principal',
    'object' => $news
])@endcomponent

@component('components.form.select', [
    'name' => 'grupos',
    'label' => 'Grupos relacionados',
    'options' => $groups->pluck('nombre', 'idGrupo'),
    'object' => $news,
    'comparison' => 'idGrupo',
    'attributes' => 'required',
    'multiple' => true
])@endcomponent


@component('components.form.buttons', [
    'buttons' => [
    	[
    		'type' => 'submit',
    	 	'class' => 'success',
    	 	'text' => 'Enviar'
    	],
    	[
    		'type' => 'reset',
    	 	'class' => '',
    	 	'text' => 'Reset'
    	],
    	[
    		'type' => 'button',
    	 	'class' => 'danger',
    	 	'text' => 'Cancel',
    	 	'action' => 'Back'
    	]

    ]
])@endcomponent