@extends('layouts.main')

@section('breadcrumb')
	@include('components.breadcrumb.breadcrumb')
@endsection


@section('content')

	<div class="container mt-4">
		
		<h1>Crear una nueva noticia</h1>
		
		<form method="post" action="{{ route('administration.news.store') }}">

			@include('administration.news.form')
			
		</form>

	</div>
	
@endsection

@section('scripts')

<script type="text/javascript">

	$(document).ready(function(){

		if ($('#principal').prop('checked'))
			$('#grupos').parent().parent().hide();

		$('#principal').on('change', function(){
			if ($(this).prop('checked')){
					$('#grupos').parent().parent().hide();
			} else {
					$('#grupos').parent().parent().show();
			}
		});

	});

</script>

@endsection