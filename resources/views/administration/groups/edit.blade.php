@extends('layouts.main')

@section('breadcrumb')
	@include('components.breadcrumb.breadcrumb')
@endsection

@section('content')

<div class="container mt-4">
	
	<h1>Editar grupo</h1>
	
	<form method="post" action="{{ route('administration.groups.update', $group->idGrupo) }}">

		{!! method_field('PUT') !!}

		@include('administration.groups.form')
		
	</form>

</div>


@endsection