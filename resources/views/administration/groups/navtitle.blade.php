@component('components.lists.navtitle',[
	'title' => 'Listado de Grupos',
	'formTitle' => 'Buscar grupos',
	'form' => [
		'formAction' => 'administration.groups.find',
		'formComponents' => [
			['input', 'nombre', 'Nombre']
		]
	]
])
@endcomponent