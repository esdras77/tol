@extends('layouts.main')

@section('breadcrumb')
	@include('components.breadcrumb.breadcrumb')
@endsection

@section('content')

<div class="container mt-4">

	<div class="pos-f-t">
		<nav class="navbar navbar-light bg-light">

			<h1>Administracion del coordinador en el grupo: {{ $group->nombre }}</h1>

		</nav>
	</div>

	<div class="row mt-3">

		<div class="col-md-4">
			<div class="card mt-5 shadow">
				<div class="card-header">
					<h3 class="">Recuerde</h3>
				</div>

				<div class="card-body">
					<blockquote class="blockquote mb-0">
				    	<p>El perfil de administrador de grupo realiza las tareas de:</p>
				    		<ul class="list-unstyled">
				    			<li>- Recepción de e-mails de contacto enviados al grupo.</li>
				    			<li>- Moderación de los salones de charla en el foro.</li>
				    		</ul>
				      <footer class="blockquote-footer">Por favor, sea responsable con su elección.</footer>
				    </blockquote>
				</div>

				<div class="card-footer"><br><br></div>

			</div>
		</div>

		<div class="col-md-4">
			<span class="text-muted">Usuarios en el grupo</span>
			<div id="listUsers" style="height: 25rem; border: 1px solid #ced4da; overflow-y: auto;" class="shadow mt-4">
				<ul class="list-group">
				@foreach ($groupusers as $user)
  					<li class="list-group-item">
  						@if (!empty($coordinator) && $user->idUsuario != $coordinator->idUsuario)
	  						<button class="btn btn-outline-success btn-sm float-right btnadd" 
	  								title="Agregar como administrador" 
	  								data-user="{{ $user->idUsuario }}"
	  								data-group="{{ $group->idGrupo }}">
	  							<i data-feather="arrow-right"></i>
	  						</button>
  						@endif
  						<div class="float-left">{{ $user->nombre }}, {{ $user->apellido }} </div>
  						
  					</li>
				@endforeach
				</ul>
			</div>

		</div>

		<div class="col-md-4">
			<span class="text-muted">Coordinador actual</span>
			<div class="card mt-4 shadow">
				<div class="card-header text-center">
					<img class="rounded-circle" src="/images/general/profile_empty.png" width="128">	
				</div>
				<div class="card-body text-center" style="height: 15.3rem;">
					
					<h4>
						@if (!empty($coordinator))
							{{ $coordinator->nombre }}, {{ $coordinator->apellido }}
						@endif
					</h4>
										
				</div>
			</div>
		</div>

	</div>

	{{-- lo uso para hacer generica la funcion --}}
	<form action="{{ route('administration.groups.adminstore', [':GROUP_ID', ':USER_ID']) }}" method="post" id="form-coord">
		{!! csrf_field() !!}
	</form>

</div>

@endsection

@section('scripts')

<script>
	$(document).ready(function(){

		$('.btnadd').on('click', function(){
			var group = $(this).data('group');
			var user = $(this).data('user');
			var form = $('#form-coord');
			var action = form.attr('action').replace(':GROUP_ID',group).replace(':USER_ID', user);
			var data = form.serialize();

			$.get(action, data, function(retrieve){
				location.reload();
			});
		});

	})
</script>

@endsection