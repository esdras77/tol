@extends('layouts.main')

@section('breadcrumb')
	@include('components.breadcrumb.breadcrumb')
@endsection

@section('content')

<div class="container">
	<div class="pos-f-t">
		<nav class="navbar navbar-light bg-light">

			<h1>Administracion de usuarios en el grupo: {{ $group->nombre }}</h1>

		</nav>
	</div>

	<div class="row mb-5">
		<div class="col-md-4">
			<label for="nombre" class="control-label">Buscar usuario</label>
			<input type="text" name="nombre" class="form-control">
		</div>
	</div>
	<div class="row">
		
		<div class="col-md-4">
			{{-- formulario de ingreso --}}
			<span class="text-muted">Usuarios</span>
			<div id="listUsers" style="height: 26.01rem; border: 1px solid #ced4da; overflow-y: auto;" class="shadow">
				<ul class="list-group">
				@foreach ($users as $user)
  					<li class="list-group-item">{{ $user->nombre }}, {{ $user->apellido }} 
  						<button class="btn btn-outline-success btn-sm float-right btnaction" data-user="{{ $user->idUsuario }}" data-group="{{ $group->idGrupo }}">
  							<i data-feather="arrow-right"></i>
  						</button>
  					</li>
				@endforeach
				</ul>
			</div>

		</div>

		<div class="col-md-4 offset-md-4">
			<span class="text-muted">Usuarios en el grupo</span>
			<div id="listUsers" style="height: 26.01rem; border: 1px solid #ced4da; overflow-y: auto;" class="shadow">
				<ul class="list-group">
				@foreach ($groupusers as $user)
  					<li class="list-group-item">
  						<button class="btn btn-outline-danger btn-sm float-left btnaction" data-user="{{ $user->idUsuario }}" data-group="{{ $group->idGrupo }}">
  							<i data-feather="arrow-left"></i>
  						</button>
  						<div class="float-right">{{ $user->nombre }}, {{ $user->apellido }} </div>
  					</li>
				@endforeach
				</ul>
			</div>

		</div>

	</div>
</div>

{{-- lo uso para hacer generica la funcion --}}
<form action="{{ route('administration.groups.userstore', [':GROUP_ID', ':USER_ID']) }}" id="form-toogle">
	{!! csrf_field() !!}
</form>

@endsection

@section('scripts')

<script>
	
	$(document).ready(function(){
		$(".btnaction").click(function(e){
			var row = $(this).parents('li');
			var user = $(this).data('user');
			var group = $(this).data('group');
			var form = $('#form-toogle');
			var action = form.attr('action').replace(':GROUP_ID',group).replace(':USER_ID', user);
			var data = form.serialize();

			row.fadeOut();
			
			$.post(action, data, function (value){
				location.reload();
			}).fail(function(){
				alert("error");
				row.show();
			})
			
		})
	});

</script>

@endsection