@extends('layouts.main')

@section('breadcrumb')
	@include('components.breadcrumb.breadcrumb')
@endsection

@section('content')

<div class="container mt-4">
	
	<h1>Crear un nuevo grupo</h1>
	
	<form method="post" action="{{ route('administration.groups.store') }}">

		@include('administration.groups.form')
		
	</form>

</div>


@endsection