@extends('layouts.main')

@section('breadcrumb')
	@include('components.breadcrumb.breadcrumb')
@endsection

@section('content')

	@include('administration.groups.navtitle')

	<table class="table table-hover mt-5">
	  	<thead>
		    <tr>
				<th scope="col">#</th>
				<th scope="col">Nombre</th>
				<th scope="col" class="text-center">Cantidad participantes</th>
				<th scope="col">
					<button class="btn btn-success btn-sm mr-3" title="Nuevo grupo" onclick="javascript: window.location.href='{{ route('administration.groups.create')}}'"><i data-feather="users"></i></button>	
				</th>
		    </tr>
	  	</thead>
	  	<tbody>
			@foreach ($groups as $group)
			<tr>
				<th scope="row">{{ $group->idGrupo }}</th>
				<td>{{ $group->nombre }}</td>
				<td class="w-25 text-center"><p>{{ $group->user->count() }}</p></td>
				<td class="w-25">
					<button class="btn btn-outline-secondary btn-sm" title="View grupo"
							onclick="javascript: window.location.href='{{ route('administration.groups.show', $group->idGrupo) }}'">
						<i data-feather="eye"></i>
					</button>

					<button class="btn btn-outline-secondary btn-sm" title="Edit" 
							onclick="javascript: window.location.href='{{ route('administration.groups.edit', $group->idGrupo) }}'">
						<i data-feather="edit"></i>
					</button>

			      	<button class="btn btn-outline-info btn-sm" title="Usuarios en el grupo" 
							onclick="javascript: window.location.href='{{ route('administration.groups.users', $group->idGrupo) }}'">
						<i data-feather="users"></i>
					</button>
					<button class="btn btn-outline-info btn-sm" title="Configurar administrador" 
							onclick="javascript: window.location.href='{{ route('administration.groups.admin', $group->idGrupo) }}'">
						<i data-feather="target"></i>
					</button>

					<form style="display: inline;" method="POST" action={{ route('administration.groups.delete', $group->idGrupo) }}>
			      		{!! csrf_field() !!}
			      		{!! method_field('delete') !!}
			      		<button class="btn btn-outline-danger btn-sm" type="submit" title="Delete"><i data-feather="trash"></i> </button>	
			      	</form>

				</td>
			</tr>
			@endforeach
	  	</tbody>
	</table>

	<div class="flex">{{$groups->appends(request()->all())->onEachSide(3)->links()}}</div>

@endsection