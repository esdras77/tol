@extends('layouts.main')

@section('breadcrumb')
	@include('components.breadcrumb.breadcrumb')
@endsection

@section('content')

<div class="container mt-4">
	
	<h1>Crear un nuevo texto</h1>
	
	<form method="post" action="{{ route('administration.translations.store') }}">

		@include('administration.translations.form')
		
	</form>

</div>


@endsection