@component('components.lists.navtitle',[
	'title' => 'Textos del sistema',
	'formTitle' => 'Buscar texto',
	'form' => [
		'formAction' => 'administration.translations.find',
		'formComponents' => [
			['input', 'traduccion', 'Texto']
		]
	]
])
@endcomponent