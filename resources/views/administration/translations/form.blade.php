{!! csrf_field() !!}

@component('components.form.input', [
    'name' => 'idTexto',
    'label' => 'ID Texto',
    'object' => $text,
    'attributes' => 'required autofocus'
])@endcomponent

@component('components.form.hidden', [
    'name' => 'idiomaOriginal',
    'type' => 'hidden',
    'object' => $text,
])@endcomponent

@component('components.form.hidden', [
    'name' => 'idTraduccion',
    'type' => 'hidden',
    'object' => $translation,
])@endcomponent

@component('components.form.hidden', [
    'name' => 'idIdioma',
    'type' => 'hidden',
    'object' => $translation,
])@endcomponent

@component('components.form.textarea', [
    'name' => 'traduccion',
    'label' => 'Traduccion al '.\App\Idioma::where('idIdioma', $translation->idIdioma)->first()->nombre,
    'object' => $translation,
    'attributes' => 'required'
])@endcomponent

@component('components.form.buttons', [
    'buttons' => [
    	[
    		'type' => 'submit',
    	 	'class' => 'success',
    	 	'text' => 'Guardar'
    	],
    	[
    		'type' => 'reset',
    	 	'class' => '',
    	 	'text' => 'Reset'
    	],
    	[
    		'type' => 'button',
    	 	'class' => 'danger',
    	 	'text' => 'Cancel',
    	 	'action' => 'Back'
    	]

    ]
])@endcomponent