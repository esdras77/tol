@extends('layouts.main')

@section('breadcrumb')
	@include('components.breadcrumb.breadcrumb')
@endsection



@section('content')
	
	@include('administration.translations.navtitle')

	<div class="container-flex mt-5 mb-5">
		<div class="btn-group" role="group">
		  <button type="button" class="btn btn-outline-primary" title="Idiomas"><i data-feather="globe" class="text-success"></i></button>
		  <button type="button" class="btn btn-outline-primary" title="Upload file"><i data-feather="upload" class="text-dark"></i></button>
		  <button type="button" class="btn btn-outline-primary" title="Download file"><i data-feather="download" class="text-dark"></i></button>
		</div>
	</div>

	<table class="table table-hover mt-5">
	  	<thead>
		    <tr>
				<th scope="col">Tag</th>
				<th scope="col">Texto</th>
				<th scope="col">Fecha de edición</th>
				<th scope="col">
					<button class="btn btn-success btn-sm mr-3" title="Nuevo texto" onclick="javascript: window.location.href='{{ route('administration.translations.create')}}'"><i data-feather="edit-3"></i></button>
				</th>
		    </tr>
	  	</thead>
	  	<tbody>
			@foreach ($texts as $text)
			<tr>
				<th scope="row">{{ $text->idTexto }}</th>
				<td style="width: 33%;">{{ $text->getTranslation()->traduccion }}</td>
				<td>{{ \Carbon\Carbon::parse($text->getTranslation()->updated_at)->format('d/m/Y') }}</td>
				<td>
					<button class="btn btn-outline-secondary btn-sm" title="View text"
							onclick="javascript: window.location.href='{{ route('administration.translations.show', $text->idTexto) }}'">
						<i data-feather="eye"></i>
					</button>

					<button class="btn btn-outline-secondary btn-sm" title="Edit" 
							onclick="javascript: window.location.href='{{ route('administration.translations.edit', $text->idTexto) }}'">
						<i data-feather="edit"></i>
					</button>
{{-- 
					<button class="btn btn-outline-secondary btn-sm" title="Translate"
							onclick="javascript: window.location.href='{{ route('administration.translations.translate', $text->idTexto) }}'">
						<i data-feather="edit-3"></i>
					</button> --}}

					<form style="display: inline;" method="POST" action={{ route('administration.translations.delete', $text->idTexto) }}>
			      		{!! csrf_field() !!}
			      		{!! method_field('delete') !!}
			      		<button class="btn btn-outline-danger btn-sm" type="submit" title="Delete"><i data-feather="trash"></i> </button>	
			      	</form>
				</td>
			</tr>
			@endforeach
	  	</tbody>
	</table>

	<div class="flex">{{ $texts->appends(request()->all())->onEachSide(3)->links() }}</div>

@endsection