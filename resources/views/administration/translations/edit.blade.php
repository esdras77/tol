@extends('layouts.main')

@section('breadcrumb')
	@include('components.breadcrumb.breadcrumb')
@endsection

@section('content')

<div class="container mt-4">
	
	<div class="container mb-4">
		<div class="row">
			<div class="h1 col-md-7">Editar texto</div>
			<div class="col-md-5">
				<div class="btn-group" role="group" aria-label="Basic example">
					@foreach ($languages as $language)
						<button type="button" 
								class="btn btn-outline-primary" 
								title="{{ $language->nombre }}"
								onclick="javascript: window.location.href = '{{ route('administration.translations.edit', ['id' => $text->idTexto, 'idIdioma' => $language->idIdioma]) }}'">
							<img src="/images/icon_flags/{{ $language->iso }}.png">
						</button>	
					@endforeach
				  	
				</div>
			</div>
		</div>
	</div>
	
	<form method="post" action="{{ route('administration.translations.update', $text->idTexto) }}">

		{!! method_field('PUT') !!}

		@include('administration.translations.form')
		
	</form>

</div>


@endsection