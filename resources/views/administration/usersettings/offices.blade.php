@extends('layouts.main')

@section('breadcrumb')
	@include('components.breadcrumb.breadcrumb')
@endsection

@section('content')
	
	<div class="container mt-4">

		<h1>Relacionar empresas - Configuracion de vistas</h1>
		
		<div class="row">
			<div class="col-md-12">
				@include('administration.usersettings.addoffices', ['enterprises' => \App\Empresa::orderBy('nombre')->get()])
			</div>
		</div>
		
		<div class="row">
			<table class="table table-hover mt-5">
			  	<thead>
				    <tr>
						<th scope="col">Oficina</th>
						<th scope="col">Empresa</th>
						<th scope="col" class="w-25">Dirección</th>
						<th scope="col" class="w-25">Contacto</th>
						<th scope="col">

						</th>
				    </tr>
			  	</thead>
			  	<tbody>
					@foreach ($offices as $office)
					<tr>
						<td>{{ $office->nombre }}</td>
						<td>{{ $office->enterprise->nombre }}</td>
						<td>{{ $office->address->direccion }}, {{ $office->address->ciudad }} - {{ $office->address->provincia }}</td>
						<td>{{ $office->telefono1 }} <br> {{ $office->telefono2 ?: '' }} </td>
						<td>
							<form style="display: inline;" 
						            	method="POST" 
						            	action={{ route('administration.user.deleteoffice') }}>
							      		{!! csrf_field() !!}
							      		{!! method_field('delete') !!}
							      		<input type="hidden" name="idUsuario" value="{{ $user->idUsuario }}">
							      		<input type="hidden" name="idOficina" value="{{ $office->idOficina }}">
							      		<button class="btn btn-outline-danger btn-sm" type="submit" title="Delete"><i data-feather="trash"></i> </button>	
					      	</form>
						</td>
					</tr>
					@endforeach
			  	</tbody>
			</table>
			
			<div class="flex">{{$offices->appends(request()->all())->onEachSide(3)->links()}}</div>
			
		</div>

	</div>

@endsection

@section('scripts')
	
	<script type="text/javascript">
		
		$(document).ready(function(){
			$("#idEmpresa").on('change', function(e){

				var route = '{{ route('administration.user.officesrelated') }}';
				var params = {'idEmpresa': $(this).val()};

				$.getJSON(route, params, function(retrieve){
					$('#idOficina').html("");
					$.each(retrieve, function(i, item){
						var selected = "";
						if (item.nombre != null){
							if (item.selected != 0) selected = "selected"; 
							$('#idOficina').append("<option label='"+item.nombre+"' value='"+item.id+"' "+selected+">"+item.nombre+"</option>");
						}
					});
				});
			});
		});

	</script>

@endsection