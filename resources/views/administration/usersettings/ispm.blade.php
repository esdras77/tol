@extends('layouts.main')

@section('breadcrumb')
	@include('components.breadcrumb.breadcrumb')
@endsection

@section('content')
	
	<div class="container mt-4">

		<h1>Configuracion para ISPM</h1>
				
		<div class="row">
			<table class="table table-hover mt-5">
			  	<thead>
				    <tr>
						<th scope="col">Cliente</th>
						<th scope="col">Proyectos</th>
				    </tr>
			  	</thead>
			  	<tbody>
					@foreach ($ispmCustomers as $customer)
					<tr scope="row">
						<td>{{ $customer->nombre }}</td>
						<td>
							<button class="btn btn-outline-secondary btn-sm" title="Proyectos"
									onclick="projectSelection('{{ $customer->id }}', {{ $user->idUsuario }})">
								<i data-feather="archive"></i>
							</button>
						</td>
					</tr>
					@endforeach
			  	</tbody>
			</table>
			
		</div>

	</div>

	<div class="modal fade" 
	     id="inspections_list" 
	     tabindex="-1" role="dialog" aria-labelledby="alternative" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">
	          <span class="align-middle">Listado de documentos de inspección</span>
	        </h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	          <form action="">
	          	<div id="form-inside"></div>
	          </form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="button" class="btn btn-primary" id="submit">Save changes</button>
	      </div>
	    </div>
	  </div>
	</div>

@endsection

@section('scripts')
	
	<script type="text/javascript">
		
		function projectSelection(idCustomer, idUser){

			var route = '{{ route('administration.ispm.ispmprojects') }}';
		    var params = {"_token": "{{ csrf_token() }}", 'idUsuario': idUser, 'idCustomer': idCustomer};

		    $("#inspections_list .modal-dialog .modal-content .modal-body #form-inside").html();
		    $.post(route, params, function(htmlrecover){
				//carga del formulario en el cuadro
				$("#inspections_list .modal-dialog .modal-content .modal-body #form-inside").html(htmlrecover);
			});

			$("#inspections_list").modal('toggle');

		}

	</script>

@endsection