<style>
	
	.form-group {width: 25% !important;}

</style>


<h1>Proyectos</h1>

<hr>

@if (!empty($currentProjects))

	@foreach ($currentProjects as $project)

			@if ($loop->index%4 == 0)
				<div class="row">	
			@endif

				@component('components.form.checkbox', [
				    'name' => $project->idProyecto,
				    'label' => $project->proyecto,
				    'object' => $user->proyectosispm()->where('idProyecto', $project->idProyecto)->first()
				])@endcomponent

			@if ($loop->index%4 == 3 || $loop->last)
				</div>
			@endif

	@endforeach
@else
	<h3>No hay proyectos para este cliente</h3>
@endif