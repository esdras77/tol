<form method="post" action="{{ route('administration.user.relateoffice') }}">

	{!! csrf_field() !!}

	@component('components.form.hidden', [
	'type' => 'hidden',
    'name' => 'idUsuario',
    'object' => $user
	])@endcomponent

	@component('components.form.select', [
	    'name' => 'idEmpresa',
	    'label' => 'Empresa',
	    'options' => $enterprises->pluck('nombre', 'idEmpresa'),
	    'attributes' => 'required',
	    'multiple' => false
	])@endcomponent

	@component('components.form.select', [
	    'name' => 'idOficina',
	    'label' => 'Oficina',
	    'attributes' => 'required',
	    'multiple' => false
	])@endcomponent

	@component('components.form.buttons', [
	    'buttons' => [
	    	[
	    		'type' => 'submit',
	    	 	'class' => 'success',
	    	 	'text' => 'Enviar'
	    	],
	    	[
	    		'type' => 'button',
	    	 	'class' => 'danger',
	    	 	'text' => 'Cancel',
	    	 	'action' => 'Back'
	    	]

	    ]
	])@endcomponent

</form>