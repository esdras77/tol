<style>
	
	.form-group {width: 25% !important;}

</style>


<h1>Inspecciones</h1>

<hr>

@foreach ($inspections as $inspection)
			
		@if ($loop->index%4 == 0)
			<div class="row">	
		@endif

			@component('components.form.checkbox', [
			    'name' => $inspection->numRefTuv,
			    'label' => $inspection->numRefTuv,
			    'object' => \App\Inspeccion::find($inspection->idLotus)->abletobeshownto($user)
			])@endcomponent

		@if ($loop->index%4 == 3 || $loop->last)
			</div>
		@endif

@endforeach

<hr>

<h1>Operadores</h1>

<hr>

@foreach ($operators as $operator)
			
		@if ($loop->index%4 == 0)
			<div class="row">	
		@endif

			@component('components.form.checkbox', [
			    'name' => $operator->numRefTuv,
			    'label' => $operator->numRefTuv,
			    'object' => \App\Operador::find($operator->idLotus)->abletobeshownto($user)
			])@endcomponent

		@if ($loop->index%4 == 3 || $loop->last)
			</div>
		@endif

@endforeach