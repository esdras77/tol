@extends('layouts.main')

@section('breadcrumb')
	@include('components.breadcrumb.breadcrumb')
@endsection

@section('content')
	
	<div class="container mt-4">

		<h1>Configuracion para inspecciones y operadores</h1>
				
		<div class="row">
			<table class="table table-hover mt-5">
			  	<thead>
				    <tr>
						<th scope="col">Oficina</th>
						<th scope="col">Empresa</th>
						<th scope="col">

						</th>
				    </tr>
			  	</thead>
			  	<tbody>
					@foreach ($offices as $office)
					<tr scope="row">
						<td>{{ $office->nombre }}</td>
						<td>{{ $office->enterprise->nombre }}</td>
						<td>
							<button class="btn btn-outline-secondary btn-sm" title="Documentos"
									onclick="documentSelection('{{ $office->idOficina }}', {{ $user->idUsuario }})">
								<i data-feather="archive"></i>
							</button>
						</td>
					</tr>
					@endforeach
			  	</tbody>
			</table>
			
			<div class="flex">{{$offices->appends(request()->all())->onEachSide(3)->links()}}</div>
			
		</div>

	</div>

	<div class="modal fade" 
	     id="inspections_list" 
	     tabindex="-1" role="dialog" aria-labelledby="alternative" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">
	          <span class="align-middle">Listado de documentos de inspección</span>
	        </h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	          <form action="">
				<input type="hidden" value="ZALO" name="TEST">
	          	<div id="form-inside"></div>
	          </form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="button" class="btn btn-primary" id="submit">Save changes</button>
	      </div>
	    </div>
	  </div>
	</div>

@endsection

@section('scripts')
	
	<script type="text/javascript">
		
		function documentSelection(idOffice, idUser){

			var route = '{{ route('administration.inspection.documents') }}';
		    var params = {"_token": "{{ csrf_token() }}", 'idUsuario': idUser, 'idOficina': idOffice};

		    $("#inspections_list .modal-dialog .modal-content .modal-body #form-inside").html();
		    $.post(route, params, function(htmlrecover){
				//carga del formulario en el cuadro
				$("#inspections_list .modal-dialog .modal-content .modal-body #form-inside").html(htmlrecover);
			});

			$("#inspections_list").modal('toggle');

		}

	</script>

@endsection