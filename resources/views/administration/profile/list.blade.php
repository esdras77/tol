@extends('layouts.main')

@section('breadcrumb')
	@include('components.breadcrumb.breadcrumb')
@endsection

@section('content')
	
	@include('administration.profile.navtitle')

	<table class="table table-hover mt-5">
	  	<thead>
		    <tr>
				<th scope="col">#</th>
				<th scope="col">Nombre</th>
				<th scope="col">Descripción</th>
				<th scope="col">
					<button class="btn btn-success btn-sm mr-3" title="Nuevo perfil" onclick="javascript: window.location.href='{{ route('administration.profile.create')}}'"><i data-feather="shield"></i></button>	
				</th>
		    </tr>
	  	</thead>
	  	<tbody>
			@foreach ($profiles as $profile)
			<tr>
				<th scope="row">{{ $profile->idPerfil }}</th>
				<td class="w-25">{{ $profile->nombre }}</td>
				<td class="w-50"><p class="w-75">{{ $profile->descripcion }}</p></td>
				<td>
					<button class="btn btn-outline-secondary btn-sm" title="View perfil"
							onclick="javascript: window.location.href='{{ route('administration.profile.show', $profile->idPerfil) }}'">
						<i data-feather="eye"></i>
					</button>

					<button class="btn btn-outline-secondary btn-sm" title="Edit" 
							onclick="javascript: window.location.href='{{ route('administration.profile.edit', $profile->idPerfil) }}'">
						<i data-feather="edit"></i>
					</button>

					<form style="display: inline;" method="POST" action={{ route('administration.profile.delete', $profile->idPerfil) }}>
			      		{!! csrf_field() !!}
			      		{!! method_field('delete') !!}
			      		<button class="btn btn-outline-danger btn-sm" type="submit" title="Delete"><i data-feather="trash"></i> </button>	
			      	</form>

			      	<button class="btn btn-outline-info btn-sm" title="Menu" 
							onclick="javascript: window.location.href='{{ route('administration.profile.menuadmin', [$profile->idMenu, null]) }}'">
						<i data-feather="menu"></i>
					</button>
					
				</td>
			</tr>
			@endforeach
	  	</tbody>
	</table>
	
	<div class="flex">{{$profiles->appends(request()->all())->onEachSide(3)->links()}}</div>

@endsection