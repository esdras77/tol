@component('components.lists.navtitle',[
	'title' => 'Listado de Perfiles',
	'formTitle' => 'Buscar perfiles',
	'form' => [
		'formAction' => 'administration.profile.find',
		'formComponents' => [
			['input', 'nombre', 'Nombre']
		]
	]
])
@endcomponent