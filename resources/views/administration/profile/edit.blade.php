@extends('layouts.main')

@section('breadcrumb')
	@include('components.breadcrumb.breadcrumb')
@endsection

@section('content')


<div class="container mt-4">
	
	<h1>Editar perfil: {{ $profile->nombre }}</h1>
	
	<form method="post" action="{{ route('administration.profile.update', $profile->idPerfil) }}">
		
		{!! method_field('PUT') !!}

		@include('administration.profile.form')
		
	</form>

</div>


@endsection