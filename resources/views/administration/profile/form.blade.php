{!! csrf_field() !!}

@component('components.form.input', [
    'name' => 'nombre',
    'label' => 'Nombre',
    'object' => $profile,
    'attributes' => 'required autofocus'
])@endcomponent

@component('components.form.textarea', [
    'name' => 'descripcion',
    'label' => 'Descripción',
    'object' => $profile
])@endcomponent

@component('components.form.select', [
    'name' => 'mainPerfil',
    'label' => 'Perfil padre',
    'options' => $profiles->pluck('nombre', 'idPerfil'),
    'object' => $profile,
    'comparison' => 'mainPerfil',
    'attributes' => 'required',
    'multiple' => false
])@endcomponent

@component('components.form.checkbox', [
    'name' => 'administrador',
    'label' => 'Administra usuarios en el sitio',
    'object' => $profile
])@endcomponent

@component('components.form.buttons', [
    'buttons' => [
    	[
    		'type' => 'submit',
    	 	'class' => 'success',
    	 	'text' => 'Enviar'
    	],
    	[
    		'type' => 'reset',
    	 	'class' => '',
    	 	'text' => 'Reset'
    	],
    	[
    		'type' => 'button',
    	 	'class' => 'danger',
    	 	'text' => 'Cancel',
    	 	'action' => 'Back'
    	]

    ]
])@endcomponent