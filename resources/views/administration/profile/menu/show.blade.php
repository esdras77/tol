@extends('layouts.main')

@section('breadcrumb')
	@include('components.breadcrumb.breadcrumb')
@endsection

@section('content')

<div class="container w-75">
	<div class="pos-f-t">
		<nav class="navbar navbar-light bg-light">

			<h1>Edición de menu: {{ $menu->nombre }}</h1>

		</nav>
	</div>

	@foreach ($items as $menuItem)

		@if($loop->iteration %3 == 1)
			<div class="card-group">
		@endif
		
		<div class="card shadow float-left mb-3 mr-3" style="width: 18rem;">

			<div class="card-header">
				<h5 class="card-title float-left mt-2"><span>@lang('tol.'.$menuItem->descripcion)</span></h5>
				
				@if ($menuItem->hasChildren())
				    <button type="button" class="btn btn-outline-primary float-right"
				     		title="Sub-items"
				    		onclick="javascript: window.location.href='{{ route('administration.profile.menuadmin', [$menu->idMenu, $menuItem->idItemMenu]) }}'">
						<i data-feather="arrow-down-circle"></i>
					</button>
				@endif
				
		  	</div>
			
			<div class="card-body">

			    @if(!empty($menuItem->textoweb))
			    	<p class="card-text">@lang('tol.'.$menuItem->textoweb)</p>
			    @endif
		    
		  	</div>
		  	<div class="card-footer">

		  		<form method="POST" action="{{ route('administration.menuadmin') }}">
					{{ csrf_field() }}
					<input type="hidden" name="idMenu" value="{{ $menu->idMenu }}">
					<input type="hidden" name="idItemMenu" value="{{ $menuItem->idItemMenu }}">
			  		<div class="custom-control custom-checkbox">
			  			
				    	<input type="checkbox" 
				    		   id="updateMenu{{ $menuItem->idItemMenu }}" 
				    		   name="updateMenu{{ $menuItem->idItemMenu }}"
				    		   value="{{ $menuItem->idItemMenu }}" 
				    		   class="custom-control-input"
				    	
				    		   @if ($menuItem->isInMenu($menu))
				    		   {{ "checked" }}
				    		   @endif

				    		   onclick="submit()" 
				    	>
				    	<label class="custom-control-label" for="updateMenu{{ $menuItem->idItemMenu }}">Incorporar item al menu</label>
				    </div>
				</form>

		    </div>
		</div>

		@if($loop->iteration %3 == 0)
			</div>
		@endif

	@endforeach


</div>

@endsection

