@extends('layouts.main')

@section('breadcrumb')
	@include('components.breadcrumb.breadcrumb')
@endsection

@section('content')

<div class="container mt-4">
	
	<h1>Crear un nuevo perfil</h1>
	
	<form method="post" action="{{ route('administration.profile.store') }}">

		@include('administration.profile.form')
		
	</form>

</div>


@endsection