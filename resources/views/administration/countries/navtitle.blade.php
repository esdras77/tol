@component('components.lists.navtitle',[
	'title' => 'Listado de Paises',
	'formTitle' => 'Buscar pais',
	'form' => [
		'formAction' => 'administration.country.find',
		'formComponents' => [
			['input', 'nombre', 'Nombre'],
			['input', 'iso_code', 'Código ISO']
		]
	]
])
@endcomponent