@extends('layouts.main')

@section('breadcrumb')
	@include('components.breadcrumb.breadcrumb')
@endsection

@section('content')

	@include('administration.countries.navtitle')

	<table class="table table-hover mt-5">
	  	<thead>
		    <tr>
				<th scope="col">#</th>
				<th scope="col">Nombre</th>
				<th scope="col">Código ISO</th>
				<th scope="col">Moneda</th>
				<th scope="col">
					
				</th>
		    </tr>
	  	</thead>
	  	<tbody>
			@foreach ($countries as $country)
			<tr>
				<th scope="row">{{ $country->idPais }}</th>
				<td>{{ $country->nombre }}</td>
				<td>{{ $country->iso_code }}</td>
				<td>
					{!! (!empty($country->currency)) ? '<strong>'.$country->currency.'</strong>' : '<span class="text-muted">Undefined</span>' !!}
				</td>
				<td class="w-25">

				</td>
			</tr>
			@endforeach
	  	</tbody>
	</table>

	<div class="flex">{{ $countries->appends(request()->all())->onEachSide(3)->links() }}</div>

@endsection