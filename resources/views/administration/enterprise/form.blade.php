{!! csrf_field() !!}

@component('components.form.hidden', [
    'name' => 'idEmpresa',
    'type' => 'hidden',
    'object' => $enterprise,
])@endcomponent

@component('components.form.hidden', [
    'name' => 'idTipoCuenta',
    'type' => 'hidden',
    'object' => $enterprise,
])@endcomponent

@component('components.form.input', [
    'name' => 'nombre',
    'label' => 'Nombre',
    'object' => $enterprise,
    'attributes' => 'required autofocus'
])@endcomponent

@component('components.form.select', [
    'name' => 'tipoEmpresa',
    'label' => 'Tipo de empresa',
    'options' => \App\TipoEmpresa::where('enabled', 1)->pluck('descripcion', 'tipoEmpresa'),
    'object' => $enterprise,
    'comparison' => 'tipoEmpresa',
    'attributes' => 'required',
    'multiple' => false
])@endcomponent

@component('components.form.input', [
    'name' => 'sapCode',
    'label' => 'Código SAP',
    'object' => $enterprise,
    'attributes' => 'required'
])@endcomponent

@component('components.form.input', [
    'name' => 'BK',
    'label' => 'Código de compañía',
    'object' => $enterprise,
    'attributes' => 'required'
])@endcomponent

@component('components.form.buttons', [
    'buttons' => [
    	[
    		'type' => 'submit',
    	 	'class' => 'success',
    	 	'text' => 'Siguiente'
    	],
    	[
    		'type' => 'reset',
    	 	'class' => '',
    	 	'text' => 'Reset'
    	],
    	[
    		'type' => 'button',
    	 	'class' => 'danger',
    	 	'text' => 'Cancel',
    	 	'action' => 'Back'
    	]

    ]
])@endcomponent