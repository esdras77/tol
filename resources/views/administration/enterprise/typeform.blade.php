{!! csrf_field() !!}

@component('components.form.hidden', [
    'name' => 'idProductos_empresasolicitante',
    'type' => 'hidden',
    'object' => $solicitant,
])@endcomponent

@component('components.form.hidden', [
    'name' => 'idEmpresa',
    'type' => 'hidden',
    'object' => $enterprise,
])@endcomponent

@component('components.form.input', [
    'name' => 'cuit',
    'label' => 'CUIT',
    'object' => $solicitant,
    'atributes' => 'required'
])@endcomponent

@component('components.form.input', [
    'name' => 'rubro',
    'label' => 'Rubro',
    'object' => $solicitant,
    'atributes' => 'required'
])@endcomponent

@component('components.form.input', [
    'name' => 'responsableLegal',
    'label' => 'Responsable legal',
    'object' => $solicitant,
    'atributes' => 'required'
])@endcomponent

@component('components.form.input', [
    'name' => 'cargo',
    'label' => 'Cargo',
    'object' => $solicitant,
    'atributes' => 'required'
])@endcomponent

@component('components.form.input', [
    'name' => 'personaDeContacto',
    'label' => 'Persona de contacto',
    'object' => $solicitant,
    'atributes' => 'required'
])@endcomponent

@component('components.form.input', [
    'name' => 'emailDeContacto',
    'label' => 'Email de contacto',
    'object' => $solicitant,
    'atributes' => 'required'
])@endcomponent

@component('components.form.input', [
    'name' => 'telefonoDeContacto',
    'label' => 'Teléfono de contacto',
    'object' => $solicitant,
    'atributes' => 'required'
])@endcomponent

@component('components.form.input', [
	'type' => 'file',
    'name' => 'logo',
    'label' => 'Logo',
    'object' => $solicitant,
    'atributes' => 'required',
    'hint' => 'Accep only JPG and GIF (no animated)'
])@endcomponent

@component('components.form.input', [
    'name' => 'personaDeContactoTecnico',
    'label' => 'Persona de contacto (Técnico)',
    'object' => $solicitant,
    'atributes' => 'required'
])@endcomponent

@component('components.form.input', [
    'name' => 'emailDeContactoTecnico',
    'label' => 'Email de contacto (Técnico)',
    'object' => $solicitant,
    'atributes' => 'required'
])@endcomponent

@component('components.form.input', [
    'name' => 'telefonoDeContactoTecnico',
    'label' => 'Teléfono de contacto (Técnico)',
    'object' => $solicitant,
    'atributes' => 'required'
])@endcomponent

@component('components.form.buttons', [
    'buttons' => [
    	[
    		'type' => 'submit',
    	 	'class' => 'success',
    	 	'text' => 'Finalizar'
    	],
    	[
    		'type' => 'reset',
    	 	'class' => '',
    	 	'text' => 'Reset'
    	]
    ]
])@endcomponent