@extends('layouts.main')

@section('breadcrumb')
	@include('components.breadcrumb.breadcrumb')
@endsection

@section('content')

<div class="container mt-4">
	
	<h1>Editar empresa: {{ $enterprise->nombre }}</h1>
	
	<form method="post" action="{{ route('administration.enterprises.update', $enterprise->idEmpresa) }}">

		{!! method_field('PUT') !!}

		@include('administration.enterprise.form')
		
	</form>

</div>


@endsection