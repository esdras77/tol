@extends('layouts.main')

@section('breadcrumb')
	@include('components.breadcrumb.breadcrumb')
@endsection

@section('content')

	@include('administration.enterprise.navtitle')

	<table class="table table-hover mt-5">
	  	<thead>
		    <tr>
				<th scope="col">#</th>
				<th scope="col">Nombre</th>
				<th scope="col">Tipo de empresa</th>
				<th scope="col">Código SAP</th>
				<th scope="col">
					<button class="btn btn-success btn-sm mr-3" title="Nueva empresa" onclick="javascript: window.location.href='{{ route('administration.enterprises.create')}}'"><i data-feather="cpu"></i></button>	
				</th>
		    </tr>
	  	</thead>
	  	<tbody>
			@foreach ($enterprises as $enterprise)
			<tr>
				<th scope="row">{{ $loop->iteration }}</th>
				<td class="w-25">{{ $enterprise->nombre }}</td>
				<td class="w-33">{{ $enterprise->tipo->descripcion }}</td>
				<td>{{ $enterprise->sapCode }}</td>
				<td>
					<button class="btn btn-outline-secondary btn-sm" title="View enterprise"
							onclick="javascript: window.location.href='{{ route('administration.enterprises.show', $enterprise->idEmpresa) }}'">
						<i data-feather="eye"></i>
					</button>

					<button class="btn btn-outline-secondary btn-sm" title="Edit" 
							onclick="javascript: window.location.href='{{ route('administration.enterprises.edit', $enterprise->idEmpresa) }}'">
						<i data-feather="edit"></i>
					</button>

					<button class="btn btn-outline-secondary btn-sm" title="Offices"
							onclick="javascript: window.location.href='{{ route('administration.offices', $enterprise->idEmpresa) }}'">
						<i data-feather="grid"></i>
					</button>

					<form style="display: inline;" method="POST" action={{ route('administration.enterprises.delete', $enterprise->idEmpresa) }}>
			      		{!! csrf_field() !!}
			      		{!! method_field('delete') !!}
			      		<button class="btn btn-outline-danger btn-sm" type="submit" title="Delete"><i data-feather="trash"></i> </button>	
			      	</form>
					
				</td>
			</tr>
			@endforeach
	  	</tbody>
	</table>
	
	<div class="flex">{{$enterprises->appends(request()->all())->onEachSide(3)->links()}}</div>

@endsection