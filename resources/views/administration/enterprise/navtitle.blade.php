@component('components.lists.navtitle',[
	'title' => 'Listado de Empresas',
	'formTitle' => 'Buscar empresas',
	'form' => [
		'formAction' => 'administration.enterprise.find',
		'formComponents' => [
			['input', 'nombre', 'Nombre'],
			['select', 'tipoEmpresa', 'Tipo de empresa', \App\TipoEmpresa::getOptions()->pluck('descripcion', 'tipoEmpresa')]
		]
	]
])
@endcomponent