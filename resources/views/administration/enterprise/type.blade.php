@extends('layouts.main')

@section('breadcrumb')
	@include('components.breadcrumb.breadcrumb')
@endsection

@section('content')

<div class="container mt-4">

		<h1>Datos de la empresa {{ $enterprise->nombre }} para solicitud de servicios</h1>

		<img src="{{ url('storage/'.$solicitant->logo) }}" alt="..." class="img-thumbnail position-absolute" style="max-width: 350px; left: 64%; z-index: 2;">
		
		<form method="post" action="{{ route('administration.enterprises.storetype') }}" enctype="multipart/form-data">

			@include('administration.enterprise.typeform')
			
		</form>
			
		

</div>


@endsection