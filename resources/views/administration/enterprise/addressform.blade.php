{!! csrf_field() !!}

@component('components.form.hidden', [
    'name' => 'idDireccion',
    'type' => 'hidden',
    'object' => $address,
])@endcomponent

@component('components.form.hidden', [
    'name' => 'idEmpresa',
    'type' => 'hidden',
    'object' => $enterprise,
])@endcomponent

@component('components.form.input', [
    'name' => 'direccion',
    'label' => 'Dirección',
    'object' => $address,
    'atributes' => 'required'
])@endcomponent

@component('components.form.input', [
    'name' => 'localidad',
    'label' => 'Localidad',
    'object' => $address,
    'atributes' => 'required'
])@endcomponent

@component('components.form.input', [
    'name' => 'ciudad',
    'label' => 'Ciudad',
    'object' => $address,
    'atributes' => 'required'
])@endcomponent

@component('components.form.input', [
    'name' => 'codigoPostal',
    'label' => 'Código postal',
    'object' => $address,
    'atributes' => 'required'
])@endcomponent

@component('components.form.input', [
    'name' => 'provincia',
    'label' => 'Provincia',
    'object' => $address,
    'atributes' => 'required'
])@endcomponent

@component('components.form.select', [
    'name' => 'idPais',
    'label' => 'Nacionalidad',
    'options' => $countries->pluck('nombre', 'idPais'),
    'object' => $address,
    'comparison' => 'idPais',
    'atributes' => 'required',
    'multiple' => false
])@endcomponent

@component('components.form.buttons', [
    'buttons' => [
    	[
    		'type' => 'submit',
    	 	'class' => 'success',
    	 	'text' => 'Siguiente'
    	],
    	[
    		'type' => 'reset',
    	 	'class' => '',
    	 	'text' => 'Reset'
    	]
    ]
])@endcomponent