@extends('layouts.main')

@section('breadcrumb')
	@include('components.breadcrumb.breadcrumb')
@endsection

@section('content')

<div class="container mt-4">
	
	<h1>Editar officina para: {{ $enterprise->nombre }}</h1>
	
	<form method="post" action="{{ route('administration.offices.update', $office->idOficina) }}">

		{!! method_field('PUT') !!}

		@include('administration.enterprise.office.form')
		
	</form>

</div>


@endsection