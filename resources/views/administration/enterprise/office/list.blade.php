@extends('layouts.main')

@section('breadcrumb')
	@include('components.breadcrumb.breadcrumb')
@endsection

@section('content')

	<h2 class="mt-5">Oficinas de {{ $enterprise->nombre }}</h2>

	<table class="table table-hover mt-5">
	  	<thead>
		    <tr>
				<th scope="col">#</th>
				<th scope="col">Nombre</th>
				<th scope="col">Teléfono</th>
				<th scope="col">
					<button class="btn btn-success btn-sm mr-3" title="Nueva oficina" onclick="javascript: window.location.href='{{ route('administration.offices.create', $enterprise->idEmpresa)}}'"><i data-feather="map-pin"></i></button>	
				</th>
		    </tr>
	  	</thead>
	  	<tbody>
	  		@if (!empty($offices))

				@foreach ($offices as $office)
				<tr>
					<th scope="row">{{ $loop->iteration }}</th>
					<td class="w-50">{{ $office->nombre }}</td>
					<td class="w-33">{{ $office->telefono1 }}</td>
					<td>
						<button class="btn btn-outline-secondary btn-sm" title="View office"
								onclick="javascript: window.location.href='{{ route('administration.offices.show', $office->idOficina) }}'">
							<i data-feather="eye"></i>
						</button>

						<button class="btn btn-outline-secondary btn-sm" title="Edit" 
								onclick="javascript: window.location.href='{{ route('administration.offices.edit', $office->idOficina) }}'">
							<i data-feather="edit"></i>
						</button>

						<form style="display: inline;" method="POST" action={{ route('administration.offices.delete', $office->idOficina) }}>
				      		{!! csrf_field() !!}
				      		{!! method_field('delete') !!}
				      		<button class="btn btn-outline-danger btn-sm" type="submit" title="Delete"><i data-feather="trash"></i> </button>	
				      	</form>
						
					</td>
				</tr>
				@endforeach
			
			@else 
				<tr><td colspan="4" class="text-center h3">No hay resultados para esta solicitud</td></tr>
	  		@endif
	  	</tbody>
	</table>
	
	@if (!empty($offices))
		<div class="flex">{{ $offices->appends(request()->all())->onEachSide(3)->links() }}</div>
	@endif

@endsection