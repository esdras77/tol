{!! csrf_field() !!}

@component('components.form.hidden', [
    'name' => 'idEmpresa',
    'type' => 'hidden',
    'object' => $enterprise,
])@endcomponent

@component('components.form.hidden', [
    'name' => 'idOficina',
    'type' => 'hidden',
    'object' => $office,
])@endcomponent

@component('components.form.input', [
    'name' => 'nombre',
    'label' => 'Nombre',
    'object' => $office,
    'attributes' => 'required autofocus'
])@endcomponent

@component('components.form.input', [
    'name' => 'telefono1',
    'label' => 'Teléfono',
    'object' => $office,
    'attributes' => 'required'
])@endcomponent

@component('components.form.input', [
    'name' => 'direccion',
    'label' => 'Dirección',
    'object' => $office->address,
    'attributes' => 'required'
])@endcomponent

@component('components.form.input', [
    'name' => 'localidad',
    'label' => 'Localidad',
    'object' => $office->address,
    'attributes' => 'required'
])@endcomponent

@component('components.form.input', [
    'name' => 'ciudad',
    'label' => 'Ciudad',
    'object' => $office->address,
    'attributes' => 'required'
])@endcomponent

@component('components.form.input', [
    'name' => 'codigoPostal',
    'label' => 'Código postal',
    'object' => $office->address,
    'attributes' => 'required'
])@endcomponent

@component('components.form.input', [
    'name' => 'provincia',
    'label' => 'Provincia',
    'object' => $office->address,
    'attributes' => 'required'
])@endcomponent

@component('components.form.select', [
    'name' => 'idPais',
    'label' => 'Nacionalidad',
    'options' => $countries->pluck('nombre', 'idPais'),
    'object' => $office->address,
    'comparison' => 'idPais',
    'attributes' => 'required',
    'multiple' => false
])@endcomponent

@component('components.form.buttons', [
    'buttons' => [
    	[
    		'type' => 'submit',
    	 	'class' => 'success',
    	 	'text' => 'Guardar'
    	],
    	[
    		'type' => 'reset',
    	 	'class' => '',
    	 	'text' => 'Reset'
    	],
    	[
    		'type' => 'button',
    	 	'class' => 'danger',
    	 	'text' => 'Cancel',
    	 	'action' => 'Back'
    	]

    ]
])@endcomponent