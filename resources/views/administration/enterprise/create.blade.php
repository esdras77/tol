@extends('layouts.main')

@section('breadcrumb')
	@include('components.breadcrumb.breadcrumb')
@endsection

@section('content')

<div class="container mt-4">
	
	<h1>Crear una nueva empresa</h1>
	
	<form method="post" action="{{ route('administration.enterprises.store') }}">

		@include('administration.enterprise.form')
		
	</form>

</div>


@endsection