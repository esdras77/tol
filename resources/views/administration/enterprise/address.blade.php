@extends('layouts.main')

@section('breadcrumb')
	@include('components.breadcrumb.breadcrumb')
@endsection

@section('content')

<div class="container mt-4">
	
	<h1>Dirección de {{ $enterprise->nombre }}</h1>
	<form method="post" action="{{ route('administration.enterprises.storeaddress') }}">

		@include('administration.enterprise.addressform')
		
	</form>

</div>


@endsection