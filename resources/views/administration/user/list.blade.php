@extends('layouts.main')

@section('breadcrumb')
	@include('components.breadcrumb.breadcrumb')
@endsection

@section('content')

	@include('administration.user.navtitle')

	<table class="table table-hover mt-5">
	  	<thead>
		    <tr>
				<th scope="col">#</th>
				<th scope="col">Nombre</th>
				<th scope="col">Apellido</th>
				<th scope="col">Email</th>
				<th scope="col">Empresa</th>
				<th scope="col">
					<button class="btn btn-success btn-sm mr-3" title="Nuevo usuario" onclick="javascript: window.location.href='{{ route('administration.user.create')}}'"><i data-feather="user-plus"></i></button>	
				</th>
		    </tr>
	  	</thead>
	  	<tbody>
			@foreach ($users as $user)
			<tr>
				<th scope="row">{{ $user->idUsuario }}</th>
				<td>{{ $user->nombre }}</td>
				<td>{{ $user->apellido }}</td>
				<td>{{ $user->email }}</td>
				<td>{{ 'empresa' }}</td>
				<td>
					<button class="btn btn-outline-secondary btn-sm" title="View user"
							onclick="javascript: window.location.href='{{ route('administration.user.show', $user->idUsuario) }}'">
						<i data-feather="eye"></i>
					</button>

					<button class="btn btn-outline-secondary btn-sm" title="Edit" 
							onclick="javascript: window.location.href='{{ route('administration.user.edit', $user->idUsuario) }}'">
						<i data-feather="edit"></i>
					</button>

					<button class="btn btn-outline-secondary btn-sm" title="Enterprise"
							onclick="javascript: window.location.href='{{ route('administration.user.offices', $user->idUsuario) }}'">
						<i data-feather="cast"></i>
					</button>
					
					@if (!empty($user->departamento))
						@switch($user->departamento->prefix)
						    @case('SI')
						        <button class="btn btn-outline-secondary btn-sm" title="Inspecciones"
										onclick="javascript: window.location.href='{{ route('administration.user.inspections', $user->idUsuario) }}'">
									<i data-feather="archive"></i>
								</button>
								<button class="btn btn-outline-secondary btn-sm" title="ISPM"
										onclick="javascript: window.location.href='{{ route('administration.user.ispm', $user->idUsuario) }}'">
									<div class="mb-1 mt-1">ISPM</div>
								</button>
						        @break

						    @case('PR')
						    	<button class="btn btn-outline-secondary btn-sm" title="Certificaciones"
										onclick="javascript: window.location.href='{{ route('administration.user.certifies', $user->idUsuario) }}'">
									<i data-feather="command"></i>
								</button>
								@break
						@endswitch
					@endif

					<form style="display: inline;" method="POST" action={{ route('administration.user.delete', $user->idUsuario) }}>
			      		{!! csrf_field() !!}
			      		{!! method_field('delete') !!}
			      		<button class="btn btn-outline-danger btn-sm" type="submit" title="Delete"><i data-feather="trash"></i> </button>	
			      	</form>
					
				</td>
			</tr>
			@endforeach
	  	</tbody>
	</table>
	
	<div class="flex">{{$users->appends(request()->all())->onEachSide(3)->links()}}</div>

@endsection