@extends('layouts.main')

@section('breadcrumb')
	@include('components.breadcrumb.breadcrumb')
@endsection

@section('content')

<div class="container mt-4">
	
	<h1>Crear un nuevo usuario</h1>
	
	<form method="post" action="{{ route('administration.user.store') }}">

		@include('administration.user.form')
		
	</form>

</div>


@endsection