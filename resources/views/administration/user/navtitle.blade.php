@component('components.lists.navtitle',[
	'title' => 'Listado de Usuarios',
	'formTitle' => 'Buscar usuarios',
	'form' => [
		'formAction' => 'administration.user.find',
		'formComponents' => [
			['input', 'nombre', 'Nombre'],
			['input', 'email', 'Email'],
			['input', 'username', 'Nombre de usuario'],
			['select', 'idDepartamento', 'Departamento', \App\Departamento::all()->pluck('nombre', 'idDepartamento')]
		]
	]
])
@endcomponent