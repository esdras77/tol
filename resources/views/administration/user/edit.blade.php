@extends('layouts.main')

@section('breadcrumb')
	@include('components.breadcrumb.breadcrumb')
@endsection

@section('content')


<div class="container mt-4">
	
	<h1>Editar usuario</h1>
	
	<form method="post" action="{{ route('administration.user.update', $user->idUsuario) }}">
		
		{!! method_field('PUT') !!}

		@include('administration.user.form')
		
	</form>

</div>


@endsection