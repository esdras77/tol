
{!! csrf_field() !!}

@component('components.form.input', [
    'name' => 'nombre',
    'label' => 'Nombre',
    'object' => $user,
    'attributes' => 'required autofocus'
])@endcomponent

@component('components.form.input', [
    'name' => 'apellido',
    'label' => 'Apellido',
    'object' => $user,
    'attributes' => 'required'
])@endcomponent

@component('components.form.select', [
    'name' => 'idPais',
    'label' => 'Nacionalidad',
    'options' => $countries->pluck('nombre', 'idPais'),
    'object' => $user,
    'comparison' => 'idPais',
    'attributes' => 'required',
    'multiple' => false
])@endcomponent

@component('components.form.input', [
    'name' => 'username',
    'label' => 'Email',
    'object' => $user,
    'attributes' => 'required'
])@endcomponent

@if( request()->is('administration.user.create'))
@component('components.form.input', [
    'name' => 'password',
    'label' => 'Password',
    'type' => 'password',
    'object' => $user,
    'attributes' => 'required',
    'multiple' => false
])@endcomponent
@endif

@component('components.form.input', [
    'name' => 'telefono',
    'label' => 'Teléfono principal',
    'object' => $user,
    'attributes' => 'required'
])@endcomponent

@component('components.form.input', [
    'name' => 'celular',
    'label' => 'Celular',
    'object' => $user
])@endcomponent

@component('components.form.select', [
    'name' => 'idPerfil',
    'label' => 'Perfil',
    'options' => $profiles->pluck('nombre', 'idPerfil'),
    'object' => $user,
    'comparison' => 'idPerfil',
    'attributes' => 'required',
    'multiple' => false
])@endcomponent

@component('components.form.select', [
    'name' => 'idDepartamento',
    'label' => 'Departamento',
    'options' => $departments->pluck('nombre', 'idDepartamento'),
    'object' => $user,
    'comparison' => 'idDepartamento',
    'attributes' => 'required',
    'multiple' => false
])@endcomponent

@component('components.form.buttons', [
    'buttons' => [
    	[
    		'type' => 'submit',
    	 	'class' => 'success',
    	 	'text' => 'Enviar'
    	],
    	[
    		'type' => 'reset',
    	 	'class' => '',
    	 	'text' => 'Reset'
    	],
    	[
    		'type' => 'button',
    	 	'class' => 'danger',
    	 	'text' => 'Cancel',
    	 	'action' => 'Back'
    	]

    ]
])@endcomponent