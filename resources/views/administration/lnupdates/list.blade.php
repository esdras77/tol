@extends('layouts.main')

@section('breadcrumb')
	@include('components.breadcrumb.breadcrumb')
@endsection

@section('content')

	<table class="table table-hover mt-5">
	  	<thead>
		    <tr>
				<th scope="col">Área de sistema</th>
				<th scope="col">Última actualización</th>
				<th scope="col">
					
				</th>
		    </tr>
	  	</thead>
	  	<tbody>
			@foreach ($updates as $update)
			<tr>
				<td>{{ $update->areaDeSistema }}</td>
				<td>{{ $update->ultimaFechaActualizacion }}</td>
				<td class="w-25">

				</td>
			</tr>
			@endforeach
	  	</tbody>
	</table>

	<div class="flex">{{ $updates->appends(request()->all())->onEachSide(3)->links() }}</div>

@endsection