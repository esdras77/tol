@extends('layouts.main')


@section('news')

	@if(!empty($news))
	<div style="margin-top: -10px;">
		<div class="onoffswitch3">
		    <input type="checkbox" name="onoffswitch3" class="onoffswitch3-checkbox" id="myonoffswitch3" checked>
		    <label class="onoffswitch3-label" for="myonoffswitch3">
		        <span class="onoffswitch3-inner">
		            <span class="onoffswitch3-active rounded">
		                <marquee class="scroll-text">

		                	@foreach($news as $newsHead)
		                	{{ $newsHead->descripcion }}
		                	<i data-feather="fast-forward"></i>
							@endforeach

		                </marquee>
		                <span class="onoffswitch3-switch rounded">
		                	NOTICIAS
		                	<i data-feather="x-circle"></i>
		            	</span>
		            </span>
		            <span class="onoffswitch3-inactive"><span class="onoffswitch3-switch">NOTICIAS</span></span>
		        </span>
		    </label>
		</div>
	</div>
	@endif

@endsection

@section('breadcrumb')
	@include('components.breadcrumb.breadcrumb')
@endsection

@section('content')

	@include('layouts.tilesmenu')

@endsection