@extends('layouts.tol')

@section('content')

<div class="container">
	<div class="row justify-content-md-center">
		<div class="col-lg offset-md-2">
			
			<form action="{{ route('resetpassword') }}" method="POST">
				{{ csrf_field() }}

				 <div class="form-group">
			    	<label for="username" class="col-sm-3 control-label">
			            Username</label>
			        <div class="col-sm-9">
			            <input type="email" 
			            	   class="form-control {{ $errors->has('username') ? 'is-invalid' : '' }}" 
			            	   id="username"
			            	   value = "{{ old('username') }}"
			            	   name="username"
			            	   placeholder="Username" required>
			            {!! $errors->first('username', '<div class=invalid-feedback>:message</div>') !!}

			            <small id="passwordHelpBlock" class="form-text text-muted">
						  Please enter your username and an email will be sent to your registered email address.
						</small>
			        </div>
			        <div class="form-group last mt-3">
			            <div class="col-md-offset-3 col-md	-9">
			                <button type="submit" class="btn btn-success btn-sm">
			                    Change password</button>
			                     <button type="reset" class="btn btn-default btn-sm">
			                    Reset</button>
			            </div>
			        </div>

			    </div>

			</form>

		</div>
	</div>

</div>


@endsection