@extends('layouts.tol')

<style>
    .login-panel { 
        background: url(/images/general/login.jpg) no-repeat center center fixed; 
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
        background-position: 80px 124px;
    }

    .panel-default {
        opacity: 0.9;
        margin-top:30px;
    }
    .form-group.last { margin-bottom:0px; }
</style>


@section('content')

    <div class="row login-panel">
        <div class="col-md-4 col-md-offset-4"></div>

        <div class="col-md-8 col-md-offset-8">
            <div class="panel panel-default">
                <div class="panel-heading h3">
                    <div class="panel-title">Reset your password</div>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('password.request') }}" novalidate>
                    
                    {{ csrf_field() }}
                    <input type="hidden" name="token" value="{{ $token }}">

                    <div class="form-group">
                        <label for="username" class="col-sm-3 control-label">
                            Username</label>
                        <div class="col-sm-9">
                            <input type="email" 
                                   class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" 
                                   id="email"
                                   value = "{{ old('email') }}"
                                   name="email"
                                   placeholder="Username" required>
                            {!! $errors->first('email', '<div class=invalid-feedback>:message</div>') !!}
                        </div>
                        
                    </div>
                    <div class="form-group">
                        
                        <label for="password" class="col-sm-3 control-label">
                            Password</label>
                        <div class="col-sm-9">
                            <input type="password" 
                                   class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" 
                                   id="password"
                                   value = "{{ old('password') }}"
                                   name="password"
                                   placeholder="Password" required>
                            {!! $errors->first('password', '<div class=invalid-feedback>:message</div>') !!}
                        </div>
                        
                    </div>

                    <div class="form-group">
                        
                        <label for="password_confirmation" class="col-sm-3 control-label">
                            Confirm password</label>
                        <div class="col-sm-9">
                            <input type="password" 
                                   class="form-control {{ $errors->has('password_confirmation') ? 'is-invalid' : '' }}" 
                                   id="password_confirmation"
                                   value = "{{ old('password') }}"
                                   name="password_confirmation"
                                   placeholder="Confirm new password" required>
                            {!! $errors->first('password_confirmation', '<div class=invalid-feedback>:message</div>') !!}
                        </div>
                        
                    </div>

                    <div class="form-group last">
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-success btn-sm">Confirm</button>
                            <button type="reset" class="btn btn-default btn-sm">Reset</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

@endsection