@extends('layouts.tol')

<style>
	.login-panel { 
		background: url(/images/general/login.jpg) no-repeat center center fixed; 
		-webkit-background-size: cover;
		-moz-background-size: cover;
		-o-background-size: cover;
		background-size: cover;
		background-position: 100px 100px;
	}

	.panel-default {
		opacity: 0.9;
		margin-top:30px;
	}
	.form-group.last { margin-bottom:0px; }
</style>

@section('content')
	<div class="row login-panel">
		<div class="col-md-4 col-md-offset-4"></div>
		<div class="col-md-4 col-md-offset-4"></div>

        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading h3">
					<div class="panel-title">Login</div>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}" novalidate>
					
					{{ csrf_field() }}

                    <div class="form-group">
                    	<label for="username" class="col-sm-9 control-label">
                            @lang('tol.txt_label_username')</label>
                        <div class="col-sm-9">
                            <input type="email" 
                            	   class="form-control {{ $errors->has('username') ? 'is-invalid' : '' }}" 
                            	   id="username"
                            	   value = "{{ old('username') }}"
                            	   name="username"
                            	   placeholder="@lang('tol.txt_label_username')" required>
                            {!! $errors->first('username', '<div class=invalid-feedback>:message</div>') !!}
                        </div>
                        
                    </div>
                    <div class="form-group">
                    	
                        <label for="inputPassword3" class="col-sm-9 control-label">
                            @lang('tol.txt_label_password')</label>
                        <div class="col-sm-9">
                            <input type="password" 
                            	   class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" 
                            	   id="inputPassword3"
                            	   value = "{{ old('password') }}"
                            	   name="password"
                            	   placeholder="@lang('tol.txt_label_password')" required>
                            {!! $errors->first('password', '<div class=invalid-feedback>:message</div>') !!}
                        </div>
                    	
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox"/>
                                    Remember me
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group last">
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-success btn-sm">
                                @lang('tol.txt_button_login')</button>
                                 <button type="reset" class="btn btn-default btn-sm">
                                Reset</button>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="panel-footer">
                   @lang('tol.txt_index_questpassforgotten') <a href="{{ route('password.request') }}">Get a new one</a>
                </div>
            </div>
        </div>

    </div>
@endsection