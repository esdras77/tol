<style>
	.card{border-left: 1px solid lightgray !important;}
</style>

@include('components.files.route')

<div class="mt-3" style="height: 550px; overflow-y: auto; overflow-x: none;" id="iconContainer">
	{{-- @include('components.files.alternative-menu')
 --}}
	@foreach ($icon as $file)

		@if ($loop->iteration%4 == 1)
			<div class="card-group">
		@endif

		@if (empty($file->extension))
			@if (empty($file->url))
				@php
					$iconImage = '<i data-feather="folder" width="48" height="48" style="color: gray;"></i>';
					$reason = 'folder';
				@endphp
			@else
				@php
					$iconImage = '<img src="/images/file_manager/file_extension_unknown.png">';
					$reason = 'file';
				@endphp
			@endif
		@else
			@php
				$extension = (!is_numeric($file->extension)) ? $file->extension : "zip";
			    $iconImage = '<img src="/images/file_manager/file_extension_'.$extension.'.png">';
			    $reason = 'file';
			@endphp
		@endif
		
		{{-- print the tile --}}
		<div class="card text-center w-25 float-left m-1 icon {{ $reason }}" 
	  		 data-idarchivo="{{ $file->idArchivo }}" 
	  		 onmouseover="this.style.cursor='pointer';">

		  <div class="card-body noselect">
		    {!! $iconImage !!}
		  </div>

		  <div class="card-footer" title="{{ $file->nombre }}" style="background-color: #e9ecef !important;">
		    <strong class="align-middle noselect">{{ str_limit($file->nombre, 25, '...') }}</strong>
		    
		    <div class="float-right d-inline-block">
				<button class="btn-btn-outline-primary" id="popover-button-{{ $file->idArchivo }}"
						data-content_target="popover-content-{{ $file->idArchivo }}"
						data-container="body" 
						data-toggle="popover" 
						data-placement="top">
					<i data-feather="more-vertical" width="12" height="12"></i>
				</button>
				<div id="popover-content-{{ $file->idArchivo }}" style="display: none;">
					<div class="btn-group btn-group-sm" role="group" aria-label="actions">
						
						@if ($reason == 'folder')
							<button type="button" class="btn btn-outline-primary" title="Edit"
									onclick="javascript: perform('edit', {{ $file->idArchivo }})">
								<i data-feather="edit" width="16" height="16"></i>
							</button>
						@else
							<button type="button" class="btn btn-outline-primary" title="Download"
									onclick="javascript: perform('download', {{ $file->idArchivo }})">
								<i data-feather="download" width="16" height="16"></i>
							</button>
						@endif

						<button type="button" class="btn btn-outline-primary" title="Permission"
								onclick="javascript: perform('permission', {{ $file->idArchivo }})">
							<i data-feather="lock" width="16" height="16"></i>
						</button>
						<button type="button" class="btn btn-outline-primary" title="Info"
								onclick="javascript: perform('info', {{ $file->idArchivo }})">
							<i data-feather="info" width="16" height="16"></i>
						</button>

					</div>
					<div class="btn-group btn-group-sm" role="group" aria-label="actions">

						<button type="button" class="btn btn-outline-success" title="Copy"
								onclick="javascript: perform('copy', {{ $file->idArchivo }})">
							<i data-feather="copy" width="16" height="16"></i>
						</button>
						<button type="button" class="btn btn-outline-success" title="Cut"
								onclick="javascript: perform('cut', {{ $file->idArchivo }})">
							<i data-feather="scissors" width="16" height="16"></i>
						</button>

						@if ($reason == 'folder')
							<button type="button" class="btn btn-outline-success" title="Paste"
									onclick="javascript: perform('paste', {{ $file->idArchivo }})">
								<i data-feather="clipboard" width="16" height="16"></i>
							</button>
						@endif

					</div>
					<div class="btn-group btn-group-sm" role="group" aria-label="actions">
						
			      		<button type="button" class="btn btn-outline-danger" title="Delete" 
				  		  		onclick="javascript: perform('delete', {{ $file->idArchivo }})">
				  			<i data-feather="trash" width="16" height="16"></i>

				  		</button>

					</div>
					<form style="display: inline;" id="deleteForm{{ $file->idArchivo }}" 
						  method="POST" 
						  action={{ route('utils.files.delete', $file->idArchivo) }}>
							{!! csrf_field() !!}
				      		{!! method_field('delete') !!}
				    </form>
				</div>
		    </div>

		  </div>

		</div>

		{{-- Alternative menu --}}
		@include('components.files.alternative-menu')

		@if ($loop->iteration%4 == 0)
			</div>
		@endif

	@endforeach
</div>