<div class="modal fade" 
     id="alternative{{ !empty($file->idArchivo) ? $file->idArchivo : (!empty($baseFile->idArchivo) ? $baseFile->idArchivo : 0) }}" 
     tabindex="-1" role="dialog" aria-labelledby="alternative" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">
          {!! (!empty($iconImage)) ? $iconImage : '<i data-feather="folder" width="48" height="48"></i>' !!} 
          <span class="align-middle">{{ empty($file->idArchivo) ? 'New Folder' : $file->nombre }}</span>
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          {{-- lo uso para hacer generica la funcion --}}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="submit">Save changes</button>
      </div>
    </div>
  </div>
</div>