<style>
<!--
		
	#filedrag {/*display: none;*/ font-weight: bold; text-align: center; padding: 1em 0; margin: 1em 0; color: #555; border: 2px dashed #555; border-radius: 7px; cursor: default;}
		#filedrag.hover {color: #009900; border-color: #009900; border-style: solid; box-shadow: inset 0 3px 4px #888;}
	
	#messages {padding: 0 10px; margin: 1em 0; border: 1px solid #999;}
	
	#progress {overflow: auto; height: 150px;}
		#progress p {display: block; width: 80%; padding: 2px 5px; margin: 2px 0; border: 1px inset #446; border-radius: 5px; background: #eee url("/images/progress.png") 100% 0 repeat-y; margin: 5px auto;}
			#progress p.success {background: #0c0 none 0 0 no-repeat;}
				#progress p.failed {background: #c00 none 0 0 no-repeat;}
-->
</style>

<form method="POST" action="{{ route('utils.files.upload') }}" id="filesUpload" enctype="multipart/form-data">

	{!! csrf_field() !!}

	@component('components.form.hidden', [
        'name' => 'idArchivo',
        'type' => 'hidden',
        'object' => (!empty($baseFile)) ? $baseFile : 0,
    ])@endcomponent

    @component('components.form.hidden', [
        'name' => 'MAX_FILE_SIZE',
        'type' => 'hidden',
        'value'=> '100000000000'
    ])@endcomponent

    @component('components.form.input', [
		'type' => 'file',
	    'name' => 'uploadify',
	    'label' => 'Pulse para seleccionar un archivo',
	    'atributes' => 'required'
	])@endcomponent

</form>

<div id="filedrag">@lang("tol.txt_message_filesadd")</div>

<div id="messages" style="display: none;">
	<p>Status Messages</p>
</div>

<div id="progress"></div>

<script type="text/javascript">
<!--
	/*
	filedrag.js - HTML5 File Drag & Drop demonstration
	Featured on SitePoint.com
	Developed by Craig Buckler (@craigbuckler) of OptimalWorks.net
	*/
	(function() {
	
		// getElementById
		function $id(id) {
			return document.getElementById(id);
		}
	
	
		// output information
		function Output(msg) {
			var m = $id("messages");
			m.innerHTML = msg + m.innerHTML;
		}
	
	
		// file drag hover
		function FileDragHover(e) {
			e.stopPropagation();
			e.preventDefault();
			e.target.className = (e.type == "dragover" ? "hover" : "");
		}
	
	
		// file selection
		function FileSelectHandler(e) {
	
			// cancel event and hover styling
			FileDragHover(e);
	
			// fetch FileList object
			var files = e.target.files || e.dataTransfer.files;
	
			// process all File objects
			for (var i = 0, f; f = files[i]; i++) {
				ParseFile(f);
				UploadFile(f);
			}

			setTimeout(function(){
				$('#uploadFiles').toggle('slow');
			}, 3000);

			$.ajax({
		        url: window.location.href,
		        headers: {
		            "Pragma": "no-cache",
		            "Expires": -1,
		            "Cache-Control": "no-cache"
		        }
		    }).done(function () {
		        window.location.reload(true);
		    });
		}
	
	
		// output file information
		function ParseFile(file) {
			
			Output(
				"<p>File information: <strong>" + file.name +
				"</strong> type: <strong>" + file.type +
				"</strong> size: <strong>" + file.size +
				"</strong> bytes</p>"
			);
	
		}

		// upload JPEG files
		function UploadFile(file) {

			var xhr = new XMLHttpRequest();
			if (xhr.upload && file.size <= $id("MAX_FILE_SIZE").value) {
			
				// create progress bar
				var o = $id("progress");
				var progress = o.appendChild(document.createElement("p"));
				progress.appendChild(document.createTextNode("uploading " + file.name + " | " + file.size + " Bytes"));
		
				// progress bar
				xhr.upload.addEventListener("progress", function(e) {
					var pc = parseInt(100 - (e.loaded / e.total * 100));
					progress.style.backgroundPosition = pc + "% 0";
				}, false);
		
				// file received/failed
				xhr.onreadystatechange = function(e) {
					if (xhr.readyState == 4) {
						progress.className = (xhr.status == 200 ? "success" : "failure");
					}
				};
		
				// start upload
				var route = $id("filesUpload").action + '?idPadre={{ (!empty($baseFile->idArchivo)) ? $baseFile->idArchivo : 0 }}';
				
				xhr.open("POST", route, true);
				xhr.setRequestHeader("X-FILENAME", file.name);
				xhr.setRequestHeader("X-CSRF-TOKEN", '{{ csrf_token() }}');
				xhr.send(file);

			} else {
				alert("Archivo demasiado grande para hacer upload");
			}

		}
	
	
		// initialize
		function Init() {
	
			var fileselect = $id("uploadify"),
				filedrag = $id("filedrag"),
				submitbutton = $id("submitbutton");
	
			// file select
			fileselect.addEventListener("change", FileSelectHandler, false);
	
			// is XHR2 available?
			var xhr = new XMLHttpRequest();
			if (xhr.upload) {
	
				// file drop
				filedrag.addEventListener("dragover", FileDragHover, false);
				filedrag.addEventListener("dragleave", FileDragHover, false);
				filedrag.addEventListener("drop", FileSelectHandler, false);
				filedrag.style.display = "block";
	
				// remove submit button
				//submitbutton.style.display = "none";
			}
	
		}
	
		// call initialization file
		if (window.File && window.FileList && window.FileReader) {
			Init();
		}
	
	
	})();
//-->
</script>
