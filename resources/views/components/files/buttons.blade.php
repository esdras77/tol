<div class="container clearfix">
	
	@if ((auth()->user()->perfil->isAdmin()) || (!empty($baseFile) && $baseFile->getPermissionFor(auth()->user()->idUsuario)->download))
		<button class="btn btn-outline-primary floating-button" 
				onclick="javascript: $('#uploadFiles').toggle('slow'); ">
			Subir Archivo
		</button>
		<button class="btn btn-outline-primary floating-button" 
				onclick="javascript: perform('create', {{ !empty($baseFile->idArchivo) ? $baseFile->idArchivo : 0 }})">
			Nueva Carpeta
		</button>
	@endif

  	@if (1==1)
		<button class="btn btn-outline-primary floating-button" title="Paste" 
			@if (!empty($baseFile->idArchivo))
				onclick="javascript: perform('paste', {{ $baseFile->idArchivo }});"
			@endif
			{{ $disabled }}	>
			<i data-feather="clipboard" width="20" height="20"></i>
		</button>
  	@endif

</div>

<div class="container clearfix" id="uploadFiles" style="display: none;">

	@include('components.files.upload')

</div>
