<style>

  .breadcrumb-item + .breadcrumb-item::before         {
      content: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI4IiBoZWlnaHQ9IjgiPjxwYXRoIGQ9Ik0yLjUgMEwxIDEuNSAzLjUgNCAxIDYuNSAyLjUgOGw0LTQtNC00eiIgZmlsbD0iY3VycmVudENvbG9yIi8+PC9zdmc+) !important;
  }

  .floating-button{
    float: right;
    margin-left: 10px;
  }

</style>

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
		<li class="breadcrumb-item" aria-current="page"> 
    		<a href="{{ route('utils.files') }}"><i data-feather="inbox"></i></a>
    	</li>
    @if (!empty($baseFile))
    	@foreach ($baseFile->getRoute() as $step)

        @if (is_object($step))
  	    	<li class="breadcrumb-item" aria-current="page"> 
  	    		<a href="{{ route('utils.files', ['base' => $step->idArchivo]) }}">{{ $step->nombre }}</a>
  	    	</li>
        @endif

	  	@endforeach
    @endif
  </ol>
</nav>

@include('components.files.buttons')