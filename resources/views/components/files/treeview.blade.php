<div class="bg-primary text-white rounded" style="height: 3.25em;">
	<div class="h5 p-2">Directorio</div>
</div>
<div class="px-2 mt-3" style="height: 600px; overflow: auto; overflow-x: none;" id="treeContainer">
	@foreach ($tree as $file)
		@if (empty($file->extension))
			@include('components.files.branch')
		@endif
	@endforeach
</div>