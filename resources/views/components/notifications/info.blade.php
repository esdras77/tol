 @if (session()->has('info'))
    <div class="alert alert-success text-center alert-dismissible fade show" role="alert">
        <p class="mt-1 mb-1 h4">{{ session('info') }}</p>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif