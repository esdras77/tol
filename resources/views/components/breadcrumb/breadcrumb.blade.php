<div class="btn-group btn-breadcrumb breadcrumb-default">
	
	@if (url()->current() != route('home'))
		<button class="btn btn-outline-primary" 
				onclick="javascript: location.href='/home'"
				title="Home"><i data-feather="home"></i></button>
	@else
		<button class="btn btn-outline-secondary" 
				title="Home">Home</button>
	@endif

	@if (!empty($breadcrumb))

		@foreach ($breadcrumb as $tile)
		
	    	@if ($loop->count > $loop->iteration)
				<button class="btn btn-outline-primary"
						title=@lang('tol.'.$tile->descripcion)
						onclick="javascript: location.href='{{ route($tile->url) }}'" title=@lang('tol.'.$tile->descripcion)>
						@lang('tol.'.$tile->descripcion)
				</button>
			@else
				<button class="btn btn-secondary visible-lg-block visible-md-block"
						title=@lang('tol.'.$tile->descripcion)
						onclick="javascript: location.href='{{ route($tile->url) }}'" title=@lang('tol.'.$tile->descripcion)>
						@lang('tol.'.$tile->descripcion)
				</button>
			@endif	

		@endforeach
	@elseif (url()->current() != route('home'))
		<button class="btn btn-outline-primary"
				onclick="javascript: location.href='{{ url()->previous() }}'" title=@lang('tol.txt_button_back')>
				@lang('tol.txt_button_back')
		</button>
	@endif

</div>