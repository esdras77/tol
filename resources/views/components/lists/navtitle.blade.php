	
	<div class="collapse" id="navbarToggleExternalContent">

	    <div class="bg-light p-4">
	    	
		      <h4>{{ $formTitle }}</h4>
		      <span class="text-muted">
		      	<form method="get" action="{{ route($form['formAction']) }}">
					{!! csrf_field() !!}
					
					@foreach ($form['formComponents'] as $component)

						@component('components.form.'.$component[0], [
						    'name' => $component[1],
						    'label' => $component[2],
						    'options' => (isset($component[3])) ? $component[3] : ''
						])@endcomponent

					@endforeach

					@component('components.form.buttons', [
					    'buttons' => [
					    	[
					    		'type' => 'submit',
					    	 	'class' => 'success',
					    	 	'text' => '<i data-feather="search"></i> Buscar'
					    	],
					    	[
					    		'type' => 'reset',
					    	 	'class' => '',
					    	 	'text' => 'Reset'
					    	]

					    ]
					])@endcomponent

		      	</form>		
		      </span>
	      	
	    </div>
	   
  	</div>
		
	<div class="float-right" style="margin-top: -10px;">
		<nav class="navbar navbar-light bg-light">
			
			<button class="navbar-toggler float-right" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
			  <i data-feather="search"></i> Buscar
			</button>

		</nav>
	</div>