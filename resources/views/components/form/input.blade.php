<div class="form-group{{ $errors->has($name) ? ' has-error' : '' }}">
    @if (isset($label))
        <label for="{{ $name }}" class="col-md-4 control-label">{{ $label }}</label>
    @endif

    <div class="col-md-9">
        <input id="{{ $name }}" 
            type="{{ isset($type) ? $type : 'text' }}"
            class="form-control{{(isset($type) && $type == 'file') ? '-file' : ''}} {{ isset($class) ? $class : '' }} {{ $errors->has($name) ? 'is-invalid' : '' }}"
            name="{{ $name }}"
            placeholder="{{ isset($placeholder) ? $placeholder : '' }}"
            value="{{
                old($name) ?: ((isset($object) && !empty($object)) ? $object->{$name} : ((isset($value)) ? $value : ''))
            }}"
            {{ isset($attributes) ? $attributes : '' }}
            aria-describedby="fileHelp">
        
        @if (isset($hint))
            <small id="fileHelp" class="form-text text-muted">{{ $hint }}</small>
  
        @endif
        @if ($errors->has($name))
            <span class="help-block invalid-feedback">
                <strong>{{ $errors->first($name) }}</strong>
            </span>
        @endif
    </div>
</div>