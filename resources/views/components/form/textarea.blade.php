<div class="form-group{{ $errors->has($name) ? ' has-error' : '' }}">
    @if (isset($label))
        <label for="{{ $name }}" class="col-md-4 control-label">{{ $label }}</label>
    @endif

    <div class="col-md-9">
        <textarea id="{{ $name }}" 
            type="{{ isset($type) ? $type : 'text' }}"
            class="form-control {{ isset($class) ? $class : '' }} {{ $errors->has($name) ? 'is-invalid' : '' }}"
            name="{{ $name }}"
            placeholder="{{ isset($placeholder) ? $placeholder : '' }}"
            {{ isset($attributes) ? $attributes : '' }}>{!! old($name) ?: ((isset($object) && !empty($object)) ? trim($object->{$name}) : '') !!}</textarea>

        @if ($errors->has($name))
            <span class="help-block invalid-feedback">
                <strong>{{ $errors->first($name) }}</strong>
            </span>
        @endif
    </div>
</div>