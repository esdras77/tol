<div class="form-group{{ $errors->has($name) ? ' has-error' : '' }}">
    
    <div class="input-group mb-3">
        <input id="{{ $name }}" 
            type="{{ isset($type) ? $type : 'text' }}"
            class="form-control{{(isset($type) && $type == 'file') ? '-file' : ''}} {{ isset($class) ? $class : '' }} {{ $errors->has($name) ? 'is-invalid' : '' }}"
            name="{{ $name }}"
            placeholder="{{ $label }}"
            value="{{
                old($name) ?: ((isset($object) && !empty($object)) ? $object->{$name} : ((isset($value)) ? $value : ''))
            }}"
            {{ isset($attributes) ? $attributes : '' }}
            aria-describedby="fileHelp"
            style="height: 2.7em;"

            data-container="body" 
            data-toggle="popover" 
            data-placement="bottom" 
            data-content="Puedes utilizar * para reemplazar caracteres en la búsqueda">
        
        @if (isset($hint))
            <small id="fileHelp" class="form-text text-muted">{{ $hint }}</small>
  
        @endif
        @if ($errors->has($name))
            <span class="help-block invalid-feedback">
                <strong>{{ $errors->first($name) }}</strong>
            </span>
        @endif
        <div class="input-group-append">
            <button class="btn btn-outline-secondary" type="button" id="button-addon2" onclick="javascript: submit();"><i data-feather="search"></i> Buscar</button>
        </div>
    </div>

</div>
