        <input id="{{ $name }}" 
            type="{{ isset($type) ? $type : 'text' }}"
            name="{{ $name }}"
            value="{{
                ((isset($object) && !empty($object)) ? $object->{$name} : ((isset($value)) ? $value : ''))
            }}"
            {{ isset($attributes) ? $attributes : '' }}>


