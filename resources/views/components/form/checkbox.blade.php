<div class="form-group{{ $errors->has($name) ? ' has-error' : '' }}">
   
    <div class="col-md-9 custom-control custom-checkbox ml-3">
        <input type="checkbox" class="custom-control-input" 
               id="{{ $name }}"
               {{ old($name) ?: ((isset($object) && !empty($object)) ? 'checked' : '') }}
               name="{{ $name }}"
               {{ isset($attributes) ? $attributes : '' }}>
        
        @if (isset($label))
            <label class="custom-control-label" for="{{ $name }}">{{ $label }}</label>
        @endif

    @if ($errors->has($name))
        <span class="help-block invalid-feedback">
            <strong>{{ $errors->first($name) }}</strong>
        </span>
    @endif

    </div>
</div>