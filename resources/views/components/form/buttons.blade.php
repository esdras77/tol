<div class="form-group last">
    <div class="col-md-9">
    	@foreach($buttons as $button)
	        <button 
	        	type="{{ $button['type'] }}" 
	        	class="btn btn-{{ $button['class'] }} btn-sm"
				@if(isset($button['action']))
					@if ($button['action'] == 'Back')
						onclick="window.history.back()"
					@else
						onclick="javascript: window.location.href='{{ $button["action"] }}'"
					@endif
				@endif
	        	>
	            @lang($button['text'])
	        </button>
    	@endforeach
    </div>
</div>