<div class="form-group{{ $errors->has($name) ? ' has-error' : '' }}">
    @if (isset($label))
        <label for="{{ $name }}" class="col-md-4 control-label">{{ $label }}</label>
    @endif

    <div class="col-md-9">
        <select id="{{ $name }}"
                name="{{ $name }}"
                class="form-control {{ $errors->has($name) ? 'is-invalid' : '' }}"
                {{ (isset($multiple) && $multiple) ? "multiple" : ""}}
                {{ isset($attributes) ? $attributes : '' }}>
                
                @if (isset($multiple) && !$multiple)
                    <option value="">Seleccione...</option>
                @endif
            
            @if (!empty($options))
                <option value="">Seleccione...</option>
                @foreach($options as $id => $value)
                    <option 
                        value="{{ $id }}"
                        {{-- old value --}}
                        {{ old($name) == $id ? 'selected' : '' }}
                        {{-- edition --}}
                        {{ (isset($object) && $object->{$comparison} == $id) ? 'selected' : '' }}
                    >{{ $value }}</option>
                @endforeach
            @endif
        </select>

        @if ($errors->has($name))
            <span class="help-block invalid-feedback">
                <strong>{{ $errors->first($name) }}</strong>
            </span>
        @endif
    </div>
</div>