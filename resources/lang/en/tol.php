<?php

use App\Texto;

$textModel = new Texto;
$arrTraducciones = array();

$textos = $textModel->all();

foreach ($textos as $key => $value) {
	$translation = $value->getTranslation();
	
	if (!is_null($translation)) {
			$arrTraducciones[$translation->idTexto] = $translation->traduccion;
	}
}


return $arrTraducciones;