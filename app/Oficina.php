<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Oficina extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'oficina';//nombre de tu tabla

    public $incrementing = false;

    protected $fillable = ['idOficina', 'idDireccion', 'idEmpresa', 'idPais', 'nombre', 'telefono1', 'telefono2', 'fechaModificacion', 'fechaBaja', 'principal'];

    public function getKeyName(){
        return 'idOficina';
    }

    public function enterprise(){
    	return $this->belongsTo(Empresa::class, 'idEmpresa');
    }

    public function address(){
        return $this->belongsTo(Direccion::class, 'idDireccion');
    }

    public function users(){
        return $this->belongsToMany(User::class, 'usuario_en_oficina', 'idUsuario', 'idOficina');
    }

    public function inspections(){
        return $this->hasMany(Inspeccion::class, 'idOficina');
    }

    public function operators(){
        return $this->hasMany(Operador::class, 'idOficina');   
    }
}
