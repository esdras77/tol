<?php

namespace App;

use Auth;

use Illuminate\Database\Eloquent\Model;

class Perfil extends Model{

	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'perfil';//nombre de tu tabla

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'descripcion', 'administrador'
    ];

	public function user(){
		return $this->hasOne(User::class, 'idPerfil');
	}

	public function menu(){
		return $this->hasOne(Menu::class, 'idMenu', 'idMenu');
	}

	public function getKeyName(){
        return 'idPerfil';
    }

    public function listProfiles(){
    	$profilesBunch = collect([]);
    	
    	$currentProfile = $this->find(auth()->user()->idPerfil);

    	$profilesList = collect([$currentProfile]);
    	$profilesList = $profilesList->merge($this->_getChildrenProfiles($currentProfile, $profilesBunch));
    	
    	return $profilesList;
    }

    public function delete(){
        return "To be delete";
    }

    /* Private section */

    private function _getChildrenProfiles(Perfil $currentProfile, $profilesBunch){
    	$children = $this->where('mainPerfil', $currentProfile->idPerfil)->get();

    	if (empty($children)) {
    		return $profilesBunch;
    	} else {
    		foreach ($children as $key => $profile) {
    			$profilesBunch->push($profile);
    			$this->_getChildrenProfiles($profile, $profilesBunch);
    		}
    		
    		return $profilesBunch;
    	}
    }

    public function isAdmin(){
        return $this->idPerfil == 1;
    }


}