<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Direccion extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'direccion';//nombre de tu tabla

    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idDireccion', 'idPais', 'direccion', 'codigoPostal', 'provincia', 'ciudad', 'localidad'
    ];

	public function empresa(){
		return $this->hasOne(Empresa::class, 'idDireccion');
	}

    public function office(){
        return $this->hasOne(Oficina::class, 'idDireccion');
    }

	public function getKeyName(){
        return 'idDireccion';
    }
}
