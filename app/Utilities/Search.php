<?php

namespace App\Utilities;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

class Search
{
	private static $itemsPerPage = 15;

    public static function apply(Request $filters, $object, $nullSearchAllowed = false)
    {
        $query = 
            static::applyDecoratorsFromRequest(
                $filters, $object->newQuery(), $nullSearchAllowed
            );

        return static::getResults($query);
    }
    
    private static function applyDecoratorsFromRequest(Request $request, Builder $query, $nullSearchAllowed)
    {
        foreach ($request->all() as $filterName => $value) {

            $decorator = static::createFilterDecorator($filterName);

            if (static::isValidDecorator($decorator)) {
            	if (!is_null($value) || $nullSearchAllowed) {
            		$query = $decorator::apply($query, $value);	
	            }
            }

        }
        return $query;
    }
    
    private static function createFilterDecorator($name)
    {
        return __NAMESPACE__ . '\\Filters\\' . 
            str_replace(' ', '', 
                ucwords(str_replace('_', ' ', $name)));
    }
    
    private static function isValidDecorator($decorator)
    {
        return class_exists($decorator);
    }

    private static function getResults(Builder $query)
    {
        return $query->paginate(self::$itemsPerPage);
    }

}