<?php

namespace App\Utilities\Filters;

use Illuminate\Database\Eloquent\Builder;

class Iso_Code implements Filter
{

    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        $value = str_replace("*", "%", $value);

        return $builder->where('iso_code', 'like', $value);
    }
}