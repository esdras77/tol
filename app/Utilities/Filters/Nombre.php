<?php

namespace App\Utilities\Filters;

use Illuminate\Database\Eloquent\Builder;

class Nombre implements Filter
{

    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        $value = str_replace("*", "%", $value);

        return $builder->where('nombre', 'like', $value);
    }
}