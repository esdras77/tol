<?php

namespace App\Utilities\Filters;

use Illuminate\Database\Eloquent\Builder;

class Traduccion implements Filter
{

    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        $value = str_replace("*", "%", $value);
        return $builder->join('traduccion', 'traduccion.idTexto', '=', 'texto.idTexto')->where('traduccion', 'like', $value);
    }
}