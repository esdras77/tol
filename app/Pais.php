<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pais';//nombre de tu tabla

    protected $fillable = ['nombre', 'iso_code', 'caracteristicaTel', 'currency'];

    public function getKeyName(){
        return 'idPais';
    }

	public function user(){
		return $this->hasOne(User::class, 'idPais');
	}

}
