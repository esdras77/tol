<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Database\Eloquent\Model;

use App\Notifications\ResetPasswordNotification;
    

class User extends Authenticatable
{
    use Notifiable;

     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'usuario';//nombre de tu tabla

    public function getKeyName(){
        return 'idUsuario';
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
            'username', 'email', 'password', 'nombre', 'apellido', 'telefono', 'celular', 'firma', 
            'idDepartamento', 'idPais', 'idPerfil', 'aceptaCondiciones', 'inicioActividades'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'inicioActividades', 'ultimoLogin'
    ];

    public function perfil(){
        return $this->belongsTo(Perfil::class, 'idPerfil');
    }

    public function pais(){
        return $this->belongsTo(Pais::class, 'idPais');
    }

    public function departamento(){
        return $this->belongsTo(Departamento::class, 'idDepartamento');
    }

    public function compra(){
        return $this->hasMany(Compra::class, 'idCompra');
    }

    public function groups(){
        return $this->belongsToMany(Grupo::class, 'usuariogrupo', 'idUsuario', 'idGrupo');
    }

    public function coordinadorGrupo(){
        return $this->belongsToMany(Grupo::class, 'coordinadorgrupo', 'idUsuario', 'idGrupo');        
    }

    public function news(){
        return $this->hasOne(Noticia::class, 'idUsuario');
    }

    public function permisos(){
        return $this->hasMany(Permiso::class, 'idUsuario');
    }

    public function archivos(){
        return $this->belongsToMany(Archivo::class, 'permiso', 'idUsuario', 'idArchivo')->wherePivot('read', 1)->groupBy('idArchivo');
    }

    public function oficinas(){
        return $this->belongsToMany(Oficina::class, 'usuario_en_oficina', 'idUsuario', 'idOficina');
    }

    public function proyectosispm(){
        return $this->hasMany(IspmUsuariosProyectos::class, 'idUsuario');
    }

    
     /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function updateLogin(){
        $this->ultimoLogin = date("Y-m-d H:i:s");

        return $this->save();
    }

    public function getMenu(){
        return $this->perfil->menu->menuItemsRender;
    }

    public function getFilesFrom($idParent){
        $arrDir = $arrFile = array();
        $auxFiles = $this->archivos()->where('idPadre', $idParent)->orderBy('nombre')->get();

        //separamos y ordenamos carpetas y archivos
        if (!empty($auxFiles)) {
            foreach ($auxFiles as $key => $file) {
                if ($file->isDir()) {
                    $arrDir[]= $file;
                } else {
                    $arrFile[]= $file;
                }
            }
        }

        $files = array_merge($arrDir, $arrFile);

        return collect($files);
    }

    public function getFilesBy($field, $compare, $needle){
        $arrDir = $arrFile = array();
        $auxFiles = $this->archivos()->where($field, $compare, $needle)->orderBy('nombre')->get();

        //separamos y ordenamos carpetas y archivos
        if (!empty($auxFiles)) {
            foreach ($auxFiles as $key => $file) {
                if ($file->isDir()) {
                    $arrDir[]= $file;
                } else {
                    $arrFile[]= $file;
                }
            }
        }

        $files = array_merge($arrDir, $arrFile);

        return collect($files);
    }

    public function changePermissionFor($file, $action, $value){

        $permission = $this->permisos()->where('idArchivo', $file)->first();
        $permission->changeTo($action, $value);

    }

}
