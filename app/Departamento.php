<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'departamento';//nombre de tu tabla

	public function user(){
		return $this->hasOne(User::class, 'idDepartamento');
	}

	public function getKeyName(){
        return 'idDepartamento';
    }
}
