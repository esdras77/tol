<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model{

	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'menu';//nombre de tu tabla

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
    ];

	public function perfil(){
		return $this->hasOne(Perfil::class, 'idMenu');
	}

	/**
	 * Devuelve los items de menu activos para un perfil/usuario en el contexto actual del sistema
	 *
	 *
	 */
	public function menuItemsRender(){
		return $this->belongsToMany(MenuItems::class, 'menu_itemdemenu', 'idMenu', 'idItemMenu')
					->where('activo', 1)
					->where('padre', session('padre_actual'))
					->orderBy('posicion');
	}

	/**
	 * recibe el id del padre y muestra los hijos
	 *
	 * @var int $id
	 */
	public function menuItems(){
		return $this->belongsToMany(MenuItems::class, 'menu_itemdemenu', 'idMenu', 'idItemMenu')
					->where('activo', 1)
					->orderBy('posicion');
	}

	public function getKeyName(){
        return 'idMenu';
    }

}