<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Logdeactualizacion extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'logsdeactualizacion';//nombre de tu tabla

    protected $fillable = ['areaDeSistema', 'ultimaFechaActualizacion'];

    public function getKeyName(){
        return 'idLogsDeActualizacion';
    }
}
