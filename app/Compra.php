<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Compra extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'compra';//nombre de tu tabla

    public function getKeyName(){
        return 'idCompra';
    }

    public function user(){
    	return $this->belongsTo(User::class, 'idCompra');
    }


}
