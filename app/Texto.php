<?php

namespace App;

use Session;

use Illuminate\Database\Eloquent\Model;

class Texto extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'texto';//nombre de tu tabla

    protected $primaryKey = 'idTexto';

    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idTexto', 'idiomaOriginal'
    ];

    public function traduccion(){
    	return $this->hasMany(Traduccion::class, 'idTexto');
    }

    public function getKeyName(){
        return 'idTexto';
    }
    
    public function getTranslation($idIdioma = null){
        $language = (!empty($idIdioma)) ? $idIdioma : Idioma::where('idIdioma', Session::get('currentLanguage'))->first()->idIdioma;
        return $this->traduccion()->where('idIdioma', $language)->first();
    }
}
