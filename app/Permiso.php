<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permiso extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'permiso';//nombre de tu tabla

    public function getKeyName(){
        return 'idPermiso';
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idArchivo', 'idUsuario', 'admin', 'read', 'download', 'delete' 
    ];

	public function archivo(){
		return $this->belongsTo(Archivo::class, 'idArchivo');
	}
	
    public function isOwner(){
        return $this->admin;
    }

    public function couldRead(){
        return $this->read;
    }

    public function couldDownload(){
        return $this->download;
    }    

    public function couldRemove(){
        return $this->delete;
    }

    public function invert($property){
        $this->{$property} = !$this->{$property};
        $this->save();
    }

    public function changeTo($property, $value){
        $this->{$property} = $value;
        $this->save();
    }
}
