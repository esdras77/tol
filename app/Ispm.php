<?php

namespace App;

use SoapClient;
use SoapFault;
use Illuminate\Database\Eloquent\Model;

class Ispm extends Model
{
    
	private $_cliente;
	private $_wsdl = 'http://10.130.14.62/webservices/testV2.php?wsdl';

	public function __construct(){
		$params = array('encoding' => 'UTF-8', 'verifypeer' => false, 'Verifyhost' => false, 'soap_version' => SOAP_1_1);
		$this->_cliente = new SoapClient($this->_wsdl, $params);
	}


	public function getMethods(){
		return $this->_cliente->__getFunctions();
	}

	public function runRequest($functionName, $parameters){
		try {
			return $this->_cliente->__soapCall($functionName, $parameters);
        }
        catch (SoapFault $ex) {
            return view('errors.soapfail', compact('ex'));
        }
	}

	public function getCompleteInspectionsList($idCustomer, $dateFrom, $dateTo = null, $idDepartment = 1){
		$dateFrom = \Carbon\Carbon::parse($dateFrom);
		$dateTo = (!is_null($dateTo)) ? \Carbon\Carbon::parse($dateTo) : \Carbon\Carbon::now();

		$parameters = array("idDepartamento" => $idDepartment, "fechaDesde" => $dateFrom, "fechaHasta" => $dateTo, "idCliente" => $idCustomer);
		return $this->runRequest('getListadoFull', $parameters);
	}

	public function getProjectsByCustomer($idCustomer, $idDepartment = 1){
		$dateFrom = date("Y-m-d", strtotime('-1 year'));
     	$dateTo = date("Y-m-d", strtotime('now'));

		$parameters = array("idDepartamento" => $idDepartment, "idCliente" => (int)$idCustomer, "fechaDesde" => $dateFrom, "fechaHasta" => $dateTo);
		return $this->runRequest('getProyectos', $parameters);	
	}

	/*
	 * Configuro SI como default ya que es su herramienta
	 */
	public function getCustomersToDepartment($idDepartment = 1){
		$parameters = array("idDepartamento" => $idDepartment);
    	return $this->runRequest('getClientePorDepartamento', $parameters);	
	}

}
