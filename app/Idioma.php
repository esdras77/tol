<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Idioma extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'idioma';//nombre de tu tabla

    protected $fillable = ['nombre', 'iso', 'activo'];

    public function getKeyName(){
        return 'idIdioma';
    }
}
