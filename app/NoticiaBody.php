<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NoticiaBody extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'noticias_cuerpo';//nombre de tu tabla

    protected $fillable = ['idNoticia', 'idioma', 'titulo', 'cuerpo'];

    public function getKeyName(){
        return 'idCuerpo';
    }

    public function news(){
    	return $this->belongsTo(Noticia::class, 'idCuerpo');
    }
}
