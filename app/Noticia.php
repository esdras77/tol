<?php

namespace App;

use App\Idioma;
use Illuminate\Database\Eloquent\Model;

class Noticia extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'noticias_cabecera';//nombre de tu tabla

    protected $fillable = ['idUsuario', 'fechaPublicacion', 'fechaVencimiento', 'principal', 'descripcion'];

    public function getKeyName(){
        return 'idNoticia';
    }

    public function body(){
    	return $this->hasMany(NoticiaBody::class, 'idNoticia');
    }

    public function responsible(){
        return $this->belongsTo(User::class, 'idUsuario');
    }

    public function getBody($idIdioma = null){
        $language = (!empty($idIdioma)) ? $idIdioma : Idioma::where('iso', \App::getLocale())->first()->idIdioma;

        return $this->body()->where('idIdioma', $language)->first();
    }
}
