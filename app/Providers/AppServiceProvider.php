<?php

namespace App\Providers;

use App\MenuItems;
use App\Breadcrumb;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //creacion de menues bajo demanda de usuario
        view()->composer('layouts.tilesmenu', function($view){
            $view->with('itemMenuList', \Auth::user()->getMenu());
        });

        //breadcrumb
        view()->composer('components.breadcrumb.breadcrumb', function($view){
            $breadcrumb = new Breadcrumb;
            $breadcrumb->setTile();

            $view->with('breadcrumb', $breadcrumb->getTiles());
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
