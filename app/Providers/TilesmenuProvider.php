<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class TilesmenuProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //$this->composeTilesMenu();
    }

    public function composeTilesMenu(){
        view()->composer('layouts.tilesmenu', 'App\Http\Composers\TilesmenuComposer');
    }
}
