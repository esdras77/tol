<?php

namespace App;

use Archivo;
use Illuminate\Database\Eloquent\Model;

class Tipoaviso extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tipoaviso';//nombre de tu tabla

    public function getKeyName(){
        return 'idTipoAviso';
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre'
    ];

    public function archivo(){
    	return $this->hasMany(Archivo::class, 'idTipoAviso');
    }

    public function getListToSelect($file = null){
        $selectionList = array();

        switch ($this->idTipoAviso) {
            case 1:
                $list = \App\User::all();
                foreach ($list as $key => $user) {
                    $selected = (!empty($file)) ? ($file->aviso == $user->idUsuario) : "";
                    $selectionList[]= array('id' => $user->idUsuario, 'nombre' => $user->nombre.', '.$user->apellido, 'selected' => $selected);
                }
                break;
            case 2:
                $user = \App\User::findOrFail(auth()->user()->idUsuario);
                $list = $user->groups;
                foreach ($list as $key => $group) {
                    $selected = (!empty($file)) ? ($file->aviso == $group->idGrupo) : "";
                    $selectionList[]= array('id' => $group->idGrupo, 'nombre' => $group->nombre, 'selected' => $selected);
                }
                break;
            
            default:
                # code...
                break;
        }

        return $selectionList;
        
    }
}
