<?php

namespace App\Http\Controllers;

use App\Perfil;
use App\Utilities\Search;

use Illuminate\Http\Request;

class ProfileController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profiles = Perfil::paginate(15);

        return view('administration.profile.list', compact('profiles'));
    }

    public function search(Request $request)
    {
        $profile = new Perfil;
        $profiles = Search::apply($request, $profile);

        return view('administration.profile.list', compact('profiles'));   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $profile = null;
        
        $profiles = Perfil::where('mainPerfil', '>=', auth()->user()->idPerfil)->get();

        return view('administration.profile.create', compact('profile', 'profiles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->merge(['administrador' => (null === request()->input('administrador'))]);

        $idProfile = Perfil::create($request->all())->idPerfil;

        if ($idProfile){
            $profile = Perfil::findOrFail($idProfile);

            $idMenu = \App\Menu::create(['nombre' => $profile->nombre." Menu"])->idMenu;
            
            $profile->idMenu = $idMenu;
            $profile->save();
        }

        return redirect()->route('administration.profiles');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profile = Perfil::findOrFail($id);

        return view('administration.profile.edit', compact('profile'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profile = Perfil::findOrFail($id);

        $profiles = Perfil::all();

        return view('administration.profile.edit', compact('profile', 'profiles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $profile = Perfil::findOrFail($id);

        request()->merge(['administrador' => (null === request()->input('administrador'))]);

        $profile->update($request->all());

        return redirect()->route('administration.profiles')->with('info', 'Perfil actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $profile = Perfil::findOrFail($id);

        return redirect()->route('administration.profiles')->with('info', 'Perfil eliminado');
    }
}
