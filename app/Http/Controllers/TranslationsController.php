<?php

namespace App\Http\Controllers;

use App\Idioma;
use App\Texto;
use App\Traduccion;
use App\Utilities\Search;

use Illuminate\Http\Request;

class TranslationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $texts = Texto::paginate(15);

        return view('administration.translations.list', compact('texts'));
    }

    public function search(Request $request){

        $text = new Texto;
        $texts = Search::apply($request, $text);

        if (!empty($texts)) {
            foreach ($texts as $key => $value) {
                unset($value->traduccion);
            }
        }

        return view('administration.translations.list', compact('texts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $text = new Texto;
        $translation = new Traduccion;

        $currentLanguage = Idioma::where('iso', \App::getLocale())->first()->idIdioma;

        $text->idiomaOriginal = $currentLanguage;
        $translation->idIdioma = $currentLanguage;

        return view('administration.translations.create', compact('text', 'translation'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $texto = Texto::create($request->all());

        $request->merge([
            'idIdioma' => $request->idiomaOriginal,
            'usuarioEditor' => auth()->user()->idUsuario
        ]);

        $texto->traduccion()->create($request->all());

        return redirect()->route('administration.translations');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Texto::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $text = Texto::findOrFail($id);
        $translation = $text->getTranslation($request->get('idIdioma'));

        if (empty($translation)) {
            $translation = new Traduccion;
            $translation->idIdioma = $request->get('idIdioma');
        }

        $languages = Idioma::where('activo', 1)->get();

        return view('administration.translations.edit', compact('text', 'translation', 'languages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $text = Texto::findOrFail($id);
        $translation = $text->getTranslation($request->get('idIdioma'));

        if (empty($translation)) {
            $translation = new Traduccion;
            $request->merge([
                'usuarioEditor' => auth()->user()->idUsuario
            ]);
            $translation->fill($request->all());
            $translation->save();
        } else {
            $translation->update($request->all());
        }

        return redirect()->route('administration.translations.edit', $request->get('idTexto'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
