<?php

namespace App\Http\Controllers;

use App\Grupo;

use Illuminate\Http\Request;

class UtilsController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->session()->put('padre_actual', 17);

		return view('utils.landing');
    }

    public function agenda(Request $request){

        $groups = auth()->user()->groups()->orderBy('nombre')->get();

        if ($request->has('idGrupo')) {
            $currentGroup = Grupo::find($request->get('idGrupo'));
        }

        return view('utils.agenda', compact('groups', 'currentGroup'));

    }
}
