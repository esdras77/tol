<?php

namespace App\Http\Controllers;

use App\User;
use App\Pais;
use App\Perfil;
use App\Departamento;
use App\Utilities\Search;

use Illuminate\Http\Request;

class UserController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate(15);

        return view('administration.user.list', compact('users'));
    }

    public function search(Request $request)
    {
        $user = new User;
        $users = Search::apply($request, $user);

        return view('administration.user.list', compact('users'));   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = null;

        $profiles = Perfil::where('mainPerfil', '>=', auth()->user()->idPerfil)->get();
        $countries = Pais::all();
        $departments = Departamento::all();
        
        return view('administration.user.create', compact('user', 'profiles', 'countries', 'departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->merge([
            'password' => bcrypt($request->password),
            'email' => $request->username,
            'inicioActividades' => date('Y-m-d')
        ]);
        
        User::create($request->all());

        return redirect()->route('administration.users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);

        return view('administration.user.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $profileModel = new Perfil;

        $profiles = $profileModel->listProfiles();
        $countries = Pais::all();
        $departments = Departamento::all();

        return view('administration.user.edit', compact('user', 'profiles', 'countries', 'departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $user->update($request->all());

        return redirect()->route('administration.users')->with('info', 'Usuario actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::findOrFail($id)->delete();

        Cache::flush();

        return redirect()->route('messages.index')->with('info', "Usuario eliminado");
    }
    
}
