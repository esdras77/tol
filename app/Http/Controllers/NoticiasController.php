<?php

namespace App\Http\Controllers;

use App\Noticia;
use App\NoticiaBody;

use Illuminate\Http\Request;

class NoticiasController extends Controller
{
    
	public static function getMainNews(){
		$newsModel = new Noticia;

		return $newsModel->where('principal', 1)
						->whereDate('fechaVencimiento', '>=', date('Y-m-d'))->get();
	}

	public function __construct(){
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = Noticia::paginate(15);

        return view('administration.news.list', compact('news'));
    }

    public function search(Request $request)
    {
        $news = new Noticia;
        $news = Search::apply($request, $news);

        return view('administration.news.list', compact('news'));   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $news = new Noticia;
        $groups = \Auth::user()->groups;
        
        return view('administration.news.create', compact('news', 'groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $parent = null)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = Noticia::findOrFail($id);
        $groups = \Auth::user()->groups;
        
        return view('administration.news.edit', compact('news', 'groups'));
    }

    /**
     * Show the form for translating the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function translate($id, $idIdioma = null)
    {
        $news = Noticia::findOrFail($id);
        $groups = \Auth::user()->groups;
        $idIdioma = (!is_null($idIdioma)) ? $idIdioma : \App::getLocale();

        return view('administration.news.translate', compact('news', 'groups', 'idIdioma'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $news = Noticia::findOrFail($id);
        $news->update($request->all());

        $body = NoticiaBody::findOrFail($request->get('idCuerpo'));

        if (empty($body)) {
            $body = new NoticiaBody;
            $body->create($request->all());

            $news->associate($body);
        } else {
            $body->update($request->all());
        }

        return redirect()->route('administration.news');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
