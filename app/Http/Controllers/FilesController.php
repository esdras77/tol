<?php

namespace App\Http\Controllers;

use Session;
use App\Archivo;
use App\Tipoaviso;
use App\Filemanager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FilesController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $parent = (!empty($request->get('base'))) ? $request->get('base') : 0;
                
        $baseFile = Archivo::find($parent);

        $disabled = !($request->session()->has('clipboard')) ? "disabled" : "";

        $icon = auth()->user()->getFilesFrom($parent);
        $tree = auth()->user()->getFilesFrom(0);

        if (is_null($baseFile) || $baseFile->isDir()) {
            return view('utils.files.list', compact('icon', 'tree', 'baseFile', 'disabled'));
        } else {
            try {
                return response()->file($baseFile->url);
            } catch(\Exception $e){
                return abort(404);
            }
        }
    }

    public function search(Request $request){
        $parent = (!empty($request->get('base'))) ? $request->get('base') : 0;
        
        $tree = auth()->user()->getFilesFrom($parent);
        $icon = auth()->user()->getFilesBy('nombre' , 'like', str_replace('*', '%', $request->get('nombre')));

        $disabled = !($request->session()->has('clipboard')) ? "disabled" : "";

        return view('utils.files.list', compact('icon', 'tree', 'baseFile', 'disabled'));
    }

    public function info(Request $request, $id){
        $file = Archivo::findOrFail($request->get('idArchivo'));

        $idOwner = $file->permisos()->where('admin', 1)->first()->idUsuario;
        $owner = \App\User::findOrFail($idOwner);

        return view('utils.files.info', compact('file', 'owner'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $folder = null;
        $tipoAviso = Tipoaviso::all();

        $parent = (!empty($request->get('idParent'))) ? $request->get('idParent') : 0;
        $baseFile = Archivo::find($parent);

        return view('utils.files.createfolder', compact('folder', 'tipoAviso', 'baseFile'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //cambia el sentido del id que recibe, viene desde el que será su padre
        $request->merge([
            'idPadre' => (($request->get('idArchivo') !== null) ? $request->get('idArchivo') : 0),
        ]);

        $folder = Archivo::create($request->all());

        $parentFolder = Archivo::find($request->get('idPadre'));

        if (!empty($parentFolder) && !auth()->user()->perfil->isAdmin()) {
            $permission = $parentFolder->getPermissionFor(auth()->user()->idUsuario);
            $folder->setPermission($permission);
        } else {
            $folder->setAdminPermission();
        }

         return "ok";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $file = Archivo::findOrFail($request->get('idArchivo'));

        return url('storage/'.$file->url);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $folder = Archivo::findOrFail($request->get('idArchivo'));
        $tipoAviso = Tipoaviso::all();

        return view('utils.files.editfolder', compact('folder', 'tipoAviso'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $folder = Archivo::findOrFail($id);
        $folder->nombre = $request->get('nombre');
        $folder->idTipoAviso = $request->get('idTipoAviso');
        $folder->aviso = $request->get('aviso');
        $folder->save();        
        
        return "ok";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $file = Archivo::findOrFail($id);
        $parent = $file->getParent();
        $file->deleteFile();

        $base = (!empty($parent->idArchivo) ? $parent->idArchivo : 0);
        return redirect()->route('utils.files', ['base' => $base])->with('info', 'Documento eliminado');
    }

    // micro operatory

    public function copy(Request $request){
        $request->session()->forget('clipboard');

        $request->session()->put('clipboard.action', 'copy');
        $request->session()->put('clipboard.file', $request->get('idArchivo'));

        return "ok";
    }

    public function cut(Request $request){
        $request->session()->forget('clipboard');
        
        $request->session()->put('clipboard.action', 'cut');
        $request->session()->put('clipboard.file', $request->get('idArchivo'));

        return "ok";
    }

    public function paste(Request $request){

        $action = $request->session()->get('clipboard.action');
        $idFile = $request->session()->get('clipboard.file');

        $file = Archivo::findOrFail($idFile);
        $parent = Archivo::findOrFail($request->get('idArchivo'));

        switch ($action) {
            case 'copy':
                $file->copyTo($parent);

                break;
            case 'cut':
                $file->moveTo($parent);

                break;
            default:
                # code...
                break;
        }

        $request->session()->forget('clipboard');
        return "ok";

    }

    public function upload(Request $request){
       
        $input = $request->all();
        
        $fileManager = new Filemanager;
        $fileManager->uploadAjax($input);

        return response()->json(['success'=>'done']);
    
    }

    public function fillaviso(Request $request){
        $list = array();

        if (!empty($request->get('idArchivo')) && !empty($request->get('idTipoAviso'))) {
            $file = Archivo::findOrFail($request->get('idArchivo'));
            $tipoAviso = Tipoaviso::findOrFail($request->get('idTipoAviso'));

            $list = $tipoAviso->getListToSelect($file);
        }

        return json_encode($list);
    }

    public function permission(Request $request){
        $file = Archivo::findOrFail($request->get('idArchivo'));
        
        $currentUser = \App\User::findOrFail(auth()->user()->idUsuario);
        $permission = $file->getPermissionFor(auth()->user()->idUsuario);
        
        return view('utils.files.permission', compact('currentUser', 'permission'));
    }

    public function grouppermission(Request $request){
        $group = \App\Grupo::findOrFail($request->get('idGrupo'));
        $currentPermission = $group->getPermissionFor($request->get('idArchivo'));

        if (!empty($group)) {
            return view('utils.files.permissiongroup', compact('currentPermission'));
        }
    }

    public function grouppermissionedit(Request $request){
        $group = \App\Grupo::findOrFail($request->get('idGrupo'));

        $group->changePermissionFor($request->get('file'), $request->get('action'));
    }

    public function userspermission(Request $request){
        $group = \App\Grupo::findOrFail($request->get('idGrupo'));
        $file = Archivo::findOrFail($request->get('idArchivo'));
        $users = $group->user;

        if (!empty($users)) {
            return view('utils.files.userspermissionlist', compact('users', 'file'));
        }

    }

    public function userspermissionedit(Request $request){
        $currentUser = \App\User::findOrFail($request->get('idUsuario'));
        
        $permission = $currentUser->permisos()->where('idArchivo', $request->get('file'))->first();
        if (!empty($permission)) {
            $permission->invert($request->get('action'));
        } else {
            $permission = \App\Permiso::create([
                                'idArchivo' => $request->get('file'),
                                'idUsuario' => $currentUser->idUsuario,
                                'admin'     => ($request->get('action') == 'admin') ? 1 : 0,
                                'read'      => ($request->get('action') == 'read') ? 1 : 0,
                                'download'  => ($request->get('action') == 'download') ? 1 : 0,
                                'delete'    => ($request->get('action') == 'delete') ? 1 : 0
                        ]);

            $currentUser->permisos()->save($permission);
        }
    }

    public function fillgroups(Request $request){
        $list = array();

        $currentUser = \App\User::findOrFail(auth()->user()->idUsuario);

        $groups = $currentUser->groups;
        foreach ($groups as $key => $group) {
            $list[]= array('id' => $group->idGrupo, 'nombre' => $group->nombre);
        }

        return json_encode($list);

    }

    public function fillusers(Request $request){
        $list = array();

        $currentGroup = \App\Grupo::findOrFail($request->get('idGrupo'));

        $users = $currentGroup->user;
        foreach ($users as $key => $user) {
            $list[]= array('id' => $user->idUsuario, 'nombre' => $user->nombre.', '.$user->apellido);
        }

        return json_encode($list);
    }

}
