<?php

namespace App\Http\Controllers;

use App\User;
use App\Empresa;
use App\Oficina;
use Illuminate\Http\Request;

class UserSettingsController extends Controller
{
    
	public function __construct(){

	}

	/* GENERAL */

	public function offices($id){

		$user = User::findOrFail($id);
		$offices = $user->oficinas()->orderBy('nombre')->paginate(15);

		return view('administration.usersettings.offices', compact('offices', 'user'));
	}

	public function relateoffice(Request $request){
		$user = User::findOrFail($request->idUsuario);
        $user->oficinas()->attach($request->idOficina);

        return redirect()->route('administration.users.offices', $request->idUsuario);
    }

    public function deleteoffice(Request $request){
    	$user = User::findOrFail($request->idUsuario);
    	$user->oficinas()->detach($request->idOficina);

    	return redirect()->route('administration.users.offices', $request->idUsuario);
    }

    public function officesrelated(Request $request){
    
        $list = array();

        $offices = \App\Empresa::findOrFail($request->get('idEmpresa'))->offices;

        foreach ($offices as $key => $office) {
            $list[]= array('id' => $office->idOficina, 'nombre' => $office->nombre);
        }

        return json_encode($list);
    
    }

	/* ISPM */
	public function ispm($id){
		$ispm = new \App\Ispm;
		
		$user = User::findOrFail($id);
		//estas oficinas vienen de ISPM, hay que relacionar especificas aca... no sirven las que tengo en tuv
		//esto va a un formulario para dar de alta relaciones con clientes
		$ispmCustomers = $ispm->getCustomersToDepartment();
		$ispmCustomers = array_shift($ispmCustomers)->cliente;
		
		return view('administration.usersettings.ispm', compact('ispmCustomers', 'user'));
	}

	public function customerprojectsselection(Request $request){
		$ispm = new \App\Ispm;

		$currentProjects = $ispm->getProjectsByCustomer($request->get('idCustomer'));
		$currentProjects = array_shift($currentProjects);
		$currentProjects = (!empty($currentProjects)) ? $currentProjects->inspeccion : null;
 
		if (!empty($currentProjects) && !is_array($currentProjects)) {
			$currentProjects = array( 0 => $currentProjects );
		}

		$user = User::findOrFail($request->get('idUsuario'));
		
		return view('administration.usersettings.projectselection', compact('currentProjects', 'user'));

	}

	/* Inspecciones & Operadores */

	public function inspections($id){
		$user = User::findOrFail($id);
		$offices = $user->oficinas()->paginate(15);

		return view('administration.usersettings.inspections', compact('offices', 'user'));
	}

	public function documents(Request $request){
		$office = \App\Oficina::findOrFail($request->get('idOficina'));
		$user = \App\User::findOrFail($request->get('idUsuario'));

		$inspections = $office->inspections()->groupBy('numRefTuv')->get();
		$operators = $office->operators()->groupBy('numRefTuv')->get();

		return view('administration.usersettings.documentslist', compact('inspections', 'operators', 'user'));
	}

	/* Productos */

}
