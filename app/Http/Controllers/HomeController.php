<?php

namespace App\Http\Controllers;

use Auth;

use App\Http\Controllers\NoticiasController;

use Illuminate\Http\Request;


class HomeController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
	}
	    
	public function index(Request $request){
		$request->session()->put('padre_actual', 0);
		$news = NoticiasController::getMainNews();

		return view('home', compact('news'));
	}

	public function terms(){
		return view('terms');
	}

	public function accept(Request $request){
		$user = Auth::user();
		$user->aceptaCondiciones = $request->get('accept') ? 1 : 0;

		$user->save();

		return redirect()->route('home');
	}

	public function administration(){

		return view("administration.landing");
	}


}
