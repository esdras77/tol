<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\User;
use App\Events\UserLoggedin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginController extends Controller
{

    public function showFormLogin(){
        return view('auth.login');
    }

    public function login(){

        $credentials = $this->validate(request(), [
            'username' => 'required|email|string',
            'password' => 'required|string'
        ]);
        
        if (Auth::attempt($credentials, request()->has('remember'))){
            event(new UserLoggedin(Auth()->user()));

            return redirect()->route('home');
        } else {
            return redirect()->route('index');
        }

    }

    /**
     * Log the user out of the application.
     *
     */
    public function logout()
    {
        return redirect('/')->with(Auth::logout());
    }

    public function resetpassword(){
        return view('auth.resetpassword');
    }

    public function passwordupdate(Request $request){
        $credentials = $this->validate(request(), [
            'username' => 'required|email|string'
        ]);

        $user = User::where('username', $credentials['username'])->first();
        $user->password = $this->newPassword();

        $user->save();

        //hay que hacer que actualice el LDAP tambien

        //enviar email con el nuevo pass

        return redirect()->route('/');
    }



    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    // use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('guest')->except('logout');
    // }
}
