<?php

namespace App\Http\Controllers;

use App\Grupo;
use App\Utilities\Search;

use Illuminate\Http\Request;

class GroupsController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = Grupo::paginate(15);

        return view('administration.groups.list', compact('groups'));
    }

    public function search(Request $request)
    {
        $group = new Grupo;
        $groups = Search::apply($request, $group);

        return view('administration.groups.list', compact('groups'));   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $group = null;
        
        return view('administration.groups.create', compact('group'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Grupo::create($request->all());

        return redirect()->route('administration.groups');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Grupo::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $group = Grupo::findOrFail($id);
        
        return view('administration.groups.edit', compact('group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $group = Grupo::findOrFail($id);

        $group->update($request->all());

        return redirect()->route('administration.groups')->with('info', 'Grupo actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Grupo::findOrFail($id)->delete();

        Cache::flush();

        return redirect()->route('administration.groups')->with('info', "Grupo eliminado");
    }

    public function users($id){
        $group = Grupo::findOrFail($id);

        //usuarios en el grupo
        $groupusers = $group->user;

        //usuarios fuera del grupo
        $users = \App\User::whereDoesntHave('groups', function($query) use ($id){
            $query->where('grupo.idGrupo', $id);
        })->get();

        return view('administration.groups.users', compact('group', 'users', 'groupusers'));
    }

    /**
     *
     * Con el toogle podemos usar la misma funcion para los dos botones
     * funciona con ajax en jquery
     *
     */
    public function setUser($idGroup, $idUser){
        $group = Grupo::findOrFail($idGroup);
        $user = \App\User::find($idUser);

        return $group->user()->toggle($user);
    }

    public function setCoordinator($idGroup, $idUser){
        $group = Grupo::findOrFail($idGroup);
        $user = \App\User::find($idUser);

        $coordinator = $group->coordinador->first();
        $group->coordinador()->detach($coordinator);

        return $group->coordinador()->attach($user);
    }

    public function admin($idGroup){
        $group = Grupo::findOrFail($idGroup);

        //usuarios en el grupo
        $groupusers = $group->user;

        $coordinator = $group->coordinador->first();
        return view('administration.groups.admin', compact('groupusers', 'coordinator', 'group'));
    }

    /* AJAX */

    public function userstore($idGroup, $idUser, Request $request){
        
        if ($request->ajax()) {
            $this->setUser($idGroup, $idUser);

            return response()->json([
                'idGrupo' => $idGroup,
                'idUsuario' => $idUser
            ]);
        }
        
    }

    public function adminstore($idGroup, $idUser, Request $request){
        if ($request->ajax()) {
            
            $this->setCoordinator($idGroup, $idUser);

            return response()->json([
                'idGrupo' => $idGroup,
                'idUsuario' => $idUser
            ]);
        }
    }
}
