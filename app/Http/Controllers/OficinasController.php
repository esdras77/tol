<?php

namespace App\Http\Controllers;

use App\Oficina;
use App\Empresa;
use App\Direccion;
use Illuminate\Http\Request;

class OficinasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $enterprise = Empresa::findOrFail($id);

        $offices = $enterprise->offices()->paginate(15);

        return view('administration.enterprise.office.list', compact('enterprise', 'offices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($idEmpresa)
    {
        $enterprise = Empresa::findOrFail($idEmpresa);
        $countries = \App\Pais::all();
        $office = new Oficina;

        return view('administration.enterprise.office.create', compact('enterprise', 'office', 'countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->merge([
            'idOficina' => md5(uniqid('', true)),
            'idDireccion' => md5(uniqid('', true))
        ]);

        $office = new Oficina;
        $office->fill($request->all());
        $office->address()->create($request->all());
        $office->save();

        return redirect()->route('administration.offices', $office->idEmpresa);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Oficina::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $office = Oficina::findOrFail($id);

        $enterprise = $office->enterprise;
        $countries = \App\Pais::all();

        return view('administration.enterprise.office.edit', compact('enterprise', 'office', 'countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $office = Oficina::findOrFail($id);
        
        $office->update($request->all());
        $office->address()->update($request->all());

        return redirect()->route('administration.offices', $office->idEmpresa);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
