<?php

namespace App\Http\Controllers;

use App\Empresa;
use App\Pais;
use App\Direccion;
use App\EmpresaSolicitante;
use App\Utilities\Search;

use Illuminate\Http\Request;

class EmpresasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $enterprises = Empresa::orderBy('nombre')->paginate(15);

        return view('administration.enterprise.list', compact('enterprises'));
    }

    public function search(Request $request)
    {
        $enterprise = new Empresa;
        $enterprises = Search::apply($request, $enterprise);

        return view('administration.enterprise.list', compact('enterprises'));   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $enterprise = null;

        return view('administration.enterprise.create', compact('enterprise'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\CreateEnterpriseRequest $request)
    {
        $request->merge([
            'idEmpresa' => md5(uniqid('', true)),
            'idTipoCuenta' => 1
        ]);

        $enterprise =Empresa::create($request->all());

        return redirect()->route('administration.enterprises.address', $enterprise->idEmpresa);
    }

    public function address($id){

        $enterprise = Empresa::findOrFail($id);

        $address = $enterprise->address;
        $countries = Pais::all();

        return view('administration.enterprise.address', compact('address', 'countries', 'enterprise'));

    }

    public function storeaddress(Request $request){
        $enterprise = Empresa::findOrFail($request->get('idEmpresa'));

        if (!is_null($enterprise->address)) {
            $enterprise->address->update($request->all());
        } else {
            $request->merge([
                'idDireccion' => md5(uniqid('', true))
            ]);

            $address = Direccion::create($request->all());
            $enterprise->address()->associate($address);
            $enterprise->save();
        }

        if ($enterprise->tipo->tipoEmpresa == 'productos_empresasolicitante') {
            return redirect()->route('administration.enterprises.type', $enterprise->idEmpresa);
        } else {
            return redirect()->route('administration.enterprises');
        }
    }

    public function type($id){

        $enterprise = Empresa::findOrFail($id);
        $solicitant = $enterprise->solicitant;

        return view('administration.enterprise.type', compact('solicitant', 'enterprise'));

    }

    public function storetype(Request $request){
        
        $enterprise = Empresa::findOrFail($request->get('idEmpresa'));
        
        //no pude cambiar el nombre del archivo al de la ubicacion en el symlink en el request, tuve que crear un array y copiar los valores
        //todo: cambiar esto    
        if ($request->hasFile('logo')){
            $params = $request->all();
            $params['logo'] = str_replace('public/', '', $request->file('logo')->store('public/enterprises_logos'));
        }
        
        if (!is_null($enterprise->solicitant)) {
            $enterprise->solicitant->update($params);
        } else {
            $solicitant = EmpresaSolicitante::create($params);
            $solicitant->asossiate($enterprise);
        }

        return redirect()->route('administration.enterprises');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Empresa::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $enterprise = Empresa::findOrFail($id);

        return view('administration.enterprise.edit', compact('enterprise'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $enterprise = Empresa::findOrFail($id);
        
        $enterprise->update($request->all());

        return redirect()->route('administration.enterprises.address', $enterprise->idEmpresa);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
