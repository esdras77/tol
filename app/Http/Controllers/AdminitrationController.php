<?php

namespace App\Http\Controllers;

use Auth;

use Illuminate\Http\Request;

class AdministrationController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
	}
	    
	public function index(Request $request){
		$request->session()->put('padre_actual', 4);

		return view('administration.landing');
	}

	public function countries(){

		$countries = \App\Pais::paginate(15);

		return view('administration.countries.list', compact('countries'));

	}

	public function findcountry(Request $request){

		$country = new \App\Pais;
        $countries = \App\Utilities\Search::apply($request, $country);

        return view('administration.countries.list', compact('countries')); 
	}

	public function lnupdates(){
		$updates = \App\Logdeactualizacion::paginate(15);

		return view('administration.lnupdates.list', compact('updates'));
	} 

	public function languageselection(){
		//dummy to activate the middleware
		return 'OK';
	}


}
