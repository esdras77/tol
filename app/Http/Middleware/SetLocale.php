<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use App\Idioma;

use Illuminate\Http\Request;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $session = $request->getSession();

        if (!$session->has('currentLanguage')){
            if ($request->has('languageChangeRequest')) {
                $session->put('currentLanguage', $request->languageChangeRequest);
            } else {
                $session->put('currentLanguage', 1);
            }
        } else {
            if ($request->has('languageChangeRequest')) {
                $session->forget('currentLanguage');
                $session->put('currentLanguage', $request->languageChangeRequest);
            }
        }

        return $next($request);
    }
}
