<?

namespace App\Http\Composers;

use Illuminate\Contracts\View\View;

class TilesmenuComposer(){

	public function compose(View $view){
		$view->with('itemMenuList', \Auth::user()->getMenu());
	}

}