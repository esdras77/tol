<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inspeccion extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'inspecciones';//nombre de tu tabla

    public $incrementing = false;

    protected $fillable = ['idOficina', 'idLotusEquipo', 'numRefTuv', 'numCorrelatividad', 'tipoEquipo', 'subTipoEquipo', 'fabricante',
							'modelo', 'numSerie', 'anioFabricacion', 'numInterno', 'fechaInspeccion', 'lugarInspeccion', 'inspector',
							'resultado', 'fechaVencimiento', 'numOblea', 'new'];

    public function getKeyName(){
        return 'idLotus';
    }

    public function oficina(){
    	return $this->belongsTo(Oficina::class, 'idOficina');
    }

    public function abletobeshownto($user){
    	$result = \DB::select('select count(*) as counter from config_inspect_specific where idUsuario = ? and numRefTuv = ? and inspecciones = 1', 
    						  [$user->idUsuario, $this->numRefTuv]);
    	
    	return boolval($result[0]->counter);
    }

}
