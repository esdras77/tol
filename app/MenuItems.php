<?php

namespace App;

use App\Menu;
use DB;

use Illuminate\Database\Eloquent\Model;

class MenuItems extends Model{

	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'itemdemenu';//nombre de tu tabla

	public function menu(){
		return $this->belongsToMany(Menu::class, 'menu_itemdemenu', 'idItemMenu', 'idMenu');
	}

	public function getKeyName(){
        return 'idItemMenu';
    }

    public function isInMenu(Menu $menu){
        // DB::connection()->enableQueryLog();
        $menues = $this->menu;
        
        // $queries = DB::getQueryLog();
        // dd(end($queries));

        return $filtered = $menues->contains($menu);
    }

    public function hasChildren(){
        
        $children = $this->where('padre', $this->idItemMenu)->get();

        return $children->isNotEmpty();
    }


    /* BREADCRUMB METHODS */
    public function getBreadCrumb(){
        $currentItem = $this->where('url', \Request::route()->getName())->first();

        if (!empty($currentItem)) {
            return $this->_getBreadCrumb($currentItem->idItemMenu)->reverse();
        }

        return null;
    }

    private function _getBreadCrumb($itemId){
        $breadcrumb = collect(array());

        $currentItem = $this->where('idItemMenu', $itemId)->first();  
        $breadcrumb->push($currentItem);

        if (!empty($currentItem->padre)) {
            $breadcrumb = $breadcrumb->merge($this->_getBreadCrumb($currentItem->padre));
        }
        
        return collect($breadcrumb);

    }

}