<?php

namespace App\Listeners;

use App\Events\UserLoggedin;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateLoginData
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserLoggedin  $event
     * @return void
     */
    public function handle(UserLoggedin $event)
    {
        $event->user->updateLogin();
    }
}
