<?php

namespace App;

use App\Permiso;
use Illuminate\Database\Eloquent\Model;

class Archivo extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'archivo';//nombre de tu tabla

    public function getKeyName(){
        return 'idArchivo';
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idPadre', 'nombre', 'url', 'extension', 'descripcion', 'aviso' 
    ];

	public function permisos(){
		return $this->hasMany(Permiso::class, 'idArchivo');
	}

	public function usuario(){
		return $this->belongsToMany(User::class, 'permiso', 'idArchivo', 'idUsuario');
	}

	public function tipoAviso(){
		return $this->belongsTo(Tipoaviso::class, 'idTipoAviso');
	}

	public function getFileTreeIcon(){

	}

	public function getPermissionFor($idUsuario){
		return $this->permisos()->where('idUsuario', $idUsuario)->first();
	}

	public function setPermission(Permiso $permission){
		return $this->permisos()->save($permission);
	}

	public function setPermissionBunch($permissionBunch){
		foreach($permissionBunch as $permiso){
        	$newPermiso = $permiso->replicate();
        	$newPermiso->save();

        	$this->permisos()->save($newPermiso);
        }
	}

	public function setAdminPermission(){
		return $this->permisos()->create([
						'idArchivo' => $this->idArchivo,
						'idUsuario' => auth()->user()->idUsuario,
						'admin' 	=> 1,
						'read'  	=> 1,
						'download' 	=> 1,
						'delete' 	=> 1
				]);
	}
	
	public function getParent(){
		$auxFile = Archivo::where('idArchivo', $this->idPadre)->first();

		return $auxFile;
	}

	public function getChildren(){
		$children = Archivo::where('idPadre', $this->idArchivo)->get();

		return $children;
	}

	public function countChildren(){
		return Archivo::where('idPadre', $this->idArchivo)->count();
	}

	public function isDir(){
		return empty($this->extension);
	}

	public function hasSubDir(){
		$arrSubdirs = array();
		$subDirs = Archivo::where('idPadre', $this->idArchivo)->where('url', null)->get();

		if (!empty($subDirs)) {
			foreach ($subDirs as $key => $subFolder) {
				$permission = $subFolder->permisos()->where('idUsuario', auth()->user()->idUsuario)->get();
				
				if (!empty($permission) && is_object($permission->first()) && $permission->first()->read){
					$arrSubdirs[]= $subFolder;
				}
			}
		}
		
		return count($arrSubdirs);
		
	}

	public function getHeaders(){
		$mime = array(	".jpg"	=>	"image/jpeg",
						".JPEG"	=>	"image/jpeg",
					  	".gif"	=>	"image/gif",
						".bmp"	=>	"image/bmp",
						".tif"	=>	"image/tiff",
						".tiff"	=>	"image/tiff",
						".doc"	=>	"application/msword",
						".docx"	=>	"application/msword",
						".rtf"	=>	"application/msword",
						".DOC"	=>	"application/msword",
						".xls"	=>	"application/vnd.ms-excel",
						".xlsx"	=>	"application/vnd.ms-excel",
						".csv"	=>	"application/vnd.ms-excel",
						".ppt"	=>	"application/vnd.ms-powerpoint",
						".pdf"	=>	"application/pdf",
						".txt"	=>	"text/plain",
						".htm"	=>	"text/html",
						".html"	=>	"text/html",
						".mht"	=>	"text/html",
						".css"	=>	"text/css",
						".zip"	=>	"application/x-zip-compressed",
						".exe"	=>	"application/x-msdos-program",
						".vsd"	=>	"application/vnd.visio"
						);

		$ext  = $this->extension;
		$type = (array_key_exists($ext,$mime))? $mime[$ext] : "multipart/form-data";

		return array( 'Content-Type' => $type );
	}

	public function deleteFile(){
		//busco permisos y elimino
		$permission = $this->permisos;
		if (!empty($permission)) {			
			foreach ($permission as $key => $value) {
				$value->delete();
			}
		}

		//luego sigo con la estructura
		if ($this->isDir()) {
			$children = $this->getChildren();
			if (!empty($children)){
				foreach ($children as $child){
					$child->deleteFile();
				}
			}
		}
		
		return $this->delete();
	}

	/**
	 *	Recursive function which copy a file and their descendence in a new parent
	 */
	public function copyTo(Archivo $archivo){

		$newFile = $this->replicate();
        $newFile->idPadre = $archivo->idArchivo;
        $newFile->save();

        $newFile->setPermissionBunch($archivo->permisos);

        if ($this->isDir()) {
            $children = $this->getChildren();
            if (!empty($children)){

                foreach ($children as $child){
                    $child->copyTo($newFile);
                }

            }
        }
                
	}

	public function moveTo(Archivo $parent){
		
        $this->idPadre = $parent->idArchivo;
        $this->save();

	}

	public function getRoute(){
		$route = array();
		$this->_getRouteAux($route);

		if (!is_array($route)) {
			$aux[] = $route;
			$route = $aux;
		}

		return $route;
	}

	/**
	 * funcion que arma el arbol de la ruta de un archivo en el camino que se tomo para llegar a él
	 *
	 */
	private function _getRouteAux(&$route){

		if (empty($this->idPadre)) {
			$route[]= $this;
		} else {
			$auxFile = $this->find($this->idPadre);
			$auxFile->_getRouteAux($route);
			$route[] = $this;
		}
	}
}
