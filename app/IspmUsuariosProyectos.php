<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IspmUsuariosProyectos extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ispm_usuarios_proyectos';//nombre de tu tabla

	public function getKeyName(){
        return array('idUsuario', 'idProyecto');
    }
    
	public function user(){
		return $this->hasMany(User::class, 'idUsuario');
	}
}
