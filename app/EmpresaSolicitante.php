<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmpresaSolicitante extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'productos_empresasolicitante';//nombre de tu tabla

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idEmpresa', 'cuit', 'rubro', 'responsableLegal', 'cargo', 'personaDeContacto', 'emailDeContacto', 'telefonoDeContacto',
        'logo', 'personaDeContactoTecnico', 'emailDeContactoTecnico', 'telefonoDeContactoTecnico'
    ];

	public function enterprise(){
		return $this->belongsTo(Empresa::class, 'idEmpresa');
	}

	public function getKeyName(){
        return 'idProductos_empresasolicitante';
    }
}
