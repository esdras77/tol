<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoEmpresa extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tipoEmpresa';//nombre de tu tabla

    public $incrementing = false;

    protected $fillable = ['tipoEmpresa', 'descripcion', 'enabled'];

    public function getKeyName(){
        return 'tipoEmpresa';
    }

    public function empresa(){
    	return $this->hasMany(Empresa::class, 'tipoEmpresa');
    }

    public static function getOptions(){
    	$aux = new TipoEmpresa;
    	$aux->descripcion = 'Seleccione';
    	$aux->tipoEmpresa = '*';
    	$arrOptions = collect(array($aux));

    	return $arrOptions->merge(self::where('enabled', 1)->get());
    }
}
