<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grupo extends Model
{
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'grupo';//nombre de tu tabla

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
    ];

    public function getKeyName(){
        return 'idGrupo';
    }

    public function user(){
		return $this->belongsToMany(User::class, 'usuariogrupo', 'idGrupo', 'idUsuario');
	}

	public function coordinador(){
		return $this->belongsToMany(User::class, 'coordinadorgrupo', 'idGrupo', 'idUsuario');		
	}

	public function delete(){
		//checkear las conecciones con los usuarios
		
	}

    public function getPermissionFor($idArchivo){
        $admin = 1;
        $read = 1;
        $download = 1;
        $delete = 1;

        $arrPermissionGroup = array();

        if (!empty($this->user)) {
            foreach ($this->user as $key => $user) {
                $file = \App\Archivo::findOrFail($idArchivo);

                $permission = $file->getPermissionFor($user->idUsuario);
                if (!is_null($permission)) {
                    if (!$permission->isOwner()) $admin = 0;
                    if (!$permission->couldRead()) $read = 0;
                    if (!$permission->couldDownload()) $download = 0;
                    if (!$permission->couldRemove()) $delete = 0;
                } else {
                    $admin = 0;
                    $read = 0;
                    $download = 0;
                    $delete = 0;
                }

            }

            $arrPermissionGroup = array('idGrupo' => $this->idGrupo, 'idUsuario' => null, 'idArchivo' => $idArchivo, 'admin' => $admin, 'read' => $read, 'download' => $download, 'delete' => $delete);
        }
        
        return (object) $arrPermissionGroup;
    }

    /**
     *  changes permission into a group for $idArchivo to $action
     */
    public function changePermissionFor($file, $action){

        $currentPermission = $this->getPermissionFor($file);

        $this->user->each(function($user, $key) use ($file, $action, $currentPermission){
           
            $value = !$currentPermission->{$action};
            $user->changePermissionFor($file, $action, $value);

        });
    }


}
