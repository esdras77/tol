<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Traduccion extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'traduccion';//nombre de tu tabla

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idTexto', 'idIdioma', 'traduccion', 'usuarioEditor'
    ];

    public function getKeyName(){
        return 'idTraduccion';
    }

    public function texto(){
    	return $this->belongsTo(Texto::class, 'idTexto');
    }
}
