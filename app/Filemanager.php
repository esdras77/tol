<?php

namespace App;

use App\Archivo;
use Illuminate\Http\Request;

class Filemanager{


	protected $_storage;

	public function __contruct(){

	}

	public function path_setup($username){
		$path = '/storage/files/';

		if (!is_dir(getcwd().$path)) mkdir(getcwd().$path, 02775, true);
		
		// directorios por usuario
		if (!is_dir(getcwd().$path."/".$username)) 
			return mkdir(getcwd().$path."/".$username, 02775, true);

		return true;
	}

	public function upload(){

	}

	public function uploadAjax($fileData){
		$url = "";
		$baseName = "";
		$username = auth()->user()->username;

		$fn = (isset($_SERVER['HTTP_X_FILENAME']) ? $_SERVER['HTTP_X_FILENAME'] : false);
        
        if ($fn) {
            // AJAX call
            $nombreOriginal = str_replace(" ", "_", $fn);
            $nombreOriginal = str_replace(",", "_", $nombreOriginal);

            if ($this->path_setup($username)){

	            $baseName = '/storage/files/'.$username.'/'.$nombreOriginal;
	            $url = getcwd().$baseName;
	            
	            //upload efectively the file
				@file_put_contents($url, file_get_contents('php://input'));
	        }

	        $arrFile = explode(".", $nombreOriginal);
			$nombreOriginal = array_shift($arrFile);
			$extension = array_pop($arrFile);

	        $fileData['nombre'] = $nombreOriginal;
	        $fileData['extension'] = $extension;
	        $fileData['url'] = str_replace('/storage/', '', $baseName);
	        Archivo::create($fileData)->setAdminPermission();

        }

	}

}