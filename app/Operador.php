<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Operador extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'operadores';//nombre de tu tabla

    public $incrementing = false;

    protected $fillable = ['idOficina', 'idLotusEquipo', 'idPerson', 'numRefTuv', 'credentialStatus', 'name', 'dni', 'hasDniCopy', 'hasPicture', 
    						'experience', 'validDateMedicalCert', 'evaluationDate', 'expiration', 'equipment', 'capacity', 'nroOperador',
    						'certificateGenerated', 'new'];

    public function getKeyName(){
        return 'idLotus';
    }

    public function oficina(){
    	return $this->belongsTo(Oficina::class, 'idOficina');
    }

    public function abletobeshownto($user){
    	$result = \DB::select('select count(*) as counter from config_inspect_specific where idUsuario = ? and numRefTuv = ? and operadores = 1', 
    						  [$user->idUsuario, $this->numRefTuv]);
    	
    	return boolval($result[0]->counter);
    }
}
