<?php

namespace App;


use Session;
use App\MenuItems;
use Illuminate\Http\Request;

use Illuminate\Database\Eloquent\Model;

class Breadcrumb extends Model
{

	public function __construct(){
		//Session::pull('breadcrumb');
	}
    
	public function setTile(){

		$tile = MenuItems::where('url', \Request::route()->getName())->first();
		
		if ($this->exist($tile)){
			$this->removeFrom($tile);
		} else {
			if (!is_null($tile)) {
				Session::push('breadcrumb', $tile);
			} else {
				if (\Request::path() == 'home'){
					Session::pull('breadcrumb');
				}
			}
		}
	}

	public function getTiles(){
		return Session::get('breadcrumb');
	}

	public function exist($tile){
		$exist = false;
		
		if (Session::has('breadcrumb') && !is_null($tile)) {
			foreach (Session::get('breadcrumb') as $key => $item){
				if ($tile->idItemMenu == $item->idItemMenu) {
					$exist = true;
				}
			}
		}
		
		return $exist;
	}

	public function getPosition($tile){
		$index = 0;
		
		if (Session::has('breadcrumb') && !is_null($tile)) {
			foreach (Session::get('breadcrumb') as $key => $item){
				if ($tile->idItemMenu == $item->idItemMenu) {
					$index = $key;
				}
			}
		}

		return $index;
	}
	
	public function removeFrom($tile){
		$pos = $this->getPosition($tile);
		
		if ($this->exist($tile)){
			//agrego a la posicion actual 1 punto para corregir la desviacion por el comienzo desde 0
			$breadcrumb = Session::get('breadcrumb');
			array_splice($breadcrumb, $pos+1);
			Session::pull('breadcrumb');
			Session::put('breadcrumb', $breadcrumb);
		}
	}


}
