<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'empresa';//nombre de tu tabla

    public $incrementing = false;

    protected $fillable = ['idEmpresa', 'idTipoCuenta', 'idDireccion', 'tipoEmpresa', 'nombre', 'sapCode', 'BK'];

    public function getKeyName(){
        return 'idEmpresa';
    }

    public function offices(){
    	return $this->hasMany(Oficina::class, 'idEmpresa');
    }

    public function tipo(){
    	return $this->belongsTo(TipoEmpresa::class, 'tipoEmpresa');
    }

    public function address(){
        return $this->belongsTo(Direccion::class, 'idDireccion');
    }

    public function solicitant(){
        return $this->hasOne(EmpresaSolicitante::class, 'idEmpresa');
    }
}
